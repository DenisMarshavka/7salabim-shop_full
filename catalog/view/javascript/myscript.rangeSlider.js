$(document).ready(function () {
    let $range_catalog_price            = $('#js-catalog-range-price'),
        id_catalog_current_sorts_price  = '#js-catalog-current-sort-price';

    $range_catalog_price.ionRangeSlider({
        skin: "big",
        step: 10,
    });

    indicatorsCurrentInstanceRange();
    $range_catalog_price.on('change', function () {
        indicatorsCurrentInstanceRange();
        changeCurrentPrice();
    });

    $('#js-catalog-min-price, #js-catalog-max-price').on('keyup change', function () {
        changeRangeDots( $('#js-catalog-min-price').val(), $('#js-catalog-max-price').val() );
    });

    function changeRangeDots(min_changed_value = false, max_changed_value = false) {
        if (min_changed_value && max_changed_value) {
            $('.js-range-slider').data('ionRangeSlider').update(
                {
                    from: min_changed_value,
                    to: max_changed_value
                }
            );
        }
    }

    function indicatorsCurrentInstanceRange() {
        $('#js-catalog-min-price').attr('min-price', $range_catalog_price.data('from')).val( normalizePriceValue($range_catalog_price.data('from')) );
        $('#js-catalog-max-price').attr('max-price', $range_catalog_price.data('to')).val( normalizePriceValue($range_catalog_price.data('to')) );
    }

    function changeCurrentPrice() {
        if ($('.catalog-sorts-current__content-item--price').length) {
            $(id_catalog_current_sorts_price).addClass('hide');

            $(id_catalog_current_sorts_price).text(
                normalizePriceValue( $('#js-catalog-min-price').val() ) + ' - ' + normalizePriceValue($('#js-catalog-max-price').val() ) + '₴'
            );

            setTimeout(function () {
                $(id_catalog_current_sorts_price).removeClass('hide');
            }, 500);
        }
    }

    //TODO: the script name 'The event reset price'
    let default_value_sort_price = $('#js-catalog-current-sort-price').text();

    $(document).on('click', '#js-catalog-reset-sort-price, #js-catalog-reset-sorts', resetInstancePrice);

    function resetInstancePrice() {
        if ($('#js-catalog-current-sort-price').text() !== default_value_sort_price) {
            $('.js-range-slider').data('ionRangeSlider').reset();
        }
    }

    function normalizePriceValue(value_price = false) {
        let array_price   = value_price.toString().split(''),
            normal_value  = '';

        if (value_price) {
            for (let i = 0; i < array_price.length; i++) {
                if ($.isNumeric(array_price[i])) {
                    if (array_price.length === 4 && i === 1) {
                        normal_value += ' ';
                    } else if (array_price.length === 5 && i === 2) {
                        normal_value += ' ';
                    } else {
                        if (array_price.length === 6 && i === 3) {
                            normal_value += ' ';
                        }
                    }

                    normal_value += array_price[i];
                }
            }

            return normal_value;
        }
    }
});
