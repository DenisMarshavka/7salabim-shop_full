let flag_to_scroll = true;

$(document).ready(function () {
    //START: Catalog
    var clicked_pagination_item = false,
        filter_min_price        = false,
        filter_max_price        = false,
        filter_holidays         = false,
        filter_sort             = false,
        filter_color            = false,
        filter_sex              = false,
        grid_items              = 20,
        length_items            = 0,
        sort_by_new             = false,
        sort_by_popular         = false,
        sort_by_price_asc       = false,
        sort_by_price_desc      = false,
        sort_by_sale_price      = false,
        sort_by_liked           = false,
        flag_filter             = true,
        flag_send_ajax          = true,
        val_to_search           = false,
        filter_attribute_id     = false,
        data_all_active_filters = [],
        current_active_item     = 1;

    if ($('#catalog').length) setTimeout(setFilters, 500);

    $(document).on('click', '.catalog-products-grid__pagination-btn-next.btn-next, .catalog-products-grid__pagination-btn-prev.btn-prev', function () {
        var active_item      = +$.trim($('.catalog-products-grid__pagination.pagination-controllers .pagination__item--active').text()),
            last_number_page = +$.trim($('.catalog-products-grid__pagination .pagination__item').last().text());

        console.log('step clicked 1', active_item, last_number_page);

        clicked_pagination_item = true;

        if (flag_filter) {
            if ( ($(this).hasClass('btn-prev') && active_item !== 1) || ($(this).hasClass('btn-next') && active_item !== last_number_page) ) {
                if ($(this).hasClass('btn-prev')) {
                    --active_item;
                } else ++active_item;

                setTimeout(setFilters, 550, active_item);

                console.log('step clicked2', active_item, last_number_page);
            }
        }
    });

    $(document).on(
        'mouseup',

        '#js-catalog-reset-sort, ' +
        '.cusSelBlock .options li, ' +
        '#js-catalog-reset-sorts, ' +
        '#js-catalog-reset-sort-search, ' +
        '.current-sorts-tab__colors-list li, ' +
        '.current-sorts-tab__sex-list-item-title, ' +
        '.catalog-sorts-current__btn--delete, ' +
        '.pagination li, ' +
        '#js-catalog-sort-price'

        , function () {
            changeFilters($(this));
        }
    );

    if (window.outerWidth <= 767) {
        $(document).on(
            'click change',

            '#js-catalog-sort-price, #js-catalog-sort-price .irs--big'

            , function () {
                changeFilters($(this));
            }
        );
    }

    $(document).on('keyup', '#js-catalog-min-price, #js-catalog-max-price', function () {
        changeFilters($(this));
    });

    function changeFilters($_this = false) {
        var reset_page          = false,
            current_href_params = [];

        if (!$_this.parents('.catalog-products-grid__pagination.pagination-controllers').length) {
            current_active_item  = 1;
            reset_page  = true;
        } else clicked_pagination_item = true;

        if ($_this.attr('id') && ($_this.attr('id') === 'js-catalog-reset-sorts' || $_this.attr('id') === 'js-catalog-reset-sort-search') ) {
            current_href_params = checkFiltersInUrl();

            if (current_href_params.length > 1) {
                changeUrl({}, null, window.location.origin + '?route=product/catalog');

                if ($('.catalog-sorts-current__content-item--search').length) {
                    $('.catalog-sorts-current__content-item--search').addClass('hide');

                    setTimeout(function () {
                        $('.catalog-sorts-current__content-item--search').slideToggle(500, function () {
                            $(this).remove();
                        });
                    }, 300);
                }
            }
        }

        setTimeout(setFilters, 550, (reset_page) ? current_active_item : false);
    }

    function changeUrl(page, title, url) {
        if ("undefined" !== typeof history.pushState) {
            history.pushState({page: page}, title, url);
        } else window.location.assign(url);
    }

    function scrollToProductsContent() {
        flag_to_scroll = false;

        if (window.outerWidth <= 767) $('#preloader').removeClass('hide');

        setTimeout(function () {
            $('html, body').animate({
                scrollTop: $( (window.outerWidth <= 767) ? '.catalog-content__section-right' : '.catalog-head').offset().top
            }, 1500);
        }, 500);

        setTimeout(function () {
            if (window.outerWidth <= 767) $('#preloader').addClass('hide');

            flag_to_scroll = true;
        }, 1500);
    }

    function checkFiltersInUrl() {
        var current_href_params            = window.location.search.split('&'),
            alert_search_html              = '',
            approve_to_alert_search_filter = false;

        if (current_href_params.length > 1) {
            current_href_params = current_href_params[1].split('=');

            if (current_href_params[0] !== undefined && current_href_params[1] !== undefined) {
                if (current_href_params[0] === "search_val" && $.trim(current_href_params[1]).length > 3) {
                    approve_to_alert_search_filter = true;

                    val_to_search = $.trim(current_href_params[1]);
                } else if (current_href_params[0] === "filter_id" && !isNaN(+current_href_params[1])){
                    filter_attribute_id = current_href_params[1];
                } else {
                    filter_attribute_id = false;
                    val_to_search = false;
                }
            }

            if (approve_to_alert_search_filter && !$('.catalog-sorts-current__content-item--search').length) {
                alert_search_html =
                    '<li class="catalog-sorts-current__content-item catalog-sorts-current__content-item--search">' +
                    '    <b>' +
                    '        <span class="title">Пошук</span>' +
                    '    </b>' +

                    '    <span id="js-catalog-current-sort-search">Бла бла бла...</span>' +

                    '    <button class="catalog-sorts-current__btn catalog-sorts-current__btn--delete" id="js-catalog-reset-sort-search">' +
                    '        <i class="fas fa-times"></i>' +
                    '    </button>' +
                    '</li>';

                $('.catalog-sorts-current__content').append(alert_search_html);
            }
        } else {
            current_href_params = [];

            filter_attribute_id = false;
            val_to_search = false;
        }

        return current_href_params;
    }

    function setFilters(active_page = false) {
        var current_href_params = checkFiltersInUrl(),
            $sort_option_jquery = $('#js-catalog-form-sort .cusSelBlock select option');

        if (current_href_params.length > 1) scrollToProductsContent();

        filter_min_price = +$.trim($('#js-catalog-min-price').attr('min-price'));
        filter_max_price = +$.trim($('#js-catalog-max-price').attr('max-price'));

        sort_by_new        = $sort_option_jquery.eq(checkSelectedOptionSortOrder()).data('new') !== undefined;
        sort_by_popular    = $sort_option_jquery.eq(checkSelectedOptionSortOrder()).data('popular') !== undefined;
        sort_by_price_asc  = $sort_option_jquery.eq(checkSelectedOptionSortOrder()).data('price-asc') !== undefined;
        sort_by_price_desc = $sort_option_jquery.eq(checkSelectedOptionSortOrder()).data('price-desc') !== undefined;
        sort_by_price_desc = $sort_option_jquery.eq(checkSelectedOptionSortOrder()).data('price-desc') !== undefined;
        sort_by_sale_price = $sort_option_jquery.eq(checkSelectedOptionSortOrder()).data('sale-desc') !== undefined;

        sort_by_liked      = $sort_option_jquery.eq(checkSelectedOptionSortOrder()).data('liked') !== undefined;

        filter_holidays  = ($('#js-catalog-sort-holidays .options li').eq(0)[0] !== $('#js-catalog-sort-holidays .options li.active')[0]) ? $('#js-catalog-sort-holidays .options li.active').attr('rel') : false;
        filter_sort      = (!$sort_option_jquery.eq(checkSelectedOptionSortOrder()).data('default')) ? $('#js-catalog-form-sort .options li.active').attr('rel') : false;
        filter_color     = (!$('.current-sorts-tab__colors-list li.active a').hasClass('item--default')) ? $('.current-sorts-tab__colors-list li.active a').data('color') : false;
        filter_sex       = ($('input[name="catalog-type-sex"]:checked').attr('value') !== undefined) ? $('input[name="catalog-type-sex"]:checked').attr('value') : false;

        grid_items       = +$.trim($('.catalog-head__section-left .pagination__item--active').text());
        (!active_page) ? current_active_item = +$.trim($('.catalog-products-grid__pagination.pagination-controllers .pagination__item--active').text()) : current_active_item = active_page;

        // console.log(filter_min_price, filter_max_price, filter_holidays, filter_sort, filter_color, grid_items, filter_sex, 'current_active_item', current_active_item, 'sort_by_new', sort_by_new);
        // console.log('filter_min_price', filter_min_price, 'filter_max_price', filter_max_price);

        (filter_color && filter_color.indexOf('#') > -1) ? filter_color = filter_color.split('#')[1] : false;

        var flag_to_new_filter = checkParamsFilter([
            filter_min_price,
            filter_max_price,
            filter_holidays,
            filter_sort,
            filter_color,
            filter_sex,
            grid_items,
            length_items,
            sort_by_new,
            sort_by_popular,
            sort_by_price_asc,
            sort_by_price_desc,
            sort_by_sale_price,
            sort_by_liked,
            val_to_search,
            filter_attribute_id,
            current_active_item
        ]);

        if (flag_to_new_filter) getProductsByFilter(true);
    }

    function checkParamsFilter(new_params_filters) {
        var approve_filters = false;

        if (new_params_filters) {
            if ( (!data_all_active_filters.length) || (new_params_filters.join('-') !== data_all_active_filters.join('-')) ) {
                approve_filters = true;

                data_all_active_filters = new_params_filters;
            }
        }

        return approve_filters;
    }

    function checkSelectedOptionSortOrder() {
        var check_step    = 0,
            finished_step = false;

        $('#js-catalog-form-sort .options li').each(function () {
            if ($(this).hasClass('active')) finished_step = check_step;

            ++check_step;
        });

        return finished_step;
    }

    function getProductsByFilter(debug = false) {
        var action_to_url =  window.location.origin + '/index.php?route=extension/module/filter_ajax';

        if (flag_filter) {
            flag_filter = false;

            (filter_attribute_id) ? action_to_url += '&filter_id=' + filter_attribute_id : false;
            (val_to_search)       ? action_to_url += '&search=' + val_to_search : false;

            (current_active_item) ? action_to_url += '&pagination_count=' + current_active_item : false;
            (filter_sex)          ? action_to_url += '&sex=' + filter_sex : false;
            (filter_min_price)    ? action_to_url += '&min_price=' + filter_min_price : false;
            (filter_max_price)    ? action_to_url += '&max_price=' + filter_max_price : false;

            (filter_holidays)     ? action_to_url += '&filter_name=' + filter_holidays : false;
            (filter_sort)         ? action_to_url += '&order=' + filter_sort : false;
            (filter_color)        ? action_to_url += '&color=' + filter_color : false;

            (sort_by_new)         ? action_to_url += '&order_by_new=true' : false;
            (sort_by_popular)     ? action_to_url += '&order_by_popular=true' : false;
            (sort_by_price_asc)   ? action_to_url += '&order_by_price_asc=true' : false;
            (sort_by_price_desc)  ? action_to_url += '&order_by_price_desc=true' : false;
            (sort_by_sale_price)  ? action_to_url += '&order_by_sale=true' : false;
            (sort_by_liked)       ? action_to_url += '&order_by_wish_list=true' : false;

            if (sort_by_liked) action_to_url += '&' + getParamsInWishList();

            action_to_url += '&grid=' + grid_items;

            // return;

            $('.catalog-products-grid__preloader').removeClass('hide');

            $('.catalog-content__section-right').addClass('catalog-content__section-right--load');
            $('.catalog-products-grid__content').addClass('catalog-products-grid__content--load');

            if ($('.alert-info').is(':visible')) {
                $('.alert-info').addClass('hide');

                setTimeout(function () {
                    $('.alert-info').hide();
                }, 550);
            }

            if (!$('.catalog-products-list').is(':visible')) $('.catalog-products-list').show();
            if (!$('.catalog-products-grid__pagination').is(':visible')) $('.catalog-products-grid__pagination').show();
            if (!$('.catalog-products-grid').is(':visible')) $('.catalog-products-grid').show();

            if (flag_send_ajax) {
                $.ajax({
                    url: action_to_url,
                    type: 'POST',
                    dataType: 'json',

                    complete: function (result = false) {},

                    success: function (response = false) {
                        let result                = response,
                            list_products_html    = '',
                            modifier_html_product = '',
                            not_select_class      = 'product-item__btn-like btn-like js-product-btn-like',
                            current_select_class  = '';

                        if (result && typeof (result) === "object") {
                            if (result['products'] !== undefined && result['products'].length) {
                                for (let product of result['products']) {
                                    (result['products_length'] !== undefined && +result['products_length']) ? length_items = +result['products_length'] : 0;

                                    if (product.image !== undefined && product.href !== undefined) {
                                        current_select_class = (findSomeSelectedId(product.product_id)) ? 'product-item__btn-like btn-like js-product-btn-like btn-like--selected' : not_select_class;

                                        list_products_html += '<li class="product-item product-item--catalog product-item" data-product-id="' + product.product_id + '">' +
                                            '<a class="' + current_select_class + '" href="javascript:void(0);">' +
                                            '<i class="far fa-heart"></i>' +

                                            '<i class="fas fa-heart"></i>' +
                                            '</a>';

                                        if (result['title_product_liked']) {
                                            list_products_html +=
                                                '                    <figure class="small-alert-info">' + result['title_product_liked'] + '<span></span></figure>';
                                        }

                                        if (result['products_new'] !== undefined) {
                                            for (let product_item of result['products_new']) {
                                                if (product_item.product_id !== undefined && product_item.product_id !== false) {
                                                    if (+product.product_id === +product_item.product_id) {
                                                        modifier_html_product = '<div class="product-item__new"><span>New</span></div>';
                                                    }
                                                }
                                            }
                                        }

                                        list_products_html += modifier_html_product;

                                        list_products_html += '    <a href="' + product.href + '">' +
                                            '                          <img class="product-item__img" src="/image/' + product.image + '" alt="' + product.title + '" title="' + product.title + ' ' + product.model + '"/>' +
                                            '                      </a>';

                                        if (product.sale_price !== null) {
                                            list_products_html += '<div class="product-item__price">' +
                                                '                      <div class="price">' +
                                                '                          <div class="price__number">' + +product.sale_price + '</div>' +

                                                '                          <strike>' + +product.price + '</strike>' +
                                                '                          </div>' +
                                                '                      </div>';
                                        } else {
                                            list_products_html += '<div class="product-item__price">' +
                                                '                      <div class="price">' +
                                                '                          <div class="price__number">' + +product.price + '</div>' +
                                                '                          </div>' +
                                                '                      </div>';
                                        }

                                        list_products_html += '        <a href="' + product.href  + '" class="product-item__title">' +
                                            product.title +

                                            '           <br/>' +

                                            '            <strong>' + product.model + '</strong>' +
                                            '       </a>';

                                        if (product.code !== null) {
                                            list_products_html += '    <span class="product-item__code">' + product.code + '</span>';
                                        }

                                        list_products_html += '        <button class="product-item__btn-buy btn js-product-btn-buy">В корзину</button>' +
                                            '                       </li>';

                                        modifier_html_product = '';
                                        current_select_class  = not_select_class;
                                    }
                                }

                                $('.catalog-products-list').html(list_products_html);

                                setTimeout(removeGeneratePreloader, 1500);

                                if (debug) console.log(response);
                            } else {
                               $('.catalog-products-list, .catalog-products-grid__pagination').hide();

                                setTimeout(function () {
                                    removeGeneratePreloader();

                                    if ($('.alert-info').hasClass('hide')) {
                                        $('.catalog-products-grid, .alert-info').show();

                                        setTimeout(function () {
                                            $('.alert-info').removeClass('hide');
                                        }, 350);
                                    } else $('.catalog-products-grid, .alert-info').show();
                                }, 3000);
                            }

                            flag_filter = true;
                        }

                        if (
                            $('.catalog-sorts-current__content').children().length >= 2 &&
                            $('.catalog-sorts-current__content-item--search').length &&
                            result['search_val'] && result['search_title']
                        ) {
                            $('.catalog-sorts-current__content-item--search .title').text(result['search_title']);
                            $('#js-catalog-current-sort-search').text(result['search_val']);
                        }

                        if (clicked_pagination_item) {
                            scrollToProductsContent();

                            clicked_pagination_item = false;
                        }

                        // if (true) console.log('result', result, 'type', result['type']);
                    },

                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }
    }

    function findSomeSelectedId(product_id = false) {
        var wish_list = JSON.parse(localStorage.getItem('selected_products'));

        if (product_id !== false && localStorage.getItem('selected_products') !== null) {
            for (var id of wish_list) {
                if (+product_id === +id) return +product_id;
            }
        }

        return false;
    }

    function getParamsInWishList() {
        var data_wish_list   = localStorage.getItem('selected_products'),
            params_wish_list = '';

        if (data_wish_list !== null && JSON.parse(data_wish_list).length) {
            for (var id of JSON.parse(data_wish_list)) {
                params_wish_list += 'wish_list[]=' + id;
            }
        } else params_wish_list = 'wish_list[]=';

        return params_wish_list;
    }

    function removeGeneratePreloader() {
        data_all_active_filters = [
            filter_min_price,
            filter_max_price,
            filter_holidays,
            filter_sort,
            filter_color,
            filter_sex,
            grid_items,
            length_items,
            sort_by_new,
            sort_by_popular,
            sort_by_price_asc,
            sort_by_price_desc,
            sort_by_sale_price,
            sort_by_liked,
            val_to_search,
            filter_attribute_id,
            current_active_item
        ];

        if ($('.catalog-products-grid__pagination.pagination-controllers').length) {
            if (+length_items) {

                $('.product-item').each(function () {
                    if ($(this).find('strike').length && !$(this).find('.product-item__new').length) {
                        if ( !$(this).parent().hasClass('product-item--present') && !$(this).find('.product-item__sale').length ) {
                            $(this).sumSale({
                                'oldPrice': +$(this).find('strike').text(),
                                'newPrice': parseInt($(this).find('.price__number').html()),
                                'classNameSale': 'product-item__sale'
                            });
                        }
                    }
                });

                $('.catalog-products-grid__pagination.pagination-controllers').html(generatePagination(grid_items, length_items, (current_active_item > 0) ? current_active_item : 1, 'catalog-products-grid'));
            } else $('.catalog-products-grid__pagination.pagination-controllers').html('');
        }

        $('.catalog-products-grid__content').removeClass('catalog-products-grid__content--load');
        $('.catalog-products-grid__preloader').addClass('hide');
        $('.catalog-content__section-right').removeClass('catalog-content__section-right--load');
    }

    function generatePagination(grid = false, length = false, create_active_item = false, class_name_grid = false) {
        var html_pagination        = '',
            default_class_name_itm = 'pagination__item',
            class_item_pagination  = default_class_name_itm,
            length_page            = 0,
            step                   = 1,
            step_generate          = 1,
            active_btn_controllers = '',
            max_count_items_nmb    = (window.outerWidth > 600) ? 4 : 2,
            inner_text             = '';

        if (class_name_grid) {
            current_active_item = create_active_item;

            if (length) length_page = Math.ceil(length / grid);

            if (length_page - 1) {
                active_btn_controllers = (create_active_item > 1) ? '' : 'disabled="disabled"';

                html_pagination =
                    '<div class="' + class_name_grid + '__pagination pagination-controllers">' +
                    '   <button class="' + class_name_grid + '__pagination-btn-prev btn-prev" ' + active_btn_controllers + '>' +
                    '       <i class="zmdi zmdi-long-arrow-left"></i>' +
                    '   </button>' +

                    '   <ul class="pagination">';

                if (create_active_item === 1) class_item_pagination += ' pagination__item--active';

                html_pagination +=
                    '    <li>' +
                    '        <a class="' + class_item_pagination + ' show-mobile' + '" href="javascript:void(0);">' + 1 + '</a>' +
                    '    </li>';
                if (class_item_pagination !== default_class_name_itm) class_item_pagination = default_class_name_itm;

                if (length_page > 2) {
                    inner_text = ( (current_active_item - max_count_items_nmb) > 0 ) ? '...' : 2;
                    class_item_pagination += ( (current_active_item - max_count_items_nmb) > 0 ) ? ' pagination__item--dots' : '';

                    if (create_active_item === 2) class_item_pagination += ' pagination__item--active';

                    // console.log('step 1', current_active_item);

                    html_pagination +=
                        '    <li>' +
                        '        <a class="' + class_item_pagination + ' show-mobile' + '" href="javascript:void(0);">' + inner_text + '</a>' +
                        '    </li>';
                    ++step;
                    if (class_item_pagination !== default_class_name_itm) class_item_pagination = default_class_name_itm;
                }


                if ( (current_active_item + max_count_items_nmb) > length_page ) {
                    current_active_item = length_page - max_count_items_nmb;
                }

                ////
                for (var i = (current_active_item > max_count_items_nmb) ? current_active_item : 3; step_generate < max_count_items_nmb; i++) {
                    if (window.outerWidth <= 600) class_item_pagination += ' show-mobile';

                    if (i === create_active_item) class_item_pagination += ' pagination__item--active';

                    if (i < length_page) {
                        html_pagination +=
                            '    <li>' +
                            '        <a class="' + class_item_pagination + '" href="javascript:void(0);">' + i + '</a>' +
                            '    </li>';
                    }

                    class_item_pagination = 'pagination__item';
                    ++step_generate;
                }
                ++step;
                if (class_item_pagination !== default_class_name_itm) class_item_pagination = default_class_name_itm;
                ////

                if (step < length_page && step !== length_page - 1) {
                    inner_text = ( (current_active_item + max_count_items_nmb) < length_page ) ? '...' : length_page - 1;
                    class_item_pagination += ( (current_active_item + max_count_items_nmb) < length_page ) ? ' pagination__item--dots' : '';

                    if (create_active_item === length_page - 1) class_item_pagination += ' pagination__item--active';

                    html_pagination +=
                        '    <li>' +
                        '        <a class="' + class_item_pagination + ' show-mobile' + '" href="javascript:void(0);">' + inner_text + '</a>' +
                        '    </li>';
                    if (class_item_pagination !== default_class_name_itm) class_item_pagination = default_class_name_itm;
                }


                if (create_active_item === length_page) class_item_pagination += ' pagination__item--active';
                html_pagination +=
                    '    <li>' +
                    '        <a class="' + class_item_pagination + ' show-mobile' + '" href="javascript:void(0);">' + length_page + '</a>' +
                    '    </li>';

                active_btn_controllers = (create_active_item < length_page) ? '' : 'disabled="disabled"';

                html_pagination +=
                    '    </ul>'  +
                    '        <button class="' + class_name_grid + '__pagination-btn-next btn-next" ' + active_btn_controllers + '>' +
                    '            <i class="zmdi zmdi-long-arrow-right"></i>' +
                    '        </button>' +
                    '</div>';

                return html_pagination;
            }
        }

        return false;
    }
    //END: Catalog
});
