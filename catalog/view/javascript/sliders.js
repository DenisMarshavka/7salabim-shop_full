$(document).ready(function () {
    //START: Home-page owl sliders
    //START: Slider header
    var $owl_products_head_slider = $('#js-header-sliders');

    $owl_products_head_slider.owlCarousel({
        animateIn: 'fadeIn',
        items: 1,
        loop: true,
        autoplay: false,
        autoplayTimeout: 0,
        responsive: {
            767: {
                animateOut: 'fadeOut',
                autoplay: true,
                autoplayTimeout: 8000,
                onChange: stopBannerAutoplay,
                onChanged: continueBannerAutoplay,
            }
        },
    });


    $('.js-header-slider-btn-next').click(function() {
        $owl_products_head_slider.trigger('next.owl.carousel');
    });

    $('.js-header-slider-btn-prev').click(function() {
        $owl_products_head_slider.trigger('prev.owl.carousel');
    });

    function continueBannerAutoplay() {
        $owl_products_head_slider.trigger('play.owl.autoplay');
    }

    function stopBannerAutoplay() {
        $owl_products_head_slider.trigger('stop.owl.autoplay');
    }
    //END: Slider header

    //Sale products
    var $owl_products_sale = $('#js-slider-products-sale');

    $owl_products_sale.owlCarousel({
        items: 1,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        slideTransition: 'linear',
    });


    $('#js-slider-products-sale-btn-next').click(function() {
        $owl_products_sale.trigger('next.owl.carousel');
    });

    $('#js-slider-products-sale-btn-prev').click(function() {
        $owl_products_sale.trigger('prev.owl.carousel');
    });
    // Sale products

    //Popular products
    var $owl_products_popular = $('#js-slider-products-popular');

    $owl_products_popular.owlCarousel({
        loop: true,
        responsive: {
            0: {
                items: 2,
                margin: 20,
            },
            375: {
                items: 2,
                margin: 30,
            },
            680: {
                items: 3,
                margin: 30,
            },
            768: {
                items: 4,
                margin: 40,

            }
        }
    });


    $('#js-slider-products-popular-btn-next').click(function() {
        $owl_products_popular.trigger('next.owl.carousel');
    });

    $('#js-slider-products-popular-btn-prev').click(function() {
        $owl_products_popular.trigger('prev.owl.carousel');
    });
    //Popular products


    //New products
    var $owl_products_new = $('#js-slider-products-new');

    $owl_products_new.owlCarousel({
        animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        loop: true,
        responsive: {
            0: {
                items: 1,
                margin: 20,
            },
            400: {
                items: 2,
                margin: 30,
            },
            768: {
                items: 3,
                margin: 40,
            }
        }
    });


    $('#js-slider-products-new-btn-next').click(function() {
        $owl_products_new.trigger('next.owl.carousel');
    });

    $('#js-slider-products-new-btn-prev').click(function() {
        $owl_products_new.trigger('prev.owl.carousel');
    });
    //New products
    //END: Home-page owl sliders

    //SATRT: Catalog-page owl sliders
    //Similar products
    var $owl_products_similar = $('#js-slider-products-similar');

    $owl_products_similar.owlCarousel({
        responsive: {
            0: {
                items: 2,
                margin: 15,
            },
            375: {
                items: 2,
                margin: 30,

            },
            1200: {
                items: 3,
                margin: 40,

            }
        }
    });


    $('#js-slider-products-similar-btn-next').click(function() {
        $owl_products_similar.trigger('next.owl.carousel');
    });

    $('#js-slider-products-similar-btn-prev').click(function() {
        $owl_products_similar.trigger('prev.owl.carousel');
    });
    //Similar products
    //END: Catalog-page owl sliders

    //SATRT: Home-page Ccard products owl sliders
    //Buying products
    var $owl_products_buying = $('#js-slider-products-buying');

    $owl_products_buying.owlCarousel({
        responsive: {
            0: {
                items: 2,
                margin: 15,
            },
            375: {
                items: 2,
                margin: 15,

            },
            768: {
                items: 3,
                margin: 30,

            },
            1200: {
                items: 4,
                margin: 30,

            }
        }
    });


    $('#js-slider-products-buying-btn-next').click(function() {
        $owl_products_buying.trigger('next.owl.carousel');
    });

    $('#js-slider-products-buying-btn-prev').click(function() {
        $owl_products_buying.trigger('prev.owl.carousel');
    });
    //Buying products

    //Similar products
    var $owl_products_similar_card = $('#js-slider-products-similar-card');

    $owl_products_similar_card.owlCarousel({
        responsive: {
            0: {
                items: 2,
                margin: 15,
            },
            375: {
                items: 2,
                margin: 15,

            },
            768: {
                items: 3,
                margin: 30,

            },
            1200: {
                items: 4,
                margin: 30,

            }
        }
    });


    $('#js-slider-products-similar-card-btn-next').click(function() {
        $owl_products_similar_card.trigger('next.owl.carousel');
    });

    $('#js-slider-products-similar-card-btn-prev').click(function() {
        $owl_products_similar_card.trigger('prev.owl.carousel');
    });
    //Similar products
    //END: Home-page Ccard products owl sliders
    $('.light-text').each(function () {
        if ($(this).next('p').find('span') && $(this).next('p').find('span').attr('style') == 'color: rgb(64, 83, 111); font-family: Lato-Regular, sans-serif; font-size: 16px;') $(this).html( $(this).next('p').find('span').text() );
    });

    $('p').each(function () {
        if ($.trim($(this).text()) == '') $(this).remove();

        if ($(this).find('span')) {
            if ($(this).find('span').attr('style') == 'color: rgb(64, 83, 111); font-family: Lato-Regular, sans-serif; font-size: 16px;') {
                $(this).remove();
            }
        }
    });
});

function getURLVar(key) {
    var value = [];

    var query = String(document.location).split('?');

    if (query[1]) {
        var part = query[1].split('&');

        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');

            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }

        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}
