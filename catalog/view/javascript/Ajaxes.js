$(document).ready(function () {
    //START: Specifications
    //Alert Cookie
    var current_status_readed_cookie = false;
    var payment_method = 'Cash';

    checkCookieStatus();

    if (current_status_readed_cookie) $('#alert-cookie').addClass('hide');

    $(document).on('click', '#js-alert-cookie-btn', function (event) {
        event.preventDefault();
        event.stopPropagation();

        localStorage.setItem('cookie_approve', 'true');
        $(this).parents('.alert-cookie').addClass('hide');

        checkCookieStatus();
    });

    $(window).scroll(function () {
        if (!current_status_readed_cookie) {
            if ( (window.pageYOffset + window.outerHeight) >= $('html').height()) {
                $('.alert-cookie').addClass('hide');
            } else $('.alert-cookie.hide').removeClass('hide');
        }
    });

    function checkCookieStatus() {
        current_status_readed_cookie = localStorage.getItem('cookie_approve') !== null && localStorage.getItem('cookie_approve') === 'true';
    }
    //Alert Cookie

    //Change language
    var flag_to_scroll      = true,
        change_language_url = window.location.origin;

    $(document).on('click', '#js-header-language-list button, .js-header-language-list button', function () {
        if ($(this).attr('value')) {
            if (!$(this).hasClass('header-section-top__nav-language-item--active') && !$(this).hasClass('header-head__language-item--active')) {
                change_language_url += '/index.php?route=setting/setting/getChangelanguage&code=' + $(this).attr('value');

                $.ajax({
                    type: 'GET',
                    url: change_language_url,
                    dataType: 'json',

                    complete: function () {
                        deleteCookie('language');
                        deleteCookie('currency');
                        deleteCookie('OCSESSID');

                        window.location.reload();
                    }
                });
            }
        }
    });

    function setCookie(key, value) {
        var expireDate = new Date();

        document.cookie = key + "=" + escape(value) +
            ";domain=" + window.location.hostname +
            ";path=/" +
            ";expires=" + expireDate.toUTCString();
    }

    function deleteCookie(name) {
        setCookie(name, "", null , null , null, 1);
    }
    //Change language

    //Forms validate
    function checkForm(arr_regex = [], arr_val = [], form_name = '') {
        var regexes             = [],
            alert_error_html    = '<span class="text-error">',
            values              = [],
            str_error           = '';

        if (arr_regex && arr_val) {
            regexes = arr_regex;
            values = arr_val;

            if (regexes.length && values.length && form_name) {
                for (var r = 0; r < regexes.length; r++) {
                    if ( $.trim(regexes[r]['regex']) && $.trim(values[r]) && $.trim(regexes[r]['name']) ) {
                        if ( !regexes[r]['regex'].test(values[r]) ) {
                            str_error = 'error_' + regexes[r]['name'];

                            if ($(form_name + " [name='" + $.trim(regexes[r]['name']) + "']").length) {
                                $(form_name + " [name='" + $.trim(regexes[r]['name']) + "']").addClass('error');

                                if ($(form_name + " [name='" + $.trim(regexes[r]['name']) + "']").data('error') !== undefined) {
                                    $(alert_error_html += $.trim($(form_name + " [name='" + $.trim(regexes[r]['name']) + "']").data('error')) + '</span>').insertBefore(form_name + ' button').slideDown(500);
                                }
                            }
                            return str_error;
                        }
                    }
                }

                return true;
            }
        }

        return 'ERROR';
    }

    function sendAjaxForm(action_to_url = '', form = '', debug = false, result_check_validation = '', product_id = false) {
        var alert_error_html   = '<span class="text-error">',
            alert_success_html = '<span class="text-success">',
            rule_to_send       = true,
            is_add_review      = ( product_id !== false && (form.indexOf('reviews') > -1) || form.indexOf('questions') > -1 );

        $(form + ' .form-input-box.security input').each(function () {
            if ( $.trim($(this).val()) !== '' ) rule_to_send = false;
        });

        if (action_to_url && form && rule_to_send) {
            if (form) $(form).find('button').attr('disabled', true);

            if (product_id !== false) action_to_url += '&product_id=' + product_id;

            console.log('sendAjaxForm', action_to_url);

            $.ajax({
                url: action_to_url,
                type: 'POST',
                dataType: 'html',

                complete: function () {
                    if (debug && result_check_validation) console.log(result_check_validation, action_to_url);
                },

                success: function(response = false) {
                    if ($.trim(response)) {
                        var result = JSON.parse(response);

                        if (!$(form + ' .text-error').length) {
                            if (result && typeof(result) === "object") {
                                if (result['type'] !== undefined && result['type'] !== 'fatal') {
                                    if ($(form + ' ' + '[name="' + result['type'] + '"]').length) $(form + ' ' + '[name="' + result['type'] + '"]').addClass('error');

                                    console.log(form + ' ' + '[name="' + result['type'] + '"]', $(form + ' ' + '[name="' + result['type'] + '"]').length);

                                    alert_error_html+= result['text'];
                                    alert_error_html+= '</span>';

                                    $(alert_error_html).insertBefore(form + ' button');
                                    $(form + ' ' + '.text-error').slideDown(500);
                                    $(form).find('button').attr('disabled', false);
                                } else if (result['result'] !== undefined && result['result']) {
                                    $('#js-popup').addClass('hide');
                                    $(form).addClass('hide');

                                    $(form).find('.form-input-box__field').each(function () {
                                        $(this).val('');
                                    });

                                    changePopup(false);
                                    $(form).find('button').attr('disabled', false);
                                } else if (result['success'] !== undefined && result['success']) {
                                    //For reviews
                                    alert_success_html+= result['success'] + '</span>';

                                    (is_add_review) ? $(alert_success_html).insertBefore(form + ' button') : $(alert_success_html).insertAfter(form);
                                    if (is_add_review) {
                                        $(form + ' ' + '.text-success').slideDown(500)
                                    }  else {
                                        $('.text-success').slideDown(500, function () {
                                            $(this).css('display', 'block');
                                        });
                                    }

                                    $(form).find('.form-input-box__field').each(function () {
                                        $(this).val('');
                                    });

                                    $(form).find('button').attr('disabled', true);

                                    setTimeout(function () {
                                        if (product_id !== false) $(form + ' ' + '.text-success').slideUp(500);

                                        $(form).find('button').attr('disabled', false);
                                    }, 5000);

                                    setTimeout(function () {
                                        $( (is_add_review) ? form + ' ' + '.text-success' : '.text-success' ).slideUp(500);
                                    }, 5450);

                                    setTimeout(function () {
                                        if (!$('#js-popup').hasClass('hide')) {
                                            $('#js-popup').addClass('hide');
                                            changePopup(false);
                                        }

                                        $( (is_add_review) ? form + ' ' + '.text-success' : '.text-success' ).remove();
                                    }, 6000);

                                    if (is_add_review) {
                                        setTimeout(function () {
                                            $(form).addClass('hide');
                                        }, 7000);
                                    }
                                } else if (result['error'] !== undefined && result['error']) {
                                    console.error(result['error']);
                                }
                            }
                        }

                        if (debug) console.log('result', result, 'type', result['type']);
                    } else console.warn('Warning of the response: The response is empty!');
                },

                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    }
    //Forms validate

    //Search forms
    $(document).on('keyup click', '#js-search-mobile input, #js-search-desktop input, #js-search-desktop button, #js-search-mobile button', function (event) {
        if (event.keyCode === 13 || $(this)[0]['type'] === 'submit') {
            if ($.trim( $(this).parents('form').find('input[name*="search"]').val() ).length > 3) {
                event.preventDefault();
                event.stopPropagation();

                window.location.href = window.location.origin + '?route=product/catalog&search_val=' + $.trim( $(this).parents('form').find('input[name*="search"]').val() );
            }
        }
    });
    //Search forms

    //Pop-Up
    function changePopup(is_open = true) {
        $('html, body').css((is_open) ? {overflowY: 'hidden'} : {overflowY: 'auto'});

        if (window.outerWidth <= 767) $('html, body').css((is_open) ? {position: 'fixed'} : {position: 'relative'});
    }
    //Pop-Up

    $(window).scroll(function (event) {
        if (!flag_to_scroll) {
            event.preventDefault();
            event.stopPropagation();
        }
    });
    //END: Specifications

    //START: Forms
    //Questions
    $(document).on('click', '.popup-form--questions button', function (event) {
        var arr_regex     = [],
            arr_val       = [],
            val_name      = $.trim($('.popup-form--questions input[name="name"]').val()),
            val_phone     = ( $.trim($('.popup-form--questions input[name="phone"]').val()) ) ? $.trim($('.popup-form--questions input[name="phone"]').val()) : false,
            val_email     = ( $.trim($('.popup-form--questions input[name="email"]').val()) ) ? $.trim($('.popup-form--questions input[name="email"]').val()) : false,
            val_question  = $.trim($('.popup-form--questions textarea[name="question"]').val()),
            ajax_url_data = '',
            result,
            action_to_url = location.origin;

        if ($.trim($('.popup-form--questions input[name="name"]').val()) && $.trim($('.popup-form--questions textarea[name="question"]').val())) {
            event.preventDefault();

            arr_val.push(val_name);
            arr_regex.push({
                'regex': /^[A-ZА-ЯҐЄІЇЁ`]+[a-zа-яґєіїё`]{2,25}\s*[A-ZА-ЯҐЄІЇЁ`]*[a-zа-яґєіїё`]*$/u,
                'name': 'name'
            });

            ajax_url_data = '&name=' + val_name;

            if (val_phone) {
                arr_val.push(val_phone);
                arr_regex.push({
                    'regex': /^\+*\d{10,12}$/,
                    'name': 'phone'
                });

                ajax_url_data += '&phone=' + val_phone;
            }

            if (val_email) {
                arr_val.push(val_email);
                arr_regex.push({
                    'regex': /^[\S.]{4,}@[0-9]*[a-zA-Z]{2,}\.[a-zA-Z]{2,6}$/m,
                    'name': 'email'
                });

                ajax_url_data += '&email=' + val_email;
            }

            arr_val.push(val_question);
            arr_regex.push({
                'regex': /^.{10,500}$/,
                'name': 'question'
            });

            ajax_url_data += '&question=' + val_question;

            result = checkForm(arr_regex, arr_val, '.popup-form--questions');

            action_to_url += '/index.php?route=extension/module/questions/add';
            action_to_url += (result === true) ? ajax_url_data : '&error_valid=' + result;

            sendAjaxForm(action_to_url, '.popup-form--questions', true);
        }
    });
    //Questions

    //Reviews
    $(document).on('click', '.popup-form--reviews button', function (event) {
        var arr_regex        = [],
            arr_val          = [],
            val_name         = $.trim($('.popup-form--reviews input[name="name"]').val()),
            val_limitations  = ( $.trim($('.popup-form--reviews input[name="limitations"]').val()) ) ? $.trim($('.popup-form--reviews input[name="limitations"]').val()) : false,
            val_priority     = ( $.trim($('.popup-form--reviews input[name="priority"]').val()) ) ? $.trim($('.popup-form--reviews input[name="priority"]').val()) : false,
            val_message      = $.trim($('.popup-form--reviews textarea[name="message"]').val()),
            ajax_url_data    = '',
            result,
            action_to_url = location.origin;

        if (val_name && val_message) {
            event.preventDefault();

            arr_val.push(val_name);
            arr_regex.push({
                'regex': /^[A-ZА-ЯҐЄІЇЁ`]+[a-zа-яґєіїё`]{2,25}\s*[A-ZА-ЯҐЄІЇЁ`]*[a-zа-яґєіїё`]*$/u,
                'name': 'name'
            });

            ajax_url_data = '&name=' + val_name;

            if (val_limitations) {
                arr_val.push(val_limitations);
                arr_regex.push({
                    'regex': /^.{10,255}$/,
                    'name': 'limitations'
                });

                ajax_url_data += '&limitations=' + val_limitations;
            }

            if (val_priority) {
                arr_val.push(val_priority);
                arr_regex.push({
                    'regex': /^.{10,255}$/,
                    'name': 'priority'
                });

                ajax_url_data += '&priority=' + val_priority;
            }

            arr_val.push(val_message);
            arr_regex.push({
                'regex': /^.{10,1000}$/,
                'name': 'message'
            });

            ajax_url_data += '&message=' + val_message;

            result = checkForm(arr_regex, arr_val, '.popup-form--reviews');

            action_to_url += '/index.php?route=product/product/write';
            action_to_url += (result === true) ? ajax_url_data : '&error_valid=' + result;

            if ($('.card-section-right').data('product-id') !== undefined) {
                sendAjaxForm(action_to_url, '.popup-form--reviews', true, result, +$('.card-section-right').data('product-id'));
            } else console.error('The product id is undefined! :-(');
        }
    });

    //Get reviews
    var status_open_reviews = false,
        product_id          = false;

    $(document).on('click', '.js-card-tab-reviews-btn, #js-card-product-tabs-btn-more', function (debug = false) {
        var action_to_url     = location.origin + '/index.php?route=product/product/review',
            $tab_reviews      = $('.card-tabs-reviews-list'),
            html_reviews      = '',
            length_reviews    = $tab_reviews.find('.card-tabs-reviews-list__item').length,
            review_id         = false,
            class_name_review = (length_reviews >= 5) ? 'card-tabs-reviews-list__item hide' : 'card-tabs-reviews-list__item';


        if (!$(this).hasClass('js-card-tab-reviews-btn')) status_open_reviews = false;

        if ($('.card-section-right').data('product-id') !== undefined) {
            product_id     = +$('.card-section-right').data('product-id');
            action_to_url += '&product_id=' + product_id;
            action_to_url += '&reviews_offset=' + length_reviews;
        }

        console.log(status_open_reviews, $(this).data('status-opened') !== undefined, product_id, $(this).hasClass('js-card-tab-reviews-btn'), !$(this).hasClass('js-card-tab-reviews-btn'));

        if (product_id) {
            if (!status_open_reviews) {
                status_open_reviews = true;

                $.ajax({
                    url: action_to_url,
                    type: 'POST',
                    dataType: 'html',

                    complete: function () {
                        if (debug) console.log(action_to_url);
                    },

                    success: function (response = false) {
                        var result             = (response.indexOf('error') <= -1) ? JSON.parse(response) : false,
                            count_like            = '',
                            count_unlike          = '',
                            attr_active_like      = '',
                            attr_active_unlike    = '',
                            class_name_btn_like   = 'card-tabs-reviews-list__system-like-btn card-tabs-reviews-list__system-like-btn--like js-btn-like-review',
                            class_name_btn_unlike = 'card-tabs-reviews-list__system-like-btn card-tabs-reviews-list__system-like-btn--unlike js-btn-like-review';

                        if (result && typeof (result) === "object") {
                            if (result['reviews'] !== undefined && result['reviews'].length) {
                                for (var review_item of result['reviews']) {
                                    if (review_item.id !== undefined) review_id = +review_item.id;

                                    html_reviews += '<li class="' + class_name_review + '" data-review-id="' + review_id + '">' +
                                        '                <div class="card-tabs-reviews-list__section-head">';

                                        if (review_item.author !== undefined && review_item.author) {
                                            html_reviews += '<span class="card-tabs-reviews-list__user-name title">' + review_item.author + '</span>';
                                        }

                                        if (result['module_like_review_status'] !== undefined && +result['module_like_review_status'] && review_item.likes_info !== undefined) {
                                            count_like = (+review_item.likes_info.count_like) ? +review_item.likes_info.count_like : '';
                                            count_unlike = (+review_item.likes_info.count_unlike) ? +review_item.likes_info.count_unlike : '';

                                            if (review_item.likes_info.your_event !== undefined && review_item.likes_info.your_event !== false && review_item.likes_info.is_like !== undefined) {
                                                if (review_item.likes_info.is_like) {
                                                    attr_active_like = 'disabled="disabled"';
                                                    class_name_btn_like += ' active';
                                                } else {
                                                    attr_active_unlike = 'disabled="disabled"';
                                                    class_name_btn_unlike += ' active';
                                                }
                                            }

                                            html_reviews += '   <div class="card-tabs-reviews-list__system-like">' +
                                                '                        <button class="' + class_name_btn_like + '" ' + attr_active_like + '>' +
                                                '                            <i class="zmdi zmdi-thumb-up"></i>' +

                                                '                            <span>' + count_like + '</span>' +
                                                '                        </button>' +

                                                '                        <button class="' + class_name_btn_unlike + '" ' + attr_active_unlike + '>' +
                                                '                            <i class="zmdi zmdi-thumb-down"></i>' +

                                                '                            <span>' + count_unlike + '</span>' +
                                                '                        </button>' +
                                                '                    </div>' +
                                                '                </div>';

                                            attr_active_like   = '';
                                            attr_active_unlike = '';

                                            class_name_btn_like   = 'card-tabs-reviews-list__system-like-btn card-tabs-reviews-list__system-like-btn--like js-btn-like-review';
                                            class_name_btn_unlike = 'card-tabs-reviews-list__system-like-btn card-tabs-reviews-list__system-like-btn--unlike js-btn-like-review';
                                        } else {
                                            html_reviews += '   </div>';
                                        }

                                        if (review_item.date_added !== undefined && review_item.date_added) {
                                            html_reviews +='<div class="card-tabs-reviews-list__section-date">' +
                                            '                    <i class="zmdi zmdi-calendar-check"></i>' +

                                            '                    <span>' + review_item.date_added + '</span>' +
                                            '               </div>';
                                        }

                                        html_reviews += '<div class="card-tabs-reviews-list__section-content">';

                                        if (review_item.text !== undefined && review_item.text) {
                                            html_reviews += '<p class="light-text">' + review_item.text + '</p>';
                                        }

                                        html_reviews += '</div>' +

                                        '                <div class="card-tabs-reviews-list__section-more">';

                                        if (review_item.priority !== undefined && result.priority_title !== undefined && review_item.priority) {
                                            html_reviews += '<div class="card-tabs-reviews-list__section-more-priority">' +
                                                '                <h5 class="title">' + result.priority_title + '</h5>' +

                                                '                <span class="light-text">' + result['priority_title'] + '</span>' +
                                                '            </div>';
                                        }

                                        if (review_item.limitations !== undefined && result.priority_limitations !== undefined && review_item.limitations) {
                                            html_reviews += '<div class="card-tabs-reviews-list__section-more-limitations">' +
                                                '                <h5 class="title">' + result.priority_limitations + '</h5>' +

                                                '                <span class="light-text">' + review_item.limitations + '</span>' +
                                                '            </div>';
                                        }

                                        html_reviews +=' </div>' +
                                        '            </li>';
                                }

                                if (length_reviews) {
                                    $tab_reviews.append(html_reviews);
                                } else $tab_reviews.html(html_reviews);

                                length_reviews = $tab_reviews.find('.card-tabs-reviews-list__item').length;

                                if (result['reviews_total'] !== undefined) {
                                    if (+result['reviews_total'] > length_reviews) {
                                        if (!$('#js-card-product-tabs-btn-more').length) {
                                            $('.card-tabs-content__section-bottom').prepend(
                                                '<button class="card-tabs-content__btn btn btn-unique" id="js-card-product-tabs-btn-more" style="display: block">' + result.btn_more_title + '</button>'
                                            );

                                            setTimeout(() => {
                                                $('#js-card-product-tabs-btn-more').slideDown(500);
                                            }, 500);
                                        }
                                    } else {
                                        if ($('#js-card-product-tabs-btn-more').length) {
                                            $('#js-card-product-tabs-btn-more').attr('disabled', true).slideUp(500);

                                            setTimeout(() => {
                                                $('.card-tabs-reviews-list__item.hide').slideDown(500).removeClass('hide');
                                                $('#js-card-product-tabs-btn-more').remove();
                                            }, 500);
                                        }
                                    }
                                }

                            } else console.warn('The response of the reviews, is not defined or is empty :(');
                        } else console.error('The response of the reviews, not match JSON :(');
                    }
                });
            }
        } else console.error('A product id is not found :(');
    });
    //Get reviews

    //Like Review
    $(document).on('click', '.js-btn-like-review', function () {
        var review_id              = false,
            $_this                 = $(this),
            is_like                = $_this.hasClass('card-tabs-reviews-list__system-like-btn--like'),
            $current_review_like   = $_this.parents('.card-tabs-reviews-list__item').find('.card-tabs-reviews-list__system-like-btn--like'),
            $current_review_unlike = $_this.parents('.card-tabs-reviews-list__item').find('.card-tabs-reviews-list__system-like-btn--unlike'),
            action_to_url          = location.origin + '/index.php?route=extension/module/system_like_review/setLike',
            old_current_unlike     = 0,
            old_current_like       = 0,
            this_current_indicator = 0;

        if ($(this).parents('.card-tabs-reviews-list__item').data('review-id') !== undefined) review_id = +$(this).parents('.card-tabs-reviews-list__item').data('review-id');

        if (product_id !== false && review_id !== false) {
            action_to_url += '&product_id=' + product_id;
            action_to_url += '&review_id=' + review_id;
            action_to_url += '&is_like=' + +is_like;

            console.log(product_id, review_id, 'action_to_url', action_to_url);

            $.ajax({
                url: action_to_url,
                type: 'POST',
                dataType: 'html',

                complete: function () {
                    console.log(action_to_url);
                },

                success: function (response = false) {
                    var result = JSON.parse(response);

                    if (result && typeof(result) === "object") {
                        if (result['error'] !== undefined) {
                            console.error('Error response: ' + result['error']);
                        } else {
                            if (result.response !== undefined) {
                                $_this.parents('.card-tabs-reviews-list__item').find('.js-btn-like-review').removeClass('active');

                                $_this.addClass('active');

                                if (result.response.review_is_liked !== undefined && result.response.is_liked !== undefined ) {
                                    if (result.response.review_is_liked) {
                                        old_current_like = +$current_review_like.text();
                                        old_current_unlike = +$current_review_unlike.text();

                                        if (result.response.is_liked) {
                                            $current_review_like.find('span').text( (old_current_like - 1) ? --old_current_like : '' );

                                            $current_review_unlike.find('span').text(++old_current_unlike);
                                        } else {
                                            $current_review_unlike.find('span').text( (old_current_unlike - 1) ? --old_current_unlike : '' );

                                            $current_review_like.find('span').text(++old_current_like);
                                        }

                                        if ($_this.hasClass('card-tabs-reviews-list__system-like-btn--unlike') && $current_review_like.attr('disabled') !== undefined) {
                                            $current_review_like.removeAttr('disabled');
                                        }

                                        if ($_this.hasClass('card-tabs-reviews-list__system-like-btn--like') && $current_review_unlike.attr('disabled') !== undefined) {
                                            $current_review_unlike.removeAttr('disabled');
                                        }
                                    } else {
                                        this_current_indicator = +$_this.find('span').text();
                                        $_this.find('span').text( ++this_current_indicator );
                                    }

                                    $_this.attr('disabled', true);
                                } else console.error('Error response: Value a reviews_is_liked or a is_liked is not defined! ^(');
                            }
                        }
                    } else console.error('The response of the like reviews, not match JSON :(');
                }
            });
        } else console.error('A product id or review id, is not found :(');
    });
    //Like Review
    //Reviews

    //Wholesale order
    $(document).on('click', '.js-form-wholesale button', function (event) {
        var arr_regex        = [],
            arr_val          = [],
            val_name         = $.trim($('.js-form-wholesale input[name="name"]').val()),
            val_organization = $.trim($('.js-form-wholesale input[name="organization"]').val()),
            val_phone        = $.trim($('.js-form-wholesale input[name="phone"]').val()),
            val_count        = $.trim($('.js-form-wholesale input[name="count"]').val()),
            val_email        = ( $.trim($('.js-form-wholesale input[name="email"]').val()) ) ? $.trim($('.js-form-wholesale input[name="email"]').val()) : false,
            val_comments     = ( $.trim($('.js-form-wholesale input[name="comments"]').val()) ) ? $.trim($('.js-form-wholesale input[name="comments"]').val()) : false,
            ajax_url_data    = '',
            result,
            action_to_url = location.origin;

        if (val_name && val_organization && val_phone && val_count) {
            event.preventDefault();

            arr_val.push(val_name);
            arr_regex.push({
                'regex': /^[A-ZА-ЯҐЄІЇЁ`]+[a-zа-яґєіїё`]{2,25}\s*[A-ZА-ЯҐЄІЇЁ`]*[a-zа-яґєіїё`]*$/u,
                'name': 'name'
            });

            ajax_url_data = '&name=' + val_name;

            arr_val.push(val_organization);
            arr_regex.push({
                'regex': /^.{3,45}$/,
                'name': 'organization'
            });

            ajax_url_data += '&organization=' + val_organization;

            arr_val.push(val_phone);
            arr_regex.push({
                'regex': /^\+*\d{10,12}$/,
                'name': 'phone'
            });

            ajax_url_data += '&phone=' + val_phone;

            arr_val.push(val_count);
            arr_regex.push({
                'regex': /^\d{2,10}$/,
                'name': 'count'
            });

            ajax_url_data += '&count=' + val_count;

            if (val_email) {
                arr_val.push(val_email);
                arr_regex.push({
                    'regex': /^[\S.]{4,}@[0-9]*[a-zA-Z]{2,}\.[a-zA-Z]{2,6}$/m,
                    'name': 'email'
                });

                ajax_url_data += '&email=' + val_email;
            }

            if (val_comments) {
                arr_val.push(val_comments);
                arr_regex.push({
                    'regex': /^.{10,255}$/,
                    'name': 'comments'
                });

                ajax_url_data += '&comments=' + val_comments;
            }

            result = checkForm(arr_regex, arr_val, '.js-form-wholesale');

            action_to_url += '/index.php?route=product/product/sendMessageWholesale';
            action_to_url += (result === true) ? ajax_url_data : '&error_valid=' + result;

            if ($('.card-section-right').data('product-id') !== undefined) {
                sendAjaxForm(action_to_url, '.js-form-wholesale', true, result, +$('.card-section-right').data('product-id'));
            } else console.error('The product id is undefined! :-(');
        }
    });
    //Wholesale order

    //Payment
    var alert_error_html     = '<span class="text-error">',
        submit_payment_class = '.complete-order-confirm-section-pay__form-btn-submit',
        bottom_pay_html      = '',
        ajax_url_data        = '';

    //TODO: will modify the method
    // getIpInfo();
    //
    // function getIpInfo() {
    //     $.get("https://api.ipdata.co?api-key=test", function (response) {
    //         if (response) {
    //             if (response.city) ajax_url_data += '&city=' + response.city;
    //             if (response.country_code) ajax_url_data += '&country_code=' + response.country_code;
    //             if (response.ip) ajax_url_data += '&ip=' + response.ip;
    //             if (response.region_code) ajax_url_data += '&region_code=' + response.region_code;
    //         }
    //     }, "jsonp");
    // }

    if ($('input[name="card-number"]').length) $('input[name="card-number"]').mask("9999 9999 9999 9999");
    if ($('input[name="card-date"]').length) $('input[name="card-date"]').mask("99/99");

    $(document).on('click', '.complete-order-confirm-section-pay__form-btn-submit', function (event) {
        var arr_regex        = [],
            arr_val          = [],
            val_name         = $.trim($('.complete-order-content__section-left input[name="name"]').val()),
            val_phone        = $.trim($('.complete-order-content__section-left input[name="phone"]').val()),
            val_last_name    = ( $.trim($('.complete-order-content__section-left input[name="last-name"]').val()) ) ? $.trim($('.complete-order-content__section-left input[name="last-name"]').val()) : false,
            val_email        = ( $.trim($('.complete-order-content__section-left input[name="email"]').val()) ) ? $.trim($('.complete-order-content__section-left input[name="email"]').val()) : false,
            val_comments     = ( $.trim($('.complete-order-content__section-left textarea[name="comments"]').val()) ) ? $.trim($('.complete-order-content__section-left textarea[name="comments"]').val()) : false,
            $input_address   = $('.complete-order-content__section-left .form-input-row').not('.small').find('input[name*="address"]'),
            val_address      = $.trim($input_address.val()),
            exist_val_card   = $('.complete-order-content__order-pay .form-input-row').eq(1).hasClass('small'),
            val_card_num     = $.trim($('.complete-order-content__order-pay input[name="card-number"]').val()),
            val_card_date    = $.trim($('.complete-order-content__order-pay input[name="card-date"]').val()),
            val_card_cvv     = $.trim($('.complete-order-content__order-pay input[name="card-cvv"]').val()),
            card_name_person = $.trim($('.complete-order-content__order-pay input[name="card-person-name"]').val()),
            action_to_url    = location.origin,
            flag_action_form = false,
            initials_entered = false,
            result;


        if ( (payment_method === 'Card') ? val_name && val_phone && val_address && val_email : val_name && val_phone && val_address) {
            initials_entered = true;
            flag_action_form = true;
            event.preventDefault();

            // getIpInfo();

            ajax_url_data += ($input_address.attr('name') === 'np-address') ? '&shipping_company=Nova Poshta' : '&shipping_company=Courier';

            arr_val.push(val_name);
            arr_regex.push({
                'regex': /^[A-Za-zА-ЯҐЄІЇЁа-яґєіїё\u044F`]{3,25}$/u,
                'name': 'name'
            });

            ajax_url_data += '&name=' + val_name;

            if (val_last_name) {
                arr_val.push(val_last_name);
                arr_regex.push({
                    'regex': /^[A-Za-zА-ЯҐЄІЇЁа-яґєіїё\u044F`]{3,35}$/u,
                    'name': 'last-name'
                });

                ajax_url_data += '&last_name=' + val_last_name;

            }

            arr_val.push(val_phone);
            arr_regex.push({
                'regex': /^\+*\d{9,15}$/,
                'name': 'phone'
            });

            ajax_url_data += '&phone=' + val_phone;

            if (val_email) {
                arr_val.push(val_email);
                arr_regex.push({
                    'regex': /^[\S.]{4,}@[0-9]*[a-zA-Z]{2,}\.[a-zA-Z]{2,6}$/m,
                    'name': 'email'
                });

                ajax_url_data += '&email=' + val_email;
            }

            arr_val.push(val_address);
            arr_regex.push({
                'regex': /^.{10,100}$/,
                'name': $input_address.attr('name')
            });

            console.log($input_address.attr('name'));

            ajax_url_data += '&address=' + val_address;

            if (val_comments) {
                arr_val.push(val_comments);
                arr_regex.push({
                    'regex': /^.{10,250}$/,
                    'name': 'comments'
                });

                ajax_url_data += '&comments=' + val_comments;
            }
        }

        if (!exist_val_card && val_card_num && val_card_date && val_card_cvv && card_name_person && initials_entered) {
            flag_action_form = true;
            event.preventDefault();

            payment_method = 'Card';

            arr_val.push(val_card_num);
            arr_regex.push({
                'regex': /^\d{4}\s\d{4}\s\d{4}\s\d{4}\s?\d?$/,
                'name': 'card-number'
            });

            ajax_url_data += '&card_num=' + val_card_num;

            arr_val.push(val_card_date);
            arr_regex.push({
                'regex': /^(1[0-2]|0[1-9])\/(1[5-9]|2\d)$/,
                'name': 'card-date'
            });

            ajax_url_data += '&card_date=' + val_card_date;

            arr_val.push(val_card_cvv);
            arr_regex.push({
                'regex': /^\d{3}$/,
                'name': 'card-cvv'
            });

            ajax_url_data += '&card_cvv=' + val_card_cvv;

            arr_val.push( card_name_person.toUpperCase() );
            arr_regex.push({
                'regex': /^[a-zA-ZА-Яа-я\u044F`]{3,15}\s[a-zA-ZА-Яа-я\u044F`]{2,16}$/,
                'name': 'card-person-name'
            });

            ajax_url_data += '&card_name_person=' + card_name_person.toUpperCase();
        }

        result = checkForm(arr_regex, arr_val, '.complete-order-content');

        if ($('.js-complete-order-label[for="complete-form-radio-rec-token"]').data('select') === true) payment_method = 'customerToken';
        if (flag_action_form) ajax_url_data += '&payment_method=' + payment_method;

        action_to_url += '/index.php?route=extension/payment/mywayforpay';
        action_to_url += (result === true) ? ajax_url_data : '&error_valid=' + result;

        if (flag_action_form && payment_method) {
            // console.log('action_to_url222', action_to_url);

            changePopup();
            $('#preloader').removeClass('hide');

            $.ajax({
                url: action_to_url,
                type: 'POST',
                dataType: 'json',

                complete: function (result = false) {},

                success: function(response = false) {
                    var result            = response,
                        form_pay_section  = '.complete-order-content__section-left',
                        // style_html        = '',
                        error_payment_url = window.location.origin + '/index.php?route=error/payment';

                    console.log('result', result);

                    if (!$('.complete-order-content__section-left' + ' .text-error').length && !$('.complete-order-content__section-right' + ' .text-error').length) {
                        alert_error_html = '<span class="text-error">';

                        changePopup();
                        $('#preloader').removeClass('hide');

                        if (result && typeof(result) === "object") {
                            if (result['type'] !== undefined && result['type'] !== 'fatal') {
                                switch (result['type']) {
                                    case 'card-number':
                                        form_pay_section = '.complete-order-content__section-right';
                                        break;

                                    case 'card-date':
                                        form_pay_section = '.complete-order-content__section-right';
                                        break;

                                    case 'card-cvv':
                                        form_pay_section = '.complete-order-content__section-right';
                                        break;

                                    case 'card-person-name':
                                        form_pay_section = '.complete-order-content__section-right';
                                        break;
                                }

                                if (result['type'].indexOf('address') <= -1) {
                                    if ($(form_pay_section + ' ' + '[name="' + result['type'] + '"]').length) $(form_pay_section + ' ' + '[name="' + result['type'] + '"]').addClass('error');
                                } else {
                                    $input_address.addClass('error');
                                }

                                alert_error_html+= result['text'];
                                alert_error_html+= '</span>';

                                if (form_pay_section !== '.complete-order-content__section-right') {
                                    $(form_pay_section).append(alert_error_html);
                                } else $(alert_error_html).insertAfter('.complete-order-content__order-pay');

                                if (window.outerWidth > 767) {
                                    changePopup(false);
                                    $('#preloader').addClass('hide');
                                }

                                setTimeout(function () {
                                    $(form_pay_section + ' ' + '.text-error').css('display', 'block');

                                    if (window.outerWidth <= 767) {
                                        flag_to_scroll = false;

                                        // style_html = $('html').attr('style');
                                        $('html').removeAttr('style');

                                        changePopup(false);
                                        $('#preloader').addClass('hide');

                                        $('html, body').animate({
                                            scrollTop: $('.text-error').offset().top - window.outerHeight / 2
                                        }, 2000);

                                        setTimeout(function () {
                                            flag_to_scroll = true;
                                        }, 2000);
                                    }
                                }, 505);
                            }
                        }

                        if (result['result'] !== undefined) {
                            if (result['result'] === 'error') {
                                if (result['code'] !== undefined) error_payment_url += '&error_code=' + result['code'];

                                window.location.href = error_payment_url;
                            } else {
                                window.location.href = window.location.origin + '/index.php?route=checkout/success';

                                localStorage.setItem('added_products', JSON.stringify([]));
                            }
                        }
                    } else {
                        changePopup(false);
                        $('#preloader').addClass('hide');
                    }
                }
            });
        }

        ajax_url_data = '';
    });

    $(document).on('click', '.js-complete-order-label', function () {
        var is_input_email_required     = false,
            is_input_last_name_required = false,
            $this                       = $(this);

        if (!$(submit_payment_class).length) {
            $('.complete-order-confirm-section-pay__form').html(bottom_pay_html);

            $(submit_payment_class).slideDown('350');
        }

        // console.log($(this), $(this).attr('for'));

        if ($(this).attr('for') === 'complete-form-radio-rec-token') {
            ($(this).data('select')) ? $(this).attr('data-select', false) : $(this).attr('data-select', true);
        } else if ($(this).attr('for') === 'complete-form-radio-qr') {
            $('.complete-order-confirm-section-pay__form-btn-submit').slideUp('350');

            setTimeout(function () {
                $(submit_payment_class).removeAttr('style').css('display', 'none');

                bottom_pay_html = $('.complete-order-confirm-section-pay__form').html();
                $(submit_payment_class).remove();
            }, 400);
        } else {
            $('.js-complete-order-label[for="complete-form-radio-rec-token"]').attr('data-select', false);
        }

        setTimeout(function () {
            is_input_last_name_required = $('#complete-form-radio-delivery').prop('checked');

            // is_input_email_required = $('#complete-form-radio-np').prop('checked');
            is_input_email_required = $('#complete-form-radio-card').prop('checked');
            //
            // if (!is_input_email_required) is_input_email_required = $('#complete-form-radio-rec-token').prop('checked');

            changeInputsRequired(is_input_last_name_required, is_input_email_required);
        }, 250);
    });

    function changeInputsRequired(is_input_last_name_required = false, is_input_email_required = false) {
        var selector_input_last_name = 'input[name="last-name"]',
            selector_input_email     = 'input[type="email"]';

        // console.log('is_input_last_name_required', is_input_last_name_required, 'is_input_email_required', is_input_email_required);

        if (is_input_last_name_required) {
            $(selector_input_last_name).removeAttr('required');

            $(selector_input_last_name + ' + span').addClass('hide');
        } else {
            if ($(selector_input_last_name).length) {
                $(selector_input_last_name).attr('required', true);

                $(selector_input_last_name + ' + span').removeClass('hide');
            }
        }

        if (!is_input_email_required) {
            $(selector_input_email).removeAttr('required');

            $(selector_input_email + ' + span').addClass('hide');
        } else if ($(selector_input_email).length) {
            $(selector_input_email).attr('required', true);

            $(selector_input_email + ' + span').removeClass('hide');
        }
    }
    //Payment

    //Payment widget
    var wayforpay = null;

    if ($('.complete-order-content').length) {
        wayforpay = new Wayforpay();
    }

    window.addEventListener("message", function (event) {
        // console.log(event);
        if (event.data === "WfpWidgetEventApproved") {
            window.location.href = window.location.origin + '?route=checkout/success&success_widget=yess';
            localStorage.removeItem('added_products');
        } else if (event.data === 'WfpWidgetEventClose') {
            window.location.reload();
        }
    });

    var pay = function () {
        var $form               = $('form[name="wayforpay_submit"]'),
            form_products_name  = [],
            form_products_price = [],
            form_products_count = [];

        $form.find('input[name="productName[]"]').each(function () {
            form_products_name.push($(this).val());
        });

        $form.find('input[name="productPrice[]"]').each(function () {
            form_products_price.push($(this).val());
        });

        $form.find('input[name="productCount[]"]').each(function () {
            form_products_count.push($(this).val());
        });

        wayforpay.run({
                merchantAccount : $form.find('input[name="merchantAccount"]').val(),
                merchantDomainName : $form.find('input[name="merchantDomainName"]').val(),
                serviceUrl : $form.find('input[name="serviceUrl"]').val(),
                authorizationType :  $form.find('input[name="merchantAuthType"]').val(),
                merchantSignature : $form.find('input[name="merchantSignature"]').val(),
                orderReference : $form.find('input[name="orderReference"]').val(),
                orderDate : +$form.find('input[name="orderDate"]').val(),
                amount : +$form.find('input[name="amount"]').val(),
                currency : $form.find('input[name="currency"]').val(),
                productName : form_products_name,
                productPrice : form_products_price,
                productCount : form_products_count,
                language: $form.find('input[name="language"]').val(),
                straightWidget: true
            },

            function (response) {
                /*on approved*/
                // console.log(response);

                $.ajax({
                    url: location.origin + '/index.php?route=extension/payment/mywayforpay/callback',
                    type: 'POST',
                    data: response,
                    dataType: 'json',

                    success: function () {window.location.origin + '?route=checkout/success'},

                    // error: function(xhr, ajaxOptions, thrownError) {
                    //     alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    // }
                });
            },
            function(response) {/*on declined*/},
            function(response) {/*on processing*/}
        );
    };


    $(document).on('click', '#open-pay-widget', pay);
    //Payment widget
    //END: Forms

    //START: Basket
    var array_products   = [],
        str_data_product = '',
        step             = 1;

    getGenerateOrder();

    function getGenerateOrder(is_show_list = true) {
        if ($('.basket__section-box').length) {
            array_products = (localStorage.getItem('added_products') !== null) ? JSON.parse(localStorage.getItem('added_products')) : [];
            str_data_product = '';
            step             = 1;

            for (var i = 0; i < array_products.length; i++) {
                (step !== 1) ? str_data_product += '&' : false;

                str_data_product += step + 'id=' + array_products[i]['id'];
                str_data_product += '&' + step + 'count=' + array_products[i]['count'];

                if (array_products[i]['color'] !== undefined && array_products[i]['color'] !== 'default') str_data_product += '&' + step + 'color=' + array_products[i]['color'];

                step++;
            }

            $.ajax({
                url: '/index.php?route=product/basket/getList',
                type: 'POST',
                data: str_data_product,
                dataType: 'json',

                complete: function (response = false) {
                    if (response !== false && $.trim(response.responseText) == "Error, the basket is empty") {
                        getPreloaderFromEmptyBasket();
                    }
                },

                success: function(response = false) {
                    var html                      = '',
                        html_btn_calc_count_minus = '',
                        html_btn_calc_count_plus  = '',
                        product_title             = '',
                        product_count             = 1,
                        current_product_price     = 0,
                        depricated_products       = 0;

                    if (response.length && $('.basket-products-list').length && is_show_list) {
                        product_title = '';

                        for (var e = 0; e < response.length; e++) {
                            if (+response[e]['quantity'] > 0) {
                                product_title = $.trim(response[e]['name']) + ' ' + $.trim(response[e]['model']);
                                product_count = ( +response[e]['count'] <= +response[e]['quantity'] ) ? +response[e]['count'] : +response[e]['quantity'];

                                current_product_price = ( +response[e]['count'] <= +response[e]['quantity'] ) ? +response[e]['price'] * +response[e]['count'] : +response[e]['price'] * +response[e]['quantity'];

                                if (+response[e]['count'] > +response[e]['quantity']) response[e]['count'] = +response[e]['quantity'];

                                html_btn_calc_count_minus  = '<button class="product-count-calculator__btn product-count-calculator__btn--minus js-count-calculator-btn-minus"';
                                html_btn_calc_count_minus += (+response[e]['count'] <= 1) ? ' disabled="disabled"' : false;
                                html_btn_calc_count_minus += '>-</button>';

                                html_btn_calc_count_plus  = '<button class="product-count-calculator__btn product-count-calculator__btn--plus js-count-calculator-btn-plus"';
                                html_btn_calc_count_plus += (+response[e]['count'] >= +response[e]['quantity']) ? ' disabled="disabled"' : false;
                                html_btn_calc_count_plus += '>+</button>';

                                html += '<li class="basket-products-list__item" data-product-id="' + +response[e]['product_id'] + '">' +
                                    '<div class="basket-products-list__item-section-left">' +
                                    '<img class="basket-products-list__item-img basket-img" src="' + response[e]['image'] + '" alt="' + product_title + '" title="' + response[e]['name'] + '">' +

                                    '<div class="basket-products-list__item-description">' +
                                    '<a href="' + response[e]['href'] + '" class="basket-products-list__item-name title">' + product_title + '</a>' +

                                    '<div class="product-count-calculator">' +
                                    html_btn_calc_count_minus +

                                    '<span class="product-count-calculator__count-number js-count-calculator-indicator" data-max-count="' + +response[e]['quantity'] + '">' + product_count  + '</span>' +

                                    html_btn_calc_count_plus +
                                    '</div>' +
                                    '</div>' +
                                    '</div>' +

                                    '<div class="basket-products-list__item-section-right">' +
                                    '<span class="basket-products-list__item-price js-basket-product-price" data-price="' + +response[e]['price'] + '">' + current_product_price + '₴</span>' +

                                    '<button class="basket-products-list__item-btn-delete js-basket-product-btn-delete">' +
                                    '<i class="fas fa-times"></i>' +
                                    '</button>' +
                                    '</div>' +
                                    '</li>';
                            } else {
                                ++depricated_products;

                                if (depricated_products === response.length) {
                                    getPreloaderFromEmptyBasket();

                                    localStorage.setItem('added_products', JSON.stringify([]));
                                    $('.header__section-top-right-list-item--package').find('span[data-products]').remove();
                                    $('.head-nav-navigation__item head-nav-navigation__item--package').find('span[data-products]').remove();
                                }
                            }
                        }

                        $('.basket-products-list').html(html);

                        setTimeout(function () {
                            $('.basket__body').removeClass('hide');
                        }, 200);

                        setTimeout(function () {
                            $('.basket__preloader').addClass('hide');
                        }, 350);
                    }
                }
            });
        } else if ($('.basket__container').length) getPreloaderFromEmptyBasket();
    }

    function getPreloaderFromEmptyBasket() {
        $('.basket__preloader').removeClass('hide');

        setTimeout(function () {
            $('.alert-info').css('display', 'flex');
        }, 250);

        setTimeout(function () {
            $('.basket__preloader').addClass('hide');
            $('.basket__body').remove();
        }, 300);
    }

    $(document).on('click', '.js-basket-product-btn-delete', function () {
        setTimeout(function () {
            array_products = (localStorage.getItem('added_products') !== null) ? JSON.parse(localStorage.getItem('added_products')) : [];

            if (!array_products.length) {
                $('.basket__body').addClass('hide');
                getPreloaderFromEmptyBasket();
            }
        }, 450);
    });

    $(document).on('click', '.js-basket-product-btn-delete, .js-count-calculator-btn-plus, .js-count-calculator-btn-minus, #js-basket-products-btn-reset-all', function () {
        setTimeout(function () {
            getGenerateOrder(false);
        }, 1000);
    });
    //END: Basket

    //START: Blog
    var current_active_item   = 1,
        articles_grid_length  = 9,
        changed_page_articles = false;


    generateArticlesGrid();

    function generateArticlesGrid() {
        var icon_assets_views = false,
            icon_assets_date  = false,
            str_data_articles = '';

        if ($('.articles-grid-list').length) {
            str_data_articles += 'grid=' + articles_grid_length;
            str_data_articles += '&page=' + current_active_item;

            $.ajax({
                url: '/index.php?route=information/blog/getGridArticles',
                type: 'POST',
                data: str_data_articles,
                dataType: 'json',

                complete: function (response = false) {},

                success: function(response = false) {
                    var html       = '';

                    if (response.url_icons_assets !== undefined ) {
                        icon_assets_views = response.url_icons_assets + '/articles/icons/views-icon.svg';
                        icon_assets_date = response.url_icons_assets + '/articles/icons/date-icon.svg';
                    }

                    if (response && response.articles !== undefined && response.articles.length) {
                        for (var article of  response.articles) {
                            if (response.url_img !== undefined && article.image !== undefined && article.link !== undefined) {
                                html +=
                                    '    <li>' +
                                    '        <a class="articles-grid-list__item article-item" href="' + article.link + '">' +
                                    '            <span class="articles-grid-list__item-wrapper article-item__wrapper">' +
                                    '                <span class="articles-grid-list__item-img article-item__wrapper-img" style="background: url(' + response.url_img + article.image + ') no-repeat center center / cover"></span>' +

                                    '                <span class="articles-grid-list__item-content article-item__content">';

                                if (article.title !== undefined) {
                                    html +=
                                        '                    <h5 class="articles-grid-list__item-content-title title article-item__content-title">' + article.title + '</h5>';
                                }

                                if (article.text !== undefined) {
                                    html +=
                                        '                    <p>' + article.text + '</p>';
                                }

                                html +=
                                    '                    <footer class="articles-grid-list__item-content-footer article-item__content-footer">';

                                if (icon_assets_date && article.date !== undefined) {
                                    html +=
                                        '                        <div class="articles-grid-list__item-content-footer-date article-item__content-footer-date">' +
                                        '                            <i style="background: url(' + icon_assets_date + ') no-repeat center center / cover"></i>' +

                                        '                            <span>' + article.date + '</span>' +
                                        '                        </div>';
                                }

                                if (icon_assets_views && article.viewed !== undefined) {
                                    html +=
                                        '                        <div class="articles-grid-list__item-content-footer-views article-item__content-footer-views">' +
                                        '                            <i style="background: url(' + icon_assets_views + ') no-repeat center center / cover"></i>' +

                                        '                            <span>' + article.viewed + '</span>' +
                                        '                        </div>';
                                }

                                html +=
                                    '                    </footer>' +
                                    '                </span>' +
                                    '            </span>' +
                                    '        </a>' +
                                    '    </li>';
                            }
                        }

                        if (window.outerWidth <= 767 && changed_page_articles) {
                            $('#preloader').removeClass('hide');

                            setTimeout(changePopup, 500);
                        }

                        setTimeout(function () {
                            $('.articles-grid-list').html(html);

                            if (response.articles_count !== undefined && response.articles_count) {
                                removePreloader(
                                    {
                                        parent_grid: '.articles-grid__section-content',
                                        parent: 'articles-grid'
                                    },

                                    articles_grid_length,
                                    +response.articles_count,
                                    current_active_item
                                );

                                current_active_item = ($('.pagination__item.pagination__item--active').length) ? +$('.pagination__item.pagination__item--active').text() : 1;

                                if (window.outerWidth <= 767 && changed_page_articles) {
                                    flag_to_scroll = false;

                                    // style_html = $('html').attr('style');
                                    $('html').removeAttr('style');

                                    changePopup(false);
                                    $('#preloader').addClass('hide');

                                    $('html, body').animate({
                                        scrollTop: $('.articles-grid-list').offset().top - window.outerHeight / 4
                                    }, 500);

                                    setTimeout(function () {
                                        flag_to_scroll = true;
                                        changed_page_articles = false;
                                    }, 1000);
                                }
                            }
                        }, 1000);
                    }
                }
            });
        }
    }

    $(document).on('click', '.pagination__item', function () {
        if (!$(this).hasClass('pagination__item--dots') && !$(this).hasClass('pagination__item--active')) {
            changed_page_articles = true;

            if (! isNaN(+$(this).text()) ) current_active_item = +$(this).text();

            addPreloader('articles-grid');
            generateArticlesGrid();
        }
    });

    $(document).on('click', '.btn-next, .btn-prev', function () {
        changed_page_articles = true;

        if ($(this).hasClass('btn-next')) {
            ++current_active_item;
        } else if (current_active_item - 1) --current_active_item;

        addPreloader('articles-grid');
        generateArticlesGrid();
    });

    function addPreloader(class_names_parent = false) {
        if (class_names_parent) {
            if (window.outerWidth > 767) $('.' + class_names_parent + '__preloader').removeClass('hide');
            $('.' + class_names_parent + '__pagination.pagination-controllers').addClass('hide');
        }
    }

    function removePreloader(class_names = {}, grid_items = false, length_items = false, current_active_item_pagination = false) {
        if (Object.keys(class_names).length > 1) {
            if ($(class_names.parent_grid + ' .' + class_names.parent + '__pagination.pagination-controllers').length) {
                if (grid_items && +length_items) {
                    $(class_names.parent_grid + ' .' + class_names.parent + '__pagination.pagination-controllers').html(
                        generatePagination(grid_items, length_items, current_active_item_pagination, class_names.parent)
                    );
                } else $(class_names.parent_grid + ' .' + class_names.parent + '__pagination.pagination-controllers').html('');

                //TODO: will delete this timeout
                setTimeout(function () {
                    if (window.outerWidth > 767) $('.' + class_names.parent + '__preloader').addClass('hide');
                    $(class_names.parent_grid + ' .' + class_names.parent + '__pagination.pagination-controllers').removeClass('hide');
                }, 2000);
            }
        } else console.error('A bad param "class_names" :(');
    }

    function generatePagination(grid = false, length = false, create_active_item = false, class_name_grid = false) {
        var html_pagination        = '',
            default_class_name_itm = 'pagination__item',
            class_item_pagination  = default_class_name_itm,
            length_page            = 0,
            step                   = 1,
            step_generate          = 1,
            active_btn_controllers = '',
            max_count_items_nmb    = (window.outerWidth > 600) ? 4 : 2,
            inner_text             = '';

        if (class_name_grid) {
            current_active_item = create_active_item;

            if (length) length_page = Math.ceil(length / grid);

            if (length_page - 1) {
                active_btn_controllers = (create_active_item > 1) ? '' : 'disabled="disabled"';

                html_pagination =
                    '<div class="' + class_name_grid + '__pagination pagination-controllers">' +
                    '   <button class="' + class_name_grid + '__pagination-btn-prev btn-prev" ' + active_btn_controllers + '>' +
                    '       <i class="zmdi zmdi-long-arrow-left"></i>' +
                    '   </button>' +

                    '   <ul class="pagination">';

                if (create_active_item === 1) class_item_pagination += ' pagination__item--active';

                html_pagination +=
                    '    <li>' +
                    '        <a class="' + class_item_pagination + ' show-mobile' + '" href="javascript:void(0);">' + 1 + '</a>' +
                    '    </li>';
                if (class_item_pagination !== default_class_name_itm) class_item_pagination = default_class_name_itm;

                if (length_page > 2) {
                    inner_text = ( (current_active_item - max_count_items_nmb) > 0 ) ? '...' : 2;
                    class_item_pagination += ( (current_active_item - max_count_items_nmb) > 0 ) ? ' pagination__item--dots' : '';

                    if (create_active_item === 2) class_item_pagination += ' pagination__item--active';

                    // console.log('step 1', current_active_item);

                    html_pagination +=
                        '    <li>' +
                        '        <a class="' + class_item_pagination + ' show-mobile' + '" href="javascript:void(0);">' + inner_text + '</a>' +
                        '    </li>';
                    ++step;

                    if (class_item_pagination !== default_class_name_itm) class_item_pagination = default_class_name_itm;
                }


                if ( (current_active_item + max_count_items_nmb) > length_page ) {
                    current_active_item = length_page - max_count_items_nmb;
                }

                ////
                for (var i = (current_active_item > max_count_items_nmb) ? current_active_item : 3; step_generate < max_count_items_nmb; i++) {
                    if (window.outerWidth <= 600) class_item_pagination += ' show-mobile';

                    if (i === create_active_item) class_item_pagination += ' pagination__item--active';

                    if (i < length_page) {
                        html_pagination +=
                            '    <li>' +
                            '        <a class="' + class_item_pagination + '" href="javascript:void(0);">' + i + '</a>' +
                            '    </li>';
                    }

                    class_item_pagination = 'pagination__item';
                    ++step_generate;
                }
                ++step;
                if (class_item_pagination !== default_class_name_itm) class_item_pagination = default_class_name_itm;
                ////

                if (step < length_page) {
                    inner_text = ( (current_active_item + max_count_items_nmb) < length_page ) ? '...' : length_page - 1;
                    class_item_pagination += ( (current_active_item + max_count_items_nmb) < length_page ) ? ' pagination__item--dots' : '';

                    if (create_active_item === length_page - 1) class_item_pagination += ' pagination__item--active';

                    html_pagination +=
                        '    <li>' +
                        '        <a class="' + class_item_pagination + ' show-mobile' + '" href="javascript:void(0);">' + inner_text + '</a>' +
                        '    </li>';
                    if (class_item_pagination !== default_class_name_itm) class_item_pagination = default_class_name_itm;
                }


                if (create_active_item === length_page) class_item_pagination += ' pagination__item--active';
                html_pagination +=
                    '    <li>' +
                    '        <a class="' + class_item_pagination + ' show-mobile' + '" href="javascript:void(0);">' + length_page + '</a>' +
                    '    </li>';

                active_btn_controllers = (create_active_item < length_page) ? '' : 'disabled="disabled"';

                html_pagination +=
                    '    </ul>'  +
                    '        <button class="' + class_name_grid + '__pagination-btn-next btn-next" ' + active_btn_controllers + '>' +
                    '            <i class="zmdi zmdi-long-arrow-right"></i>' +
                    '        </button>' +
                    '</div>';

                return html_pagination;
            }
        }

        return false;
    }
    //END: Blog
});
