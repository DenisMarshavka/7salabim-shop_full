<?php
// Text
$_['text_subject']       = '%s - the order for wholesale :-)';
$_['text_waiting']       = 'Please call to the client, for confirm the data.';
$_['text_product']       = 'Product name: %s';
$_['text_reviewer']      = 'Client initials: %s';
$_['text_organization']  = 'From a organization: %s';
$_['text_phone']         = 'Phone number:';
$_['text_email']         = 'Exist the information with Email client:';
$_['text_count']         = 'Counts: %d';
$_['text_comments']      = 'And exist the comments:';