<?php
/***********************************************************************************************************************

Version 1.4.9.3
Created 17:22 21.12.2010 UTF-8
Copyright (C) 2010 Ukrainian Localization by OpenCart Ukrainian Team (http://opencart-ua.org). All rights reserved.

Translators:
 Eugene Kuligin (k.evgen@meta.ua)
 and others.

Responsible for Translation:
 Eugene Kuligin (admin@opencart-ua.org)

License http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
Ask a question http://forum.opencart-ua.org

************************************************************************************************************************/

$_['decimal_point'] = '.'; #3443
$_['thousand_point'] = ','; #3444
$_['text_home'] = 'Головна'; #3445
$_['text_yes'] = 'Так'; #3446
$_['text_no'] = 'Ні'; #3447
$_['text_none'] = '--- Немає ---'; #3448
$_['text_select'] = '--- Оберіть ---'; #3449
$_['text_all_zones'] = 'Усі зони'; #3450
$_['text_separator'] = ' &gt; '; #3451
$_['button_continue'] = 'Продовжити'; #3452
$_['button_back'] = 'Назад'; #3453
$_['button_add_to_cart'] = 'Додати до кошика'; #3454
$_['button_add_address'] = 'Додати адресу'; #3455
$_['button_new_address'] = 'Нова адреса'; #3456
$_['button_change_address'] = 'Оберіть адресу'; #3457
$_['button_edit'] = 'Змінити'; #3458
$_['button_delete'] = 'Видалити'; #3459
$_['button_reviews'] = 'Відгуки'; #3460
$_['button_write'] = 'Редагувати відгуки'; #3461
$_['button_login'] = 'Увійти'; #3462
$_['button_update'] = 'Прийняти зміни'; #3463
$_['button_shopping'] = 'Продовжити покупки'; #3464
$_['button_checkout'] = 'Оформити'; #3465
$_['button_confirm'] = 'Підтвердіть замовлення'; #3466
$_['button_view'] = 'Перегляд'; #3467
$_['button_search'] = 'Пошук'; #3468
$_['button_go'] = 'Вперед'; #3469
$_['button_coupon'] = 'Використати дисконтний купон'; #3470
$_['button_guest'] = 'Гостьове замовлення'; #3471
$_['code'] = 'uk'; #3472
$_['date_format_short'] = 'd.m.Y'; #3473
$_['time_format'] = 'H:i:s'; #3474
$_['direction'] = 'ltr'; #3479
$_['text_pagination'] = 'Показано {start} по {end} із {total} (всього сторінок: {pages})'; #3480
$_['date_format_long'] = 'd.m.Y'; #3481