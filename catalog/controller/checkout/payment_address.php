<?php
class ControllerCheckoutPaymentAddress extends Controller {
	public function index() {
		$this->load->language('checkout/checkout');

        $this->load->model('localisation/country');
        $this->load->model('localisation/zone');

        $data['basket_href'] = $this->url->link('product/basket');

        $data['breadcrumbs'][] = array(
            'text'      => '7salabim',
            'href'      => $this->url->link('common/home'),
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text'      => 'Корзина',
            'href'      => $this->url->link('product/basket'),
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('bread_crumbs_title'),
            'href'      => 'javascript:void(0);',
            'separator' => false
        );

        $this->load->model('design/layout');
        $data['layout_id'] = (isset($this->request->request['route'])) ? $this->model_design_layout->getLayout($this->request->request['route']) : 1;

        //Set title and description
        $this->load->model('catalog/category');
        $data['category_id'] = $this->model_catalog_category->getCategoryIdByLayoutId($data['layout_id']);
        $data['category_info'] = $this->model_catalog_category->getCategory($data['category_id']);

        $this->document->setTitle( trim(strip_tags($data['category_info']['meta_title'])) );
        $this->document->setDescription( trim(strip_tags($data['category_info']['meta_description'])) );

        $data['theme_url_img_assets'] = 'catalog/view/theme/7salabim/assets/img';
        $data['custom_agreement_link'] = $this->url->link('information/about_terms_use');

        $this->document->addStyle('catalog/view/theme/7salabim/assets/css/main.css');

        $this->document->addScript('catalog/view/javascript/jquery/jquery.min.js', 'footer');
        $this->document->addScript('catalog/view/javascript/jquery/jquery.maskedinput.min.js', 'footer');
        $this->document->addScript('catalog/view/javascript/common.js', 'footer');
        $this->document->addScript('catalog/view/javascript/Ajaxes.js', 'footer');
        $this->document->addScript('catalog/view/javascript/libraries/owl.carousel.min.js', 'footer');
        $this->document->addScript('https://secure.wayforpay.com/server/pay-widget.js', 'footer');

        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (isset($this->session->data['data_added_products']) && $this->session->data['data_added_products']) {
            $this->load->model('localisation/country');

            $this->load->language('checkout/payment_address');

            $data['countries'] = $this->model_localisation_country->getCountries();

            $this->load->model('checkout/order');
            $data['order_id'] = $this->model_checkout_order->getCountOrders();

            $data['qr_image'] = $this->load->controller('extension/payment/mywayforpay/save');

            if (isset($this->session->data['fields']) && $this->session->data['fields']) {
                $data['pay_fields'] = array(
                    'action' =>  'https://secure.wayforpay.com/pay',
                    'fields' => $this->session->data['fields']
                );

                foreach ($this->session->data['fields']['productName'] as $item) {
                    $data['pay_fields']['prod_nam'][] = $item;
                }

                foreach ($this->session->data['fields']['productCount'] as $item) {
                    $data['pay_fields']['prod_count'][] = $item;
                }

                foreach ($this->session->data['fields']['productPrice'] as $item) {
                    $data['pay_fields']['prod_price'][] = $item;
                }

                $data['payment_fields'] = $this->load->view('extension/payment/wayforpay', $data['pay_fields']);
            }

            if (isset($_COOKIE["recToken"]) && isset($_COOKIE["recTokenInfo"])) {
                $data['recTokenText'] = $this->language->get('payment_token_description');
                $data['recTokenInfo'] = $_COOKIE["recTokenInfo"];
            }

            $data['added_list_products'] = $this->session->data['data_added_products'];

            $data['catalog_href'] = $this->url->link('product/catalog');
            $data['basket_href'] = $this->url->link('product/basket');
            $data['submit_href'] = $this->url->link('checkout/payment_address');

            $this->response->setOutput($this->load->view('checkout/payment_address', $data));
        } else $this->response->redirect($this->url->link('product/basket', '', true));
	}

	public function save() {
	    if ( ($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->request->get || $this->request->post) ) {
	        $request_data = (isset($this->request->post)) ? $this->request->post : $this->request->get;
	        $data = array();

            $data['payment_initials'] = (isset($request_data['card_name_person'])) ? explode(" ", $request_data['card_name_person']) : false;
	        $data['payment_firstname'] = ($data['payment_initials']) ? $data['payment_initials'][0] : ' ';
	        $data['payment_lastname'] = ($data['payment_initials']) ? $data['payment_initials'][1] : ' ';
	        $data['language_id'] = $this->config->get('config_language_id');

	        if (isset($request_data['phone'])) $data['telephone'] = $request_data['phone'];
	        if (isset($request_data['email'])) $data['email'] = $request_data['email'];
	        if (isset($request_data['name'])) $data['firstname'] = $request_data['name'];
	        if (isset($request_data['last_name'])) $data['lastname'] = $request_data['last_name'];
	        if (isset($request_data['address'])) $data['shipping_address_1'] = $request_data['address'];
	        if (isset($request_data['comments'])) $data['comment'] = $request_data['comments'];
            if (isset($request_data['ip'])) $data['ip'] = $request_data['ip'];
            if (isset($request_data['shipping_company'])) $data['shipping_company'] = $request_data['shipping_company'];
            if (isset($request_data['payment_method'])) $data['payment_method'] = $request_data['payment_method'];
            if ($this->config->get('config_name')) $data['store_name'] = $this->config->get('config_name');
            if ($this->config->get('config_store_id') !== false) $data['store_id'] = $this->config->get('config_store_id');
            if ($_SERVER['HTTP_USER_AGENT']) $data['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
            if (isset($request_data['city'])) $data['payment_city'] = $request_data['city'];

            if (isset($this->session->data['data_added_products']) && $this->session->data['data_added_products']) $data['products'] = $this->session->data['data_added_products'];

            $data['total'] = 0;

            if ($this->session->data['data_added_products']) {
                foreach ($this->session->data['data_added_products'] as $added_product) {
                    if (isset($added_product['count']) && isset($added_product['price'])) {
                        $data['total'] += ((int)$added_product['count'] * (float)$added_product['price']);
                    }
                }
            }

            $data['currency_code'] = 'UAH';
	        $data['currency_value'] = 1.00000000;
	        $data['currency_id'] = 4;
	        $data['commission'] = 0.000;

            if (isset($request_data['country_code'])) {
                $this->load->model('localisation/country');
                $this->load->model('localisation/zone');

                $data['payment_country'] = $request_data['country_code'];
                $data['payment_country_data'] = $this->model_localisation_country->getCountryByCode($data['payment_country']);


                if ($data['payment_country_data'] && isset($data['payment_country_data']['country_id'])) {
                    $data['payment_country_id'] = (int)$data['payment_country_data']['country_id'];

                    if (isset($request_data['region_code'])) $data['payment_country_code'] = $request_data['region_code'];

                    if (isset($data['payment_country_code']) && $data['payment_country_code'] !== false) {
                        $data['zone'] = $this->model_localisation_zone->getZonesByCountryIdAndByCountryCode($data['payment_country_id'], $data['payment_country_code']);
                    }

                    if ($data['zone'] && isset($data['zone']['name'])) {
                        $data['payment_zone'] = $data['zone']['name'];
                    }

                    if ($data['zone'] && isset($data['zone']['zone_id'])) {
                        $data['payment_zone_id'] = $data['zone']['zone_id'];
                    }
                }
            }
        }
    }
}