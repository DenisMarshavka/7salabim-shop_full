<?php
class ControllerCheckoutSuccess extends Controller {
	public function index() {
	    if ( (isset($this->session->data['data_added_products']) && $this->session->data['data_added_products'] && isset($this->session->data['pay_complete']) && $this->session->data['pay_complete']) || $this->request->server['REQUEST_METHOD'] == 'GET' && isset($this->request->get['success_widget']) && $this->request->get['success_widget'] === 'yess') {
            $this->load->language('checkout/success');

            $this->load->language('common/success');

            $data['theme_url_img_assets'] = 'catalog/view/theme/7salabim/assets/img';

            $data['complete_order_products'] = array();
            $data['today'] = date("d.m.y");

            if (isset($this->session->data['order_id'])) $data['order_id'] = $this->session->data['order_id'];

            foreach ($this->session->data['data_added_products'] as $product_item) {
                $product = array(
                    'image' => $product_item['image'],
                    'count' => $product_item['count'],
                    'price' => $product_item['price'],
                    'name' => $product_item['name'],
                    'model' => $product_item['model']
                );

                if (isset($product_item['sale_price'])) {
                    $product['sale_price'] = round($product_item['sale_price'], 2);
                }

                $data['complete_order_products'][] = $product;
            }

            $data['theme_url_img_assets'] = 'catalog/view/theme/7salabim/assets/img';
            $data['products_list'] = $data['complete_order_products'];

            $data['continue'] = $this->url->link('product/catalog');

            if (isset($this->session->data['order_id'])) {
                $this->cart->clear();

                unset($this->session->data['shipping_method']);
                unset($this->session->data['shipping_methods']);
                unset($this->session->data['payment_method']);
                unset($this->session->data['payment_methods']);
                unset($this->session->data['guest']);
                unset($this->session->data['comment']);
                unset($this->session->data['order_id']);
                unset($this->session->data['coupon']);
                unset($this->session->data['reward']);
                unset($this->session->data['voucher']);
                unset($this->session->data['vouchers']);
                unset($this->session->data['totals']);
            }

            $this->document->setTitle($this->language->get('heading_title'));

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => '7salabim',
                'href' => $this->url->link('common/home')
            );

            $data['breadcrumbs'][] = array(
                'text' => 'Корзина',
                'href' => $this->url->link('product/basket')
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('bread_crumbs_title'),
                'href' => $this->url->link('checkout/payment_address')
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_success'),
                'href' => 'javascript:void(0);'
            );

            $this->session->data['data_added_products'] = array();
            $this->session->data['pay_complete'] = false;

            $this->document->addStyle('catalog/view/theme/7salabim/assets/css/main.css');

            $this->document->addScript('catalog/view/javascript/jquery/jquery.min.js', 'footer');
            $this->document->addScript('catalog/view/javascript/common.js', 'footer');
            $this->document->addScript('catalog/view/javascript/Ajaxes.js', 'footer');
            $this->document->addScript('catalog/view/javascript/libraries/owl.carousel.min.js', 'footer');

            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            $this->response->setOutput($this->load->view('common/success', $data, 'SSL'));
        } else $this->response->redirect($this->url->link('product/basket', '', true));
	}
}