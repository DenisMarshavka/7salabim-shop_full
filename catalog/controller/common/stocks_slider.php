<?php
class ControllerCommonStocksSlider extends Controller {
    public function index() {
        $data['theme_url_img'] = '/image/';

        $this->load->language('common/stocks_slider');
        $data['price_title'] = $this->language->get('price_title');
        $data['order_button'] = $this->language->get('order_button');

        $this->load->model('design/banner');
        $data['banners'] = $this->model_design_banner->getBanners();

        $data['URI'] = $_SERVER['SCRIPT_URI'];

        if ($data['banners']) {
            return $this->load->view('common/stocks_slider', $data);
        }
    }
}