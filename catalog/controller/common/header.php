<?php
class ControllerCommonHeader extends Controller {
    public function index() {
        $data['developer'] = md5('Denis Marshavka');

        // Analytics
        $this->load->model('setting/extension');

        if (isset($this->request->request['route'])) {
            if ($this->request->request['route'] == 'common/home') $data['home_page'] = true;
            if ($this->request->request['route'] == 'product/catalog') $data['catalog_page'] = true;
        } else {
            if ($this->request->server['REQUEST_URI'] == '/') $data['home_page'] = true;
        }

        $data['analytics'] = array();

        $analytics = $this->model_setting_extension->getExtensions('analytics');

        foreach ($analytics as $analytic) {
            if ($this->config->get('analytics_' . $analytic['code'] . '_status')) {
                $data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get('analytics_' . $analytic['code'] . '_status'));
            }
        }

        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
            $this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
        }

        $data['title'] = $this->document->getTitle();

        $data['catalog_href'] = $this->url->link('product/catalog');

        $data['base'] = $server;
        $data['description'] = $this->document->getDescription();
        $data['theme_url_img_assets'] = 'catalog/view/theme/7salabim/assets/img';
        $data['theme_url_img'] = '/image/';
//        $data['keywords'] = $this->document->getKeywords();
        $data['links'] = $this->document->getLinks();
        $data['styles'] = $this->document->getStyles();
        $data['scripts'] = $this->document->getScripts('header');
        $data['lang'] = $this->language->get('code');
        $data['direction'] = $this->language->get('direction');

        $data['name'] = $this->config->get('config_name');

        if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
            $data['logo'] = $server . 'image/' . $this->config->get('config_logo');
        } else {
            $data['logo'] = '';
        }

        $this->load->language('common/header');

        // Wishlist
        if ($this->customer->isLogged()) {
            $this->load->model('account/wishlist');

            $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
        } else {
            $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
        }

        $data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

        $data['home'] = $this->url->link('common/home');

        if (isset($this->request->request['route'])) {
            if ($this->request->request['route'] == 'common/home' || $this->request->server['REQUEST_URI'] == '/') {
                $data['home'] = 'javascript:void(0)';
            } else  $data['home'] = $this->url->link('common/home');
        }

        $data['wishlist'] = $this->url->link('account/wishlist', '', true);
        $data['logged'] = $this->customer->isLogged();
        $data['account'] = $this->url->link('account/account', '', true);
        $data['register'] = $this->url->link('account/register', '', true);
        $data['login'] = $this->url->link('account/login', '', true);
        $data['order'] = $this->url->link('account/order', '', true);
        $data['transaction'] = $this->url->link('account/transaction', '', true);
        $data['download'] = $this->url->link('account/download', '', true);
        $data['logout'] = $this->url->link('account/logout', '', true);
        $data['shopping_cart'] = $this->url->link('checkout/cart');
        $data['checkout'] = $this->url->link('checkout/checkout', '', true);
        $data['contact'] = $this->url->link('information/contact');
        $data['telephone'] = $this->config->get('config_telephone');

        $data['language'] = $this->load->controller('common/language');
        $data['currency'] = $this->load->controller('common/currency');
        $data['search'] = $this->load->controller('common/search');
        $data['cart'] = $this->load->controller('common/cart');
        $data['menu'] = $this->load->controller('common/menu');

        //Blocks
        $this->load->model('design/layout');
        $data['layout_id'] = (isset($this->request->request['route'])) ? $this->model_design_layout->getLayout($this->request->request['route']) : 1;

        if ($data['layout_id'] != false) {
            $this->load->model('setting/module');
            $data['blocks'] = $this->model_setting_module->getModulesForLayout($data['layout_id']);

            if ($data['blocks']) {
                for ($e = 0; $e < count($data['blocks']); $e++) {
                    switch ($data['blocks'][$e]['code']) {
                        case 'slideshow.27':
                            $data['stocks_slider'] = true;
                            break;
                        case 'featured.28':
                            $data['filter_link_list'] = true;
                    }
                }
            }
        }

        if (isset($data['stocks_slider']) && $data['stocks_slider']) $data['stocks_slider'] = $this->load->controller('common/stocks_slider', $data);
        if (isset($data['filter_link_list']) && $data['filter_link_list']) $data['filter_link_list'] = $this->load->controller('extension/module/filter_link_list');

        //Language
        $this->load->model('localisation/language');

        $data['languages_abbr'] = $this->model_localisation_language->getAbbrLanguages();

        if (!empty($data['languages_abbr'])) {
            foreach ($data['languages_abbr'] as $lang_key => $val) {
                $data['languages_abbr'][$lang_key]['link'] = 'http://' . $this->request->server['HTTP_HOST'];
            }
        }

        $data['current_language_code'] = $this->session->data['language'];

        $this->load->model('setting/setting');
        $data['enabled_language'] = $this->model_setting_setting->getAbbriaturelanguageById( $this->config->get('config_language_id') );

        $data['disabled_change_language'] = false;

        if ( isset($this->request->get['route']) && ($this->request->get['route'] === 'checkout/payment_address' || $this->request->get['route'] === 'product/basket') ) $data['disabled_change_language'] = true;

        //Submenu random Product
        $this->load->model('catalog/product');

        $data['all_products_id'] = $this->model_catalog_product->getAllProductsId();
        $data['rand_product_id'] = $data['all_products_id'][rand(0, count($data['all_products_id']) - 1)]['product_id'];

        $data['rand_product'] = $this->model_catalog_product->getProduct( (int)$data['rand_product_id'] );
        $data['filter_new_product'] = $this->model_catalog_product->getFilterIdByName('product_new', $this->config->get('config_language_id'));
        $data['rand_product_is_new'] = false;

        if (!empty($data['filter_new_product']) && isset($data['filter_new_product']['filter_id'])) {
            $data['filter_products_id'] = $this->model_catalog_product->getProductsByFilterId( (int)$data['filter_new_product']['filter_id'] );
        } else $data['filter_products_id'] = false;

        if ($data['filter_products_id'] != false) {
            foreach ($data['filter_products_id'] as $product) {
                if ((int)$data['rand_product_id'] == (int)$product['product_id']) {
                    $data['rand_product_is_new'] = true;

                    break;
                }
            }
        }

        if (isset($data['rand_product']) && $data['rand_product']) {
            if (isset($data['rand_product']['price'])) $data['rand_product']['price'] = round($data['rand_product']['price']);
            if (isset($data['rand_product']['name'])) $data['rand_product']['name'] = mb_strimwidth($data['rand_product']['name'], 0, 15, "...");
            if (isset($data['rand_product']['model'])) $data['rand_product']['model'] = mb_strimwidth($data['rand_product']['model'], 0, 30, "...");
            if (isset($data['rand_product']['special'])) $data['rand_product']['special'] = round($data['rand_product']['special']);

            if (isset($data['rand_product']['product_id'])) {
                if ( $_SERVER['SCRIPT_URI'] . '?' . $_SERVER['QUERY_STRING'] !== trim($this->url->link('product/product') . '&' . 'product_id=' . (int)$data['rand_product']['product_id']) ) {
                    $data['rand_product']['href'] = $this->url->link('product/product', 'product_id=' . (int)$data['rand_product']['product_id']);
                } else $data['rand_product']['href'] = 'javascript:void(0)';
            }
        }

        //Submenu Links
        $this->load->model('design/menu_links');

        $data['header_links'] = $this->model_design_menu_links->getMenuLinks($this->config->get('config_language_id'), 'header_status');
        foreach ($data['header_links'] as $link_key => $val) {
            if ( $_SERVER['SCRIPT_URI'] . '?' . $_SERVER['QUERY_STRING'] === $_SERVER['SCRIPT_URI'] . trim($data['header_links'][$link_key]['href']) ) {
                $data['header_links'][$link_key]['href'] = 'javascript:void(0)';
            }
        }

        $data['submenu_links'] = $this->model_design_menu_links->getSubMenu();

        $data['basket'] = $this->url->link('product/basket');

        $data['submenu'] = array();

        $count_link = 0;
        $count_link_level = 0;

        foreach ($data['submenu_links'] as $link => $val) {
            if ($count_link == 10) {
                $count_link = 0;
                ++$count_link_level;
            }

            if (isset($data['submenu_links'][$link]['filter_id']) && $data['submenu_links'][$link]['filter_id']) {
                if ($_SERVER['SCRIPT_URI'] . '?' . $_SERVER['QUERY_STRING'] !== $this->url->link('product/catalog') .  '&filter_id=' . $data['submenu_links'][$link]['filter_id']) {
                    $data['submenu_links'][$link]['href'] = $this->url->link('product/catalog') .  '&filter_id=' . $data['submenu_links'][$link]['filter_id'];
                } else $data['submenu_links'][$link]['href'] = 'javascript:void(0)';
            }

            $data['submenu'][$count_link_level][] = $data['submenu_links'][$link];
            $count_link++;
        }

        return $this->load->view('common/header', $data);
    }
}