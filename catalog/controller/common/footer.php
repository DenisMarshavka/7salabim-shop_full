<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$data['cookie_text'] = $this->language->get('cookie_text');
		$data['cookie_text_and'] = $this->language->get('cookie_text_and');

		$data['cookie_text_terms_use'] = $this->language->get('cookie_text_terms_use');
		$data['cookie_text_terms_use_link'] = $this->url->link('information/about_terms_use');

        if ( $_SERVER['SCRIPT_URI'] . '?' . $_SERVER['QUERY_STRING'] === trim($this->url->link('information/about_terms_use')) ) {
            $data['cookie_text_terms_use_link'] = 'javascript:void(0)';
        }

		$data['cookie_text_data_policy'] = $this->language->get('cookie_text_data_policy');
		$data['cookie_text_data_policy_link'] = $this->url->link('information/about_data_policy');

        if ( $_SERVER['SCRIPT_URI'] . '?' . $_SERVER['QUERY_STRING'] === trim($this->url->link('information/about_data_policy')) ) {
            $data['cookie_text_data_policy_link'] = 'javascript:void(0)';
        }

        $data['payment_title'] = $this->language->get('payment_title');
        $data['payment_description'] = $this->language->get('payment_description');

        $data['question_title'] = $this->language->get('question_title');
        $data['question_description'] = $this->language->get('question_description');
        $data['question_button'] = $this->language->get('question_button');

        $data['copyright_left'] = $this->language->get('copyright_left');

        $data['policy_left'] = $this->language->get('policy_left');
        $data['policy_left_link'] = $this->url->link('information/about_terms_use');

        if ( $_SERVER['SCRIPT_URI'] . '?' . $_SERVER['QUERY_STRING'] === trim($data['policy_left_link']) ) {
            $data['policy_left_link'] = 'javascript:void(0)';
        }

        $data['policy_right'] = $this->language->get('policy_right');
        $data['policy_right_link'] = $this->url->link('information/about_data_policy');

        if ( $_SERVER['SCRIPT_URI'] . '?' . $_SERVER['QUERY_STRING'] === trim($data['policy_right_link']) ) {
            $data['policy_right_link'] = 'javascript:void(0)';
        }

        $data['theme_url_img_assets'] = 'catalog/view/theme/7salabim/assets/img';

        $data['add_product'] = $this->load->controller('design/popups/add_product', $data);

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}

        $data['catalog_href'] = $this->url->link('product/catalog', 'path=59');

		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', true);
		$data['sitemap'] = $this->url->link('information/sitemap');
        $data['theme_url_img'] = 'catalog/view/theme/7salabim/assets/img';
		$data['tracking'] = $this->url->link('information/tracking');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', true);
		$data['affiliate'] = $this->url->link('affiliate/login', '', true);
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = ($this->request->server['HTTPS'] ? 'https://' : 'http://') . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

		$data['scripts'] = $this->document->getScripts('footer');

        //Footer Links
        $this->load->model('design/menu_links');

        $data['footer_links'] = $this->model_design_menu_links->getMenuLinks($this->config->get('config_language_id'), 'footer_status');

        $data['footer_menu_left'] = array();
        $data['footer_menu_right'] = array();
        $count_link = 0;
        $count_link_level = 0;

        foreach ($data['footer_links'] as $link => $val) {
            if ( $_SERVER['SCRIPT_URI'] . '?' . $_SERVER['QUERY_STRING'] === $_SERVER['SCRIPT_URI'] . trim($data['footer_links'][$link]['href']) ) {
                $data['footer_links'][$link]['href'] = 'javascript:void(0)';
            }

            if ($count_link == 3) {
                $count_link = 0;
                ++$count_link_level;
            }

            $data[($count_link_level <= 1) ? 'footer_menu_left' : 'footer_menu_right'][$count_link_level][] = $data['footer_links'][$link];
            $count_link++;
        }

        //Forms
        $data['form_question'] = $this->load->controller('extension/module/questions', $data);

        $data['form_review'] = $this->load->controller('product/form_review', $data);

        return $this->load->view('common/footer', $data);
	}
}
