<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

        $this->document->addStyle('catalog/view/theme/7salabim/assets/css/main.css');

        $this->document->addScript('catalog/view/javascript/jquery/jquery.min.js', 'footer');
        $this->document->addScript('catalog/view/javascript/common.js', 'footer');
        $this->document->addScript('catalog/view/javascript/Ajaxes.js', 'footer');
        $this->document->addScript('catalog/view/javascript/sliders.js', 'footer');
        $this->document->addScript('catalog/view/javascript/libraries/owl.carousel.min.js', 'footer');

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}

        $this->load->model('design/layout');

        $data['theme_url_img'] = 'catalog/view/theme/7salabim/assets/img';

        $data['slider_sale'] = $this->load->controller('design/products/slider_sale', $data);
        $data['slider_new'] = $this->load->controller('design/products/slider_new', $data);
        $data['slider_popular'] = $this->load->controller('design/products/slider_popular', $data);

        $data['panel_priority'] = $this->load->controller('extension/module/panel_priority', $data);

        $data['slider_articles'] = $this->load->controller('design/articles/slider_articles/get');

		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->setOutput($this->load->view('common/home', $data));
	}
}
