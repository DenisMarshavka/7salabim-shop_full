<?php

class ControllerInformationAboutTermsUse extends Controller {
    function index() {
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => '7salabim',
            'href' => $this->url->link('common/home')
        );

        $this->document->addStyle('catalog/view/theme/7salabim/assets/css/main.css');

        $this->document->addScript('catalog/view/javascript/jquery/jquery.min.js', 'footer');
        $this->document->addScript('catalog/view/javascript/common.js', 'footer');
        $this->document->addScript('catalog/view/javascript/Ajaxes.js', 'footer');
        $this->document->addScript('catalog/view/javascript/libraries/owl.carousel.min.js', 'footer');

        //Set title and description
        $this->load->model('design/layout');
        $data['layout_id'] = (isset($this->request->request['route'])) ? $this->model_design_layout->getLayout($this->request->request['route']) : 1;

        $this->load->model('catalog/category');
        $data['category_id'] = $this->model_catalog_category->getCategoryIdByLayoutId($data['layout_id']);
        $data['category_info'] = $this->model_catalog_category->getCategory($data['category_id']);

        if (!empty($data['category_info']['name'])) {
            $data['breadcrumbs'][] = array(
                'text' => $data['category_info']['name'],
                'href' => 'javascript:void(0)'
            );
        }

        $data['text'] = html_entity_decode(trim($data['category_info']['description']));

        $this->document->setTitle( trim(strip_tags($data['category_info']['meta_title'])) );
        $this->document->setDescription( trim(strip_tags($data['category_info']['meta_description'])) );

        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('information/information_page', $data));
    }
}
