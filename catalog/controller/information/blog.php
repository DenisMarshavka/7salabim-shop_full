<?php
date_default_timezone_set('Europe/Kiev');

class ControllerInformationBlog extends Controller {
    protected $json = array();
    
    public function index() {
        $this->load->language('information/blog');
        $this->load->model('catalog/information');

        $data['theme_url_img'] = 'catalog/view/theme/7salabim/assets/img';
        $data['url_img'] = '/image/';

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => '7salabim',
            'href' => $this->url->link('common/home')
        );

        //TODO: will change the variable 'text'
        $data['breadcrumbs'][] = array(
            'text' => 'Блог',
            'href' => 'javascript:void(0)'
        );

        $data['random_article'] = $this->getRandomArticle();

        $this->document->addStyle('catalog/view/theme/7salabim/assets/css/main.css');

        $this->document->addScript('catalog/view/javascript/jquery/jquery.min.js', 'footer');
        $this->document->addScript('catalog/view/javascript/common.js', 'footer');
        $this->document->addScript('catalog/view/javascript/Ajaxes.js', 'footer');
        $this->document->addScript('catalog/view/javascript/sliders.js', 'footer');
        $this->document->addScript('catalog/view/javascript/libraries/owl.carousel.min.js', 'footer');

        //Set title and description
        $this->load->model('design/layout');
        $data['layout_id'] = (isset($this->request->request['route'])) ? $this->model_design_layout->getLayout($this->request->request['route']) : 1;

        $this->load->model('catalog/category');
        $data['category_id'] = $this->model_catalog_category->getCategoryIdByLayoutId($data['layout_id']);
        $data['category_info'] = $this->model_catalog_category->getCategory($data['category_id']);

        $this->document->setTitle( trim(strip_tags($data['category_info']['meta_title'])) );
        $this->document->setDescription( trim(strip_tags($data['category_info']['meta_description'])) );

        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('information/blog', $data));
    }

    public function getRandomArticle() {
        $this->load->language('design/articles/slider_articles');
        $this->load->model('design/articles/slider_articles');

        $data['new_articles'] = $this->model_design_articles_slider_articles->get(
            array(
                'sort'  => 'id.date_added',
                'order' => 'DESC',
                'start' => 0,
                'limit' => 25
            )
        );

        if (!empty($data['new_articles'])) {
            $data['articles'] = array();

            $data['random_article'] = $data['new_articles'][rand(0, count($data['new_articles']) - 1)];
            $data['articles'][] = $data['random_article'];

            return $this->getNormalizeArticles($data['articles'])[0];
        }

        return false;
    }

    public function getGridArticles() {
        if ($this->request->server['REQUEST_METHOD'] == 'POST' && isset($this->request->post['grid']) && isset($this->request->post['page'])) {
            $this->load->language('design/articles/slider_articles');
            $this->load->model('design/articles/slider_articles');

            $this->json['url_icons_assets'] = 'catalog/view/theme/7salabim/assets/img';
            $this->json['url_img'] = '/image/';

            $data['articles'] = $this->model_design_articles_slider_articles->get(
                array(
                    'sort'  => 'id.date_added',
                    'order' => 'DESC',
                    'start' => ((int)$this->request->post['page'] - 1) ? ((int)$this->request->post['page'] - 1) * (int)$this->request->post['grid'] : 0,
                    'limit' => (int)$this->request->post['grid']
                )
            );

            $this->json['articles_count'] = $this->model_design_articles_slider_articles->getCountArticles(
                array(
                    'sort'  => 'id.date_added',
                    'order' => 'DESC'
                )
            );

            $this->json['articles'] = $this->getNormalizeArticles($data['articles'], true, true);
            
            $this->response->addHeader('Content-Type: application/json');
            echo json_encode($this->json);
        }
        
        return false;
    }
    
    public function getNormalizeArticles($articles = array(), $strim_width_texts = false, $is_generate_grid = false) {
        if ($articles) {
            foreach ($articles as $article_key => $article_val) {
                if (isset($articles[$article_key]['information_id'])) {
                    $articles[$article_key]['link'] = $this->url->link('information/article', 'article_id=' . $articles[$article_key]['information_id']);
                }

                if ($is_generate_grid) {
                    if (isset($articles[$article_key]['title'])) {
                        $articles[$article_key]['title'] = strip_tags(html_entity_decode($articles[$article_key]['title'], ENT_QUOTES, 'UTF-8'));

                        if ($strim_width_texts) {
                            $articles[$article_key]['title'] = mb_strimwidth($articles[$article_key]['title'], 0, 32, "...");
                        }
                    }

                    if (isset($articles[$article_key]['description'])) {
                        $articles[$article_key]['text'] = strip_tags(html_entity_decode($articles[$article_key]['description'], ENT_QUOTES, 'UTF-8'));

                        if ($strim_width_texts) {
                            $articles[$article_key]['text'] = mb_strimwidth($articles[$article_key]['text'], 0, 133, "...");
                        }
                    }
                } else {
                    if (isset($articles[$article_key]['subtitle']) && $strim_width_texts) {
                        $articles[$article_key]['subtitle'] = mb_strimwidth($articles[$article_key]['subtitle'], 0, 32, "...");
                    }
                }

                if (isset($articles[$article_key]['date_added'])) {
                    $articles[$article_key]['date'] = date('d.m.Y', strtotime($articles[$article_key]['date_added']));
                    $data['date_hours'] = date('H:i', strtotime($articles[$article_key]['date_added']));

                    $data_received_date_array = explode('.', $articles[$article_key]['date']);
                    $current_date_array = explode('.', date('d.m.Y'));

                    $data_received_hours_array = explode(':', $data['date_hours']);
                    $current_hours_array = explode(':', date('H:i'));

                    if ($data_received_date_array[0] === $current_date_array[0] && $data_received_date_array[1] === $current_date_array[1]) {
                        if ((int)$current_hours_array[0] >= (int)$data_received_hours_array[0]) {
                            $data_between_dates = (int)$current_hours_array[0] - (int)$data_received_hours_array[0];

                            if (($data_between_dates <= 4) && ($data_between_dates > 0)) {
                                $articles[$article_key]['date'] = $data_between_dates . ' ' . $this->language->get('title_hours');
                            } elseif (($data_between_dates) === 0) {
                                $articles[$article_key]['date'] = $this->language->get('title_newly');
                            }
                        }
                    }
                } 
            }
            

            return $articles;
        }
        
        return false;
    }
}
