<?php

date_default_timezone_set('Europe/Kiev');

class ControllerInformationArticle extends Controller {
	public function index() {
		$this->load->language('information/article');

		$this->load->model('catalog/information');

        $data['theme_url_img'] = 'catalog/view/theme/7salabim/assets/img';
        $data['url_img'] = '/image/';

		$data['breadcrumbs'] = array();

		$data['url_blog'] = $this->url->link('information/blog');

		$data['breadcrumbs'][] = array(
			'text' => '7salabim',
			'href' => $this->url->link('common/home')
		);

        $data['breadcrumbs'][] = array(
            'text' => 'Блог',
            'href' => $data['url_blog']
        );

		if (isset($this->request->get['article_id'])) {
			$information_id = (int)$this->request->get['article_id'];
            $this->model_catalog_information->changeViewById($information_id);
		} else {
			$information_id = 0;
		}

		$information_info = $this->model_catalog_information->getInformation($information_id);
		$data['viewed'] = $this->model_catalog_information->getInformationViewed($information_id);

        $this->document->addStyle('catalog/view/theme/7salabim/assets/css/main.css');

        $this->document->addScript('catalog/view/javascript/jquery/jquery.min.js', 'footer');
        $this->document->addScript('catalog/view/javascript/common.js', 'footer');
        $this->document->addScript('catalog/view/javascript/Ajaxes.js', 'footer');
        $this->document->addScript('catalog/view/javascript/sliders.js', 'footer');
        $this->document->addScript('catalog/view/javascript/libraries/owl.carousel.min.js', 'footer');

		if ($information_info) {
		    $data['image'] = $information_info['image'];

		    $data['title'] = strip_tags($information_info['title']);
		    $data['subtitle'] = strip_tags($information_info['subtitle']);

		    if ($information_info['date_added']) {
                $data['date'] = date('d.m.Y', strtotime($information_info['date_added']));
                $data['date_hours'] = date('H:i', strtotime($information_info['date_added']));

                $data_received_date_array = explode('.', $data['date']);
                $current_date_array = explode('.', date('d.m.Y'));

                $data_received_hours_array = explode(':', $data['date_hours']);
                $current_hours_array = explode(':', date('H:i'));

                if ($data_received_date_array[0] === $current_date_array[0] && $data_received_date_array[1] === $current_date_array[1]) {
                    if ((int)$current_hours_array[0] >= (int)$data_received_hours_array[0]) {
                        $data_between_dates = (int)$current_hours_array[0] - (int)$data_received_hours_array[0];

                        if (($data_between_dates <= 4) && ($data_between_dates > 0)) {
                            $data['date'] = $data_between_dates . ' ' . $this->language->get('title_hours');
                        } elseif (($data_between_dates) === 0) {
                            $data['date'] = $this->language->get('title_newly');
                        }
                    }
                }
            }

            $data['text'] = html_entity_decode($information_info['description']);

			$this->document->setTitle($information_info['meta_title']);
			$this->document->setDescription($information_info['meta_description']);

			$data['breadcrumbs'][] = array(
				'text' => $data['title'],
				'href' => 'javascript:void(0);'
			);

			$data['heading_title'] = $information_info['title'];

			$data['description'] = html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8');

			$data['continue'] = $this->url->link('common/home');

			$data['slider_popular_articles'] = $this->load->controller('design/articles/slider_articles/get', true);

			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('information/article', $data));
		} else {
			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('information/article', 'information_id=' . $information_id)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			$this->response->setOutput($this->load->view('error/not_found', $data));
		}
	}

	public function agree() {
		$this->load->model('catalog/information');

		if (isset($this->request->get['article_id'])) {
			$information_id = (int)$this->request->get['article_id'];
		} else {
			$information_id = 0;
		}

		$output = '';

		$information_info = $this->model_catalog_information->getInformation($information_id);

		if ($information_info) {
			$output .= html_entity_decode($information_info['description'], ENT_QUOTES, 'UTF-8') . "\n";
		}

		$this->response->setOutput($output);
	}
}