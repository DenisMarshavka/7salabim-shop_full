<?php

class ControllerExtensionPaymentMywayforpay extends Controller {
    private $error  = array();
    private $result = array();
    public $data    = array();
    public $fields  = array();

    public $codesCurrency = array(
        980 => 'UAH',
        840 => 'USD',
        978 => 'EUR',
        643 => 'RUB',
    );

    public function index() {
        if ( ($this->request->server['REQUEST_METHOD'] = 'POST') && $this->validate() ) {
            echo json_encode($this->save($this->request->get));

            if (isset($this->result['result']) && $this->result['result'] === 'success') {
                $this->session->data['pay_complete'] = true;
            } else $this->session->data['pay_complete'] = false;
        } else echo json_encode($this->error);
    }

    public function save($request_data = false) {
//        try {
            if ($request_data !== false && $this->session->data['data_added_products']) {
                $data = array();

                $data['payment_initials'] = (isset($request_data['card_name_person'])) ? explode(" ", $request_data['card_name_person']) : false;
                $data['payment_firstname'] = ($data['payment_initials']) ? $data['payment_initials'][0] : ' ';
                $data['payment_lastname'] = ($data['payment_initials']) ? $data['payment_initials'][1] : ' ';
                $data['language_id'] = $this->config->get('config_language_id');
                $data['ip'] = getenv('REMOTE_ADDR');

                if (isset($request_data['phone'])) $data['telephone'] = $request_data['phone'];
                if (isset($request_data['email'])) $data['email'] = $request_data['email'];
                if (isset($request_data['name'])) $data['firstname'] = $request_data['name'];
                if (isset($request_data['last_name'])) $data['lastname'] = $request_data['last_name'];
                if (isset($request_data['address'])) $data['shipping_address_1'] = $request_data['address'];
                if (isset($request_data['comments'])) $data['comment'] = $request_data['comments'];
                if (isset($request_data['shipping_company'])) $data['shipping_company'] = $request_data['shipping_company'];
                if (isset($request_data['payment_method'])) $data['payment_method'] = $request_data['payment_method'];
                if ($this->config->get('config_name')) $data['store_name'] = $this->config->get('config_name');
                if ($this->config->get('config_store_id') !== false) $data['store_id'] = $this->config->get('config_store_id');
                if ($_SERVER['HTTP_USER_AGENT']) $data['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
                if (isset($request_data['city'])) $data['payment_city'] = $request_data['city'];

                if (isset($this->session->data['data_added_products']) && $this->session->data['data_added_products']) $data['products'] = $this->session->data['data_added_products'];

                $data['total'] = 0;

                if ($this->session->data['data_added_products']) {
                    foreach ($this->session->data['data_added_products'] as $added_product) {
                        if (isset($added_product['count']) && isset($added_product['price'])) {
                            $data['total'] += ((int)$added_product['count'] * (float)$added_product['price']);
                        }
                    }
                }

                $data['currency_code'] = 'UAH';
                $data['currency_value'] = 1.00000000;
                $data['currency_id'] = 4;
                $data['commission'] = 0.000;

                if (isset($request_data['country_code'])) {
                    $this->load->model('localisation/country');
                    $this->load->model('localisation/zone');

                    $data['payment_country'] = $request_data['country_code'];
                    $data['payment_country_data'] = $this->model_localisation_country->getCountryByCode($data['payment_country']);


                    if ($data['payment_country_data'] && isset($data['payment_country_data']['country_id'])) {
                        $data['payment_country_id'] = (int)$data['payment_country_data']['country_id'];

                        if (isset($request_data['region_code'])) {
                            $data['payment_country_code'] = $request_data['region_code'];

                            if (isset($data['payment_country_code']) && $data['payment_country_code'] && $data['payment_country_code'] !== false) {
                                $data['zone'] = $this->model_localisation_zone->getZonesByCountryIdAndByCountryCode($data['payment_country_id'], $data['payment_country_code']);
                            }


                            if ($data['zone'] && isset($data['zone']['name'])) {
                                $data['payment_zone'] = $data['zone']['name'];
                            }

                            if ($data['zone'] && isset($data['zone']['zone_id'])) {
                                $data['payment_zone_id'] = $data['zone']['zone_id'];
                            }
                        }
                    }
                }
            } else {
                //For create QR
                $data = array();

                if ($this->session->data['data_added_products']) {
                    foreach ($this->session->data['data_added_products'] as $added_product) {
                        if (isset($added_product['count']) && isset($added_product['price'])) {
                            $data['total'] += ((int)$added_product['count'] * (float)$added_product['price']);
                        }
                    }
                }
            }

            if ($data && $this->session->data['data_added_products']) {
                $this->load->model('checkout/order');
                $data['order_id'] = $this->model_checkout_order->addOrder($data);

                $data['country_code'] = (isset($request_data['country_code'])) ? $request_data['country_code'] : 'UA';

                if ($data['order_id'] !== false) {
                    $this->session->data['order_id'] = ((int)$data['order_id']) ? $data['order_id'] : 1;

                    if (isset($data['payment_method']) && $data['payment_method'] === 'Cash') {
                        $this->setOrderCash($data['order_id']);
                    } else if (
                        isset($request_data['card_date']) && isset($request_data['card_date']) && isset($request_data['card_cvv']) &&
                        isset($request_data['card_name_person']) && isset($data['email']) && isset($data['firstname']) &&
                        isset($data['lastname']) && isset($data['telephone']) && isset($data['country_code'])
                    ) {
                        $this->session->data['fields'] = $this->generateFields(false, true);

                        $data['card_date_arr'] = explode("/", $request_data['card_date']);
                        $data['card_initials_arr'] = explode(" ", $request_data['card_name_person']);

                        $this->session->data['fields']['card'] = str_replace(array(' '), array(''), $request_data['card_num']);

                        if (count($data['card_date_arr'])) {
                            $this->session->data['fields']['expMonth'] = $data['card_date_arr'][0];
                            $this->session->data['fields']['expYear'] = '20' . $data['card_date_arr'][1];
                        }

                        $this->session->data['fields']['cardCvv'] = $request_data['card_cvv'];
                        $this->session->data['fields']['cardHolder'] = $request_data['card_name_person'];

                        //TODO: check this event with a country on JS script
                        $this->session->data['fields']['clientCountry'] = (isset($data['payment_country'])) ? $data['payment_country'] : 'UA';

                        $this->session->data['fields'];
                        $data['response_card'] = json_decode($this->load->controller('extension/payment/wayforpaysend'), true);

                        if ($data['response_card']) {
                            if (isset($data['response_card']['reasonCode'])) {
                                if ($data['response_card']['reasonCode'] === 1100) {
                                    $this->setRecToken($data['response_card']['recToken'], (isset($data['response_card']['cardPan'])) ? $data['response_card']['cardPan'] : false, (isset($data['response_card']['cardType'])) ? $data['response_card']['cardType'] : false);
                                    $this->result['result'] = 'success';
                                } else $this->setError($data['response_card']['reasonCode'], 'error_card_' . (isset($data['response_card']['orderReference'])) ? $data['response_card']['orderReference'] : 'card', $data['response_card']['reason']);
                            }
                        }
                    } else if (
                        isset($data['payment_method']) && $data['payment_method'] === 'customerToken' && isset($data['email']) && isset($data['firstname']) &&
                        isset($data['lastname']) && isset($data['telephone']) && isset($_COOKIE['recToken'])
                    ) {
                        $this->session->data['fields'] = $this->generateFields(false, true, $_COOKIE['recToken']);

                        //TODO: check this event with a country on JS script
                        $this->session->data['fields']['clientCountry'] = (isset($data['payment_country'])) ? $data['payment_country'] : 'UA';

                        $data['response_token'] = json_decode($this->load->controller('extension/payment/wayforpaysend'), true);

                        if ($data['response_token']) {
                            if (isset($data['response_token']['reasonCode'])) {
                                if ($data['response_token']['reasonCode'] === 1100) {
                                    $this->setRecToken($data['response_token']['recToken'], (isset($data['response_token']['cardPan'])) ? $data['response_token']['cardPan'] : false, (isset($data['response_token']['cardType'])) ? $data['response_token']['cardType'] : false);
                                    $this->result['result'] = 'success';
                                } else $this->setError($data['response_token']['reasonCode'], 'error_token_' . $data['response_token']['orderReference'], $data['response_token']['reason']);
                            }
                        }
                    } else {
                        if (!isset($data['payment_method'])) {
                            $this->session->data['fields'] = $this->generateFields(true);

                            $data['response_qr'] = json_decode($this->load->controller('extension/payment/wayforpaysend'), true);

                            if ($data['response_qr']) {
                                if (isset($data['response_qr']['reasonCode'])) {
                                    if ( $data['response_qr']['reasonCode'] === 1100 && isset($data['response_qr']['imageUrl']) ) {
                                        return $data['response_qr']['imageUrl'];
                                    } else $this->setError($data['response_qr']['reasonCode'], 'error_qr_' . $data['response_qr']['orderReference'], $data['response_qr']['reason']);
                                }
                            }
                        }
                    }
                }
            }
//        } catch (Exception $e) {
//            $this->setError(0, 'error_fatal', $e);
//            $this->response->redirect($this->url->link('error/payment', '', true));
//        }

        return $this->result;
    }

    public function setRecToken($rec_token = false, $card_pan = false, $card_type = false) {
        if ($rec_token) {
            setcookie("recToken", $rec_token, time()+60*60*24*30, '/');

            if ($card_pan && $card_type) {
                setcookie("recTokenInfo", $card_pan . ' ' . $card_type, time()+60*60*24*30, '/');
            }
        }
    }

    public function setError($reason_code = false, $order_reference = false, $message = false) {
        if ($reason_code !== false && $message) {
            $this->load->model('extension/payment/wayforpay');

            $order_reference = ($order_reference !== false) ? $order_reference : 'false';

            $this->model_extension_payment_wayforpay->setError($reason_code, $this->config->get('config_language_id'), $order_reference, $message);

            $this->result['result'] = 'error';
            $this->result['code'] = (int)$reason_code;
        }
    }

    public function generateFields($is_qr = false, $is_card = false, $rec_token = false) {
        $w4p = new WayForPay();
        $key = $this->config->get('payment_wayforpay_secretkey');
        $w4p->setSecretKey($key);

        $this->load->model('checkout/order');
        $order = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        $serviceUrl = $this->config->get('payment_wayforpay_serviceUrl');
        $returnUrl = $this->config->get('payment_wayforpay_returnUrl');

        $currency = $this->codesCurrency[980];
        $amount = round(($order['total'] * $order['currency_value']), 2);

        $fields = array(
            'orderReference' => $order['order_id'] . WayForPay::ORDER_SEPARATOR . time(),
            'merchantAccount' => $this->config->get('payment_wayforpay_merchant'),
            'orderDate' => strtotime($order['date_added']),
            'merchantAuthType' => 'SimpleSignature',
            'merchantDomainName' => $_SERVER['HTTP_HOST'],
            'merchantTransactionSecureType' => 'AUTO',
            'amount' => $amount,
            'currency' => $currency,
            'serviceUrl' => $serviceUrl,
            'returnUrl' => $returnUrl,
            'language' => $this->config->get('payment_wayforpay_language')
        );

        if ($is_qr) {
            $fields['transactionType'] = 'CREATE_QR';
            $fields['orderTimeout'] = 86400;
        } else {
            $fields['transactionType'] = 'CHARGE';

            if ($rec_token) $fields['recToken'] = $rec_token;
        }

        if ($is_qr || $is_card) {
            $fields['apiVersion'] = 1;
        }

        if ($is_card || $rec_token) {
            $fields['merchantTransactionType'] = 'AUTH';
            $fields['merchantTransactionSecureType'] = 'NON3DS';
        }

        $productNames = array();
        $productQty = array();
        $productPrices = array();

        $this->load->model('account/order');
        $products = $this->model_account_order->getOrderProducts($order['order_id']);

        foreach ($products as $product) {
            $product_title = str_replace(array("'", '"', '&#39;', '&'), '', htmlspecialchars_decode($product['name']));
            if (isset($product['model']) && $product['model']) $product_title .= ' ' . str_replace(array("'", '"', '&#39;', '&'), '', htmlspecialchars_decode($product['model']));

            if (isset($product['color']) && $product['color']) {
                $color = $this->model_account_order->getColorNameByHash($product['color']);

                if ($color && trim($color) !== '') $product_title .= ' ' . '(' . $color . ')';
            }

            $productNames[] = $product_title;
            $productPrices[] = round($product['price'], 2);
            $productQty[] = $product['quantity'];
        }

        $fields['productName'] = $productNames;
        $fields['productPrice'] = $productPrices;
        $fields['productCount'] = $productQty;

        /**
         * Check phone
         */
        $phone = str_replace(array('+', ' ', '(', ')'), array('', '', '', ''), $order['telephone']);
        if (strlen($phone) == 10) {
            $phone = '38' . $phone;
        } elseif (strlen($phone) == 11) {
            $phone = '3' . $phone;
        }

        if ( !preg_match('/^NONE.*$/', trim($order['firstname'])) ) $fields['clientFirstName'] = $order['firstname'];
        $this->data['clientLastName'] = $order['lastname'];

        if ( $this->data['clientLastName'] && !preg_match('/^NONE.*$/', trim($this->data['clientLastName'])) ) {
            $fields['clientLastName'] = $this->data['clientLastName'];
        }

        $fields['clientEmail'] = $order['email'];
        $fields['clientPhone'] = $phone;
        if ($order['payment_city'] !== '') $fields['clientCity'] =  $order['payment_city'];
        if ($order['shipping_address_1'] !== '') $fields['clientAddress'] = $order['shipping_company'] . ' ' . $order['shipping_address_1'];
        if ($order['payment_iso_code_3'] !== '') $fields['clientCountry'] = $order['payment_iso_code_3'];

        $fields['merchantSignature'] = $w4p->getRequestSignature($fields);

        return $fields;
    }

    public function response() {
        $w4p = new WayForPay();
        $key = $this->config->get('payment_wayforpay_secretkey');
        $w4p->setSecretKey($key);

        $paymentInfo = $w4p->isPaymentValid($_POST);

        if ($paymentInfo === true) {
            list($order_id,) = explode(WayForPay::ORDER_SEPARATOR, $_POST['orderReference']);

            $message = '';

            $this->load->model('checkout/order');

            /**
             * check current order status if no eq wayforpay_order_status_id then confirm
             */
            $orderInfo = $this->model_checkout_order->getOrder($order_id);
            if (
                $orderInfo &&
                $orderInfo['order_status_id'] == $this->config->get('payment_wayforpay_order_status_id')
            ) {
                //nothing
            } else {
                echo $message;
                $this->model_checkout_order->updateOrder($order_id, $_POST);


                $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_wayforpay_order_status_id'), $message, false, false);
            }

            $this->response->redirect($this->url->link('checkout/success', '', 'SSL'));
        } else {
            $this->session->data['error'] = $paymentInfo;
            echo $this->session->data['error'];

            $this->response->redirect($this->url->link('common/home', '', 'SSL'));
        }
    }

    public function callback() {
        $data = json_decode(file_get_contents("php://input"), true);

        $this->load->model('checkout/order');

        $w4p = new WayForPay();
        $key = $this->config->get('payment_wayforpay_secretkey');
        $w4p->setSecretKey($key);

        $paymentInfo = $w4p->isPaymentValid($data);

        if ($paymentInfo === true) {
            list($order_id,) = explode(WayForPay::ORDER_SEPARATOR, $data['orderReference']);

            $message = '';

            /**
             * check current order status if no eq wayforpay_order_status_id then confirm
             */

            $orderInfo = $this->model_checkout_order->getOrder($order_id);
            if (
                $orderInfo &&
                $orderInfo['order_status_id'] == $this->config->get('payment_wayforpay_order_status_id')
            ) {
                //nothing
            } else {
                if (isset($data['recToken'])) {
                    $this->setRecToken($data['recToken'], (isset($data['cardPan'])) ? $data['cardPan'] : false, (isset($data['cardType'])) ? $data['cardType'] : false);
                }

                $this->model_checkout_order->updateOrder($order_id, $data);

                $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('payment_wayforpay_order_status_id'), $message, false, false, $data);
                $this->response->redirect($this->url->link('checkout/success', '', true));
            }

            echo $w4p->getAnswerToGateWay($data);
        } else {
            if ((int)$data['reasonCode'] !== 1100) {
                $this->setError($data['reasonCode'],  (isset($data['orderReference'])) ? $data['orderReference'] : false, $data['reason']);

//                $this->response->redirect($this->url->link('error/payment', '', true));
            }
        }
        exit();
    }

    public function setOrderCash($order_id) {
        if ($order_id != false) {
            $this->model_checkout_order->addOrderHistory($order_id, 2, '', false);
            $this->result['result'] = 'success';
        }
    }

    public function validate() {
         $this->load->language('checkout/payment_address');

        if (!isset($this->request->get['error_valid'])) {
            if (isset($this->request->get['name'])) {
                if ( !preg_match('/^[A-Za-zА-ЯҐЄІЇЁа-яґєіїё\`]{3,25}$/u', trim($this->request->get['name'])) ) {
                    $this->error['type'] = 'name';
                    $this->error['text'] = $this->language->get('error_input_name');
                }
            }

            if (isset($this->request->get['last_name'])) {
                if ( !preg_match('/^[A-Za-zА-ЯҐЄІЇЁа-яґєіїё\`]{3,35}$/u', trim($this->request->get['last_name'])) ) {
                    $this->error['type'] = 'last-name';
                    $this->error['text'] = $this->language->get('error_input_last_name');
                }
            }

            if (isset($this->request->get['phone'])) {
                if ( !preg_match('/^\+*\d{9,15}$/', trim($this->request->get['phone'])) ) {
                    $this->error['type'] = 'phone';
                    $this->error['text'] = $this->language->get('error_input_phone');
                }
            }

            if (isset($this->request->get['email'])) {
                if ( !preg_match('/^[\S.]{4,}@[0-9]*[a-zA-Z]{2,}\.[a-zA-Z]{2,6}$/m', trim($this->request->get['email'])) ) {
                    $this->error['type'] = 'email';
                    $this->error['text'] = $this->language->get('error_input_email');
                }
            }

            if (isset($this->request->get['comments'])) {
                if ( (utf8_strlen(trim($this->request->get['comments'])) < 10) || (utf8_strlen(trim($this->request->get['comments'])) > 250) ) {
                    $this->error['type'] = 'comments';
                    $this->error['text'] = $this->language->get('error_input_comments');
                }
            }

            if (isset($this->request->get['address'])) {
                if ( (utf8_strlen(trim($this->request->get['address'])) < 10) || (utf8_strlen(trim($this->request->get['address'])) > 100) ) {
                    $this->error['type'] = 'address';
                    $this->error['text'] = $this->language->get('error_input_address');
                }
            }

            if (isset($this->request->get['card_num'])) {
                if ( !preg_match('/^\d{4}\s\d{4}\s\d{4}\s\d{4}\s?\d?$/', trim($this->request->get['card_num'])) ) {
                    $this->error['type'] = 'card-number';
                    $this->error['text'] = $this->language->get('error_input_card_number');
                }
            }

            if (isset($this->request->get['card_date'])) {
                if ( !preg_match('/^(1[0-2]|0[1-9])\/(1[5-9]|2\d)$/', trim($this->request->get['card_date'])) ) {
                    $this->error['type'] = 'card-date';
                    $this->error['text'] = $this->language->get('error_input_card_date');
                }
            }

            if (isset($this->request->get['card_cvv'])) {
                if ( !preg_match('/^\d{3}$/', trim($this->request->get['card_cvv'])) ) {
                    $this->error['type'] = 'card-cvv';
                    $this->error['text'] = $this->language->get('error_input_card_cvv');
                }
            }

            if (isset($this->request->get['card_name_person'])) {
                if ( !preg_match('/^[A-Za-zА-ЯҐЄІЇЁа-яґєіїё\`]{3,15}\s[A-Za-zА-ЯҐЄІЇЁа-яґєіїё\`]{2,16}$/', trim($this->request->get['card_name_person'])) ) {
                    $this->error['type'] = 'card-person-name';
                    $this->error['text'] = $this->language->get('error_input_card_initials');
                }
            }
        } else {
            switch ($this->request->get['error_valid']) {
                case 'error_name':
                    $this->error['type'] = 'name';
                    $this->error['text'] = $this->language->get('error_input_name');
                    break;

                case 'error_last-name':
                    $this->error['type'] = 'last-name';
                    $this->error['text'] = $this->language->get('error_input_last_name');
                    break;

                case 'error_phone':
                    $this->error['type'] = 'phone';
                    $this->error['text'] = $this->language->get('error_input_phone');
                    break;

                case 'error_email':
                    $this->error['type'] = 'email';
                    $this->error['text'] = $this->language->get('error_input_email');
                    break;

                case 'error_comments':
                    $this->error['type'] = 'comments';
                    $this->error['text'] = $this->language->get('error_input_comments');
                    break;

                case 'error_np-address':
                    $this->error['type'] = 'address';
                    $this->error['text'] = $this->language->get('error_input_address');
                    break;

                case 'error_courier-address':
                    $this->error['type'] = 'address';
                    $this->error['text'] = $this->language->get('error_input_address');
                    break;

                case 'error_card-number':
                    $this->error['type'] = 'card-number';
                    $this->error['text'] = $this->language->get('error_input_card_number');
                    break;

                case 'error_card-date':
                    $this->error['type'] = 'card-date';
                    $this->error['text'] = $this->language->get('error_input_card_date');
                    break;

                case 'error_card-cvv':
                    $this->error['type'] = 'card-cvv';
                    $this->error['text'] = $this->language->get('error_input_card_cvv');
                    break;

                case 'error_card-person-name':
                    $this->error['type'] = 'card-person-name';
                    $this->error['text'] = $this->language->get('error_input_card_initials');
                    break;

                default:
                    $this->error['type'] = 'error_fatal';
                    $this->error['text'] = $this->language->get('error_fatal');
            }
        }

        return !$this->error;
    }
}

class WayForPay {
    const ORDER_APPROVED = 'Approved';
    const ORDER_HOLD_APPROVED = 'WaitingAuthComplete';
    const ORDER_IS_PENDING = 'Pending';

    const ORDER_SEPARATOR = '#';

    const SIGNATURE_SEPARATOR = ';';

    const URL = "https://secure.wayforpay.com/pay";

    protected $secret_key = '';
    protected $keysForResponseSignature = array(
        'merchantAccount',
        'orderReference',
        'amount',
        'currency',
        'authCode',
        'cardPan',
        'transactionStatus',
        'reasonCode'
    );

    /** @var array */
    protected $keysForSignature = array(
        'merchantAccount',
        'merchantDomainName',
        'orderReference',
        'orderDate',
        'amount',
        'currency',
        'productName',
        'productCount',
        'productPrice'
    );


    /**
     * @param $option
     * @param $keys
     * @return string
     */
    public function getSignature($option, $keys)
    {
        $hash = array();
        foreach ($keys as $dataKey) {
            if (!isset($option[$dataKey])) {
                $option[$dataKey] = '';
            }
            if (is_array($option[$dataKey])) {
                foreach ($option[$dataKey] as $v) {
                    $hash[] = $v;
                }
            } else {
                $hash [] = $option[$dataKey];
            }
        }

        $hash = implode(self::SIGNATURE_SEPARATOR, $hash);
        return hash_hmac('md5', $hash, $this->getSecretKey());
    }


    /**
     * @param $options
     * @return string
     */
    public function getRequestSignature($options)
    {
        return $this->getSignature($options, $this->keysForSignature);
    }

    /**
     * @param $options
     * @return string
     */
    public function getResponseSignature($options)
    {
        return $this->getSignature($options, $this->keysForResponseSignature);
    }


    /**
     * @param array $data
     * @return string
     */
    public function getAnswerToGateWay($data)
    {
        $time = time();
        $responseToGateway = array(
            'orderReference' => $data['orderReference'],
            'status' => 'accept',
            'time' => $time
        );
        $sign = array();
        foreach ($responseToGateway as $dataKey => $dataValue) {
            $sign [] = $dataValue;
        }
        $sign = implode(self::SIGNATURE_SEPARATOR, $sign);
        $sign = hash_hmac('md5', $sign, $this->getSecretKey());
        $responseToGateway['signature'] = $sign;

        return json_encode($responseToGateway);
    }

    /**
     * @param $response
     * @return bool|string
     */
    public function isPaymentValid($response)
    {
        if (!isset($response['merchantSignature']) && isset($response['reason'])) {
            return $response['reason'];
        }

        $sign = $this->getResponseSignature($response);
        if ($sign != $response['merchantSignature']) {
            return 'An error has occurred during payment';
        }

        if (
            $response['transactionStatus'] == self::ORDER_APPROVED ||
            $response['transactionStatus'] == self::ORDER_HOLD_APPROVED ||
            $response['transactionStatus'] == self::ORDER_IS_PENDING
        ) {
            return true;
        }

        return false;
    }

    public function setSecretKey($key)
    {
        $this->secret_key = $key;
    }

    public function getSecretKey()
    {
        return $this->secret_key;
    }
}
