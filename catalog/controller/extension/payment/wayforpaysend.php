<?php

class ControllerExtensionPaymentWayforpaySend extends Controller
{
    public function index() {
        if (isset($this->session->data['fields']) && $this->session->data['fields'] !== false) {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://api.wayforpay.com/api");

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                "Content-Type:application/json"
            ]);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->session->data['fields']));

            $res = curl_exec($ch);

            curl_close($ch);
            return $res;
        }

        return false;
    }
}