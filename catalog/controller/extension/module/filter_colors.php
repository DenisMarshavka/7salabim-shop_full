<?php

//TODO: will added this module for admin
class ControllerExtensionModuleFilterColors extends Controller {
    private $status_active = false;

    public function getItems() {
        $this -> load -> language('extension/module/filter_colors');
        $this -> load -> model('extension/module/filter_colors');

        $this->checkStatusActive();

        if ($this->status_active) {
            $color_items = $this-> model_extension_module_filter_colors -> get();

            if ($color_items) return $color_items;
        }

        return false;
    }

    protected function checkStatusActive() {
        $this -> load -> model('extension/module/filter_colors');

        $this->status_active = $this-> model_extension_module_filter_colors -> getStatus();
    }
}