<?php

//TODO: will added this module for admin
class ControllerExtensionModuleFilterHolidays extends Controller {
    private $status_active = false;

    public function getItems() {
        $this -> load -> language('extension/module/filter_holidays');
        $this -> load -> model('extension/module/filter_holidays');

        $this->checkStatusActive();

        if ($this->status_active) {
            $holiday_items = $this-> model_extension_module_filter_holidays -> get();

            if ($holiday_items) return $holiday_items;
        }

        return false;
    }

    protected function checkStatusActive() {
        $this -> load -> model('extension/module/filter_holidays');

        $this->status_active = $this-> model_extension_module_filter_holidays -> getStatus();
    }
}