<?php

class ControllerExtensionModuleQuestions extends Controller {
    private $error = array();
    private $result = array();

    public function index() {
        $data['theme_url_img'] = 'catalog/view/theme/7salabim/assets/img';

        $this->load->language('extension/module/questions');

        $data['form_title'] = $this->language->get('form_title');
        $data['form_description'] = $this->language->get('form_description');
        $data['placeholder_name'] = $this->language->get('placeholder_name');
        $data['placeholder_phone'] = $this->language->get('placeholder_phone');
        $data['placeholder_email'] = $this->language->get('placeholder_email');
        $data['placeholder_question'] = $this->language->get('placeholder_question');
        $data['text_form_submit_button'] = $this->language->get('text_form_submit_button');

        return $this->load->view('extension/module/questions', $data);
    }

    public function add() {
        if ( ($this->request->server['REQUEST_METHOD'] = 'POST') && $this->validate() ) {
            $this->load->model('extension/module/questions');
            $this->model_extension_module_questions->addQuestion($this->request->get);
            $this->result['success'] = $this->language->get('title_success_question');

            echo json_encode($this->result);
        } else {
            echo json_encode($this->error);
        }
    }

    public function validate() {
        $this->load->language('extension/module/questions');

        if (!isset($this->request->get['error_valid'])) {
            if (isset($this->request->get['name'])) {
                if ( (utf8_strlen(trim($this->request->get['name'])) < 3) || (utf8_strlen(trim($this->request->get['name'])) > 25) ) {
                    $this->error['type'] = 'name';
                    $this->error['text'] = $this->language->get('error_name');
                }
            }

            if (isset($this->request->get['phone'])) {
                if ( (utf8_strlen(trim($this->request->get['phone'])) < 9) || (utf8_strlen(trim($this->request->get['phone'])) > 12) ) {
                    $this->error['type'] = 'phone';
                    $this->error['text'] = $this->language->get('error_phone');
                }
            }

            if (isset($this->request->get['email'])) {
                if ( !preg_match('/^[\S.]{4,}@[0-9]*[a-zA-Z]{2,}\.[a-zA-Z]{2,6}$/m', trim($this->request->get['email'])) ) {
                    $this->error['type'] = 'email';
                    $this->error['text'] = $this->language->get('error_email');
                }
            }

            if (isset($this->request->get['question'])) {
                if ( (utf8_strlen(trim($this->request->get['question'])) < 10) || (utf8_strlen(trim($this->request->get['question'])) > 500) ) {
                    $this->error['type'] = 'question';
                    $this->error['text'] = $this->language->get('error_question');
                }
            }
        } else {
            switch ($this->request->get['error_valid']) {
                case 'error_name':
                    $this->error['type'] = 'name';
                    $this->error['text'] = $this->language->get('error_name');
                    break;

                case 'error_phone':
                    $this->error['type'] = 'phone';
                    $this->error['text'] = $this->language->get('error_phone');
                    break;

                case 'error_email':
                    $this->error['type'] = 'email';
                    $this->error['text'] = $this->language->get('error_email');
                    break;

                case 'error_question':
                    $this->error['type'] = 'question';
                    $this->error['text'] = $this->language->get('error_question');
                    break;

                default:
                    $this->error['type'] = 'error_fatal';
                    $this->error['text'] = $this->language->get('error_fatal');
            }
        }

        return !$this->error;
    }
}