<?php
class ControllerExtensionModuleSystemLikeReview extends Controller {
    protected $json          = array();
    protected $status_active = false;

    public function index() {
        $this->load->model('extension/module/system_like_review');

        $this->status_active = $this->model_extension_module_system_like_review->checkStatusActive();
    }

    public function setLike() {
        $this->load->model('extension/module/system_like_review');
        $this->status_active = $this->model_extension_module_system_like_review->checkStatusActive();

        if ($this->status_active && $this->request->server['REQUEST_METHOD'] === 'POST') {
            if ( isset($this->request->get['product_id']) && isset($this->request->get['review_id']) && isset($this->request->get['is_like']) ) {
                $this->json['response'] = $this->model_extension_module_system_like_review->setLikeById((int)$this->request->get['review_id'], (int)$this->request->get['product_id'], (int)$this->request->get['is_like']);

            } else $this->json['error'] = 'Product id or Review id or type like review, are not defined! ^(';
        } else $this->json['error'] = 'It`s request method is not POST, or the module is not active! ^(';

        $this->sendResponse();
    }

    public function getLikes() {
        $this->load->model('extension/module/system_like_review');

        if ($this->status_active && $this->request->server['REQUEST_METHOD'] === 'POST') {
            if ( isset($this->request->get['product_id']) && isset($this->request->get['review_id']) ) {
                $this->json['response_likes'] = $this->model_extension_module_system_like_review->getLikesReviews((int)$this->request->get['review_id'], (int)$this->request->get['product_id']);
            }
        } else $this->json['error'] = 'It`s request method is not POST, or the module is not active! ^(';

        $this->sendResponse();
    }

    protected function sendResponse() {
        $this->response->addHeader('Content-Type: application/json');
        echo json_encode($this->json);
    }
}