<?php

//TODO: will added this module for admin
class ControllerExtensionModuleFilterPrice extends Controller {
    private $status_active = false;

    public function getParams() {
        $this -> load -> language('extension/module/filter_price');
        $this -> load -> model('extension/module/filter_price');

        $this->checkStatusActive();

        if ($this->status_active) {
            $params = $this-> model_extension_module_filter_price -> get();

            if ($params) return $params;
        }

        return false;
    }

    protected function checkStatusActive() {
        $this -> load -> model('extension/module/filter_price');

        $this->status_active = $this-> model_extension_module_filter_price -> getStatus();
    }
}