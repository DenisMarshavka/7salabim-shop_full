<?php
class ControllerExtensionModuleFilterAjax extends Controller {
    protected $result         = array(
        'products_length'     => 0,
        'sort_by_two_filters' => false,
        'debug_steps'         => array()
    );

    protected $result_operations = array(
        'first_products' => array(),
        'last_products'  => array()
    );

    protected $sort_by_filter = false;
    protected $debug          = true;
    protected $debug_step     = array();
    public $current_page      = 1;
    public $current_grid      = 20;

    public function index() {
        if ($this->request->server['REQUEST_METHOD'] = 'POST') {
            $this->load->language('product/catalog');

            $this->result['title_product_liked'] = $this->language->get('title_btn_liked');

            $array_filters = array();

            $this->result['filters']            = $this->request->get;
            $this->result['filters_id']         = array();
            $this->result['filter_products_id'] = array();

            if (isset($this->request->get['pagination_count'])) $this->current_page = (int)$this->request->get['pagination_count'];
            if (isset($this->request->get['grid']))             $this->current_grid = (int)$this->request->get['grid'];

            $this->load->model('extension/module/filter_ajax');

            //Filter by Holidays and sex
            $sort_by_filter = $this->model_extension_module_filter_ajax->getFilterId(
                (isset($this->request->get['filter_name'])) ? $this->request->get['filter_name'] : false
            );

            if ($sort_by_filter && !in_array($sort_by_filter, $this->result['filters_id'])) $this->result['filters_id'][] = $sort_by_filter;

            if (isset($this->request->get['sex']) && $this->request->get['sex'] && !in_array($this->request->get['sex'], $this->result['filters_id'])) {
                $this->result['filters_id'][] = $this->model_extension_module_filter_ajax->getFilterId($this->request->get['sex']);
            }

            $this->result['products_new'] = $this->getNewProducts();

            if (isset($this->request->get['filter_id']) && !in_array(trim($this->request->get['filter_id']), $this->result['filters_id'])) {
                $this->result['filters_id'][] = trim($this->request->get['filter_id']);
            }

            if (isset($this->request->get['search'])) $array_filters['filter_search'] = trim($this->request->get['search']);
            if (isset($this->request->get['color']))  $array_filters['filter_color'] = trim($this->request->get['color']);

            if ( isset($this->request->get['min_price']) && isset($this->request->get['max_price']) ) {
                $array_filters['filter_price']['from'] = (int)$this->request->get['min_price'];
                $array_filters['filter_price']['to']   = (int)$this->request->get['max_price'];
            }

            if (isset($this->request->get['pagination_count']) && isset($this->request->get['grid'])) {
                $array_filters['start']   = ($this->current_page - 1) ? $this->current_page : 0;
                $array_filters['limit']   = $this->current_grid;
            }

            //Sort by Popular products
            if (isset($this->request->get['order_by_popular']) && $this->request->get['order_by_popular']) {
                $array_filters['sort']  = 'pd.viewed';
                $array_filters['order'] = 'DESC';
            }

            //Sort by Price products
            if (
                (isset($this->request->get['order_by_price_desc']) && $this->request->get && ['order_by_price_desc'])
                ||
                (isset($this->request->get['order_by_price_asc']) && $this->request->get && ['order_by_price_asc'])
            ) {
                $array_filters['sort']  = 'current_price';
                $array_filters['order'] = (isset($this->request->get['order_by_price_desc'])) ? 'DESC' : 'ASC';
            }

            //Sort by Sale products
            if (isset($this->request->get['order_by_popular']) && $this->request->get['order_by_popular']) {
                $array_filters['sort']  = 'pd.viewed';
                $array_filters['order'] = 'DESC';
            }

            if (isset($this->request->get['order_by_sale']) && $this->request->get && ['order_by_sale']) {
                $array_filters['sort'] = 'special.price';
                $array_filters['sort_by_sale'] = true;
                $array_filters['order'] = 'ASC';
            }

            $this->result['sender_date'] = $array_filters;

            if (!empty($this->result['filters_id'])) {
                foreach ($this->result['filters_id'] as $filter_item) {
                    $array_filters['filter_filter_id'] = $filter_item;

                    $this->result['products'] = $this->model_extension_module_filter_ajax->getProducts($array_filters);
                    $this->setProductsResult();
                }
            } else {
                $this->result['products'] = $this->model_extension_module_filter_ajax->getProducts($array_filters);
                $this->setProductsResult();
            }

            //Sort by New products
            if ($this->result['products_new'] && isset($this->request->get['order_by_new']) && $this->request->get['order_by_new']) {
                $this->result['products'] = $this->filterArraysByIndex($this->result['products_new'], $this->result['products'], 'product_id');
                $this->setProductsResult();
            }

            //Sort by WishList
            if (isset($this->request->get['order_by_wish_list']) && $this->request->get['order_by_wish_list']) {
                if (isset($this->request->get['wish_list']) && !empty($this->request->get['wish_list'])) {
                    $this->result['wish_list_2'] = $this->request->get['wish_list'];

                    $this->result['wish_list_user'] = $this->getNormalizeParamsWishList();
                    $this->result['products'] = $this->filterArraysByIndex($this->result['wish_list_user'], $this->result['products'], 'product_id');
                    $this->setProductsResult();
                } else {
                    $this->result['products'] = array();
                    $this->result['products_length'] = 0;
                }
            }

            $this->result['products'] = $this->generateResultProducts();

            echo json_encode($this->result);
            return;

//            if (count($this->result['filter_products_id']) > 1) {
//                $this->sort_by_filter = true;
//
//                $this->result['products_id'] = $this->filterArraysByIndex($this->result['filter_products_id'][0], $this->result['filter_products_id'][1], 'product_id');
//
//                $this->result['sort_by_two_filters'] = true;
//            } else {
//                $this->result['products_id'] = $this->model_extension_module_filter_ajax->getProductsId($this->result['filters_id'][0]);
//            }

//            $this->result['count_products'] = $this->model_extension_module_filter_ajax->getProductsCount($this->result['products_id'], $this->result['filter_products_id'], false, true);
//            $this->result['really_id_products'] = $this->model_extension_module_filter_ajax->getProductsNotPagination($this->result['products_id'], $this->result['filter_products_id'], false, true);

//            $this->generateProducts($this->model_extension_module_filter_ajax->getProducts(
//                $this->current_page, $this->current_grid, $this->result['products_id'], $this->result['filter_products_id'], false, true),
//                $this->result['really_id_products']
//            );

//            if (isset($this->request->get['color'])) {
//                $this->result['products'] = $this->model_extension_module_filter_ajax->getProductsByColor($this->request->get['color']);
//                $this->setProductsResult();
//            }



//            if ($this->result['products_new'] && isset($this->request->get['order_by_new']) && $this->request->get['order_by_new']) {
//                $this->result['products_sort_array'] = $this->filterArraysByIndex($this->result['products_new'], $this->result['products_id'], 'product_id');
//                $this->setProductsResult();
//
//            } else if ($this->result['products_id'] && isset($this->request->get['order_by_popular']) && $this->request->get['order_by_popular']) {
//
//                $this->result['products_really'] = $this->model_extension_module_filter_ajax->getProductsNotPagination($this->result['products_id'], false, true);
//
//            } else if (isset($this->result['products']) && $this->result['products'] && isset($this->request->get['order_by_price_desc']) && $this->request->get && ['order_by_price_desc']) {
//
//                $this->result['products_really'] = $this->model_extension_module_filter_ajax->getProductsNotPagination($this->result['products_id'], false, false, true);
//            } else if (isset($this->result['products']) && $this->result['products'] && isset($this->request->get['order_by_price_asc']) && $this->request->get && ['order_by_price_asc']) {
//
//                $this->result['products_really'] = $this->model_extension_module_filter_ajax->getProductsNotPagination($this->result['products_id'], false, false, true, 'ASC');
//                $this->result['order_by_price_asc_products_really'] = $this->model_extension_module_filter_ajax->getProductsNotPagination($this->result['products_id'], false, false, true, 'ASC');
//            } else if (isset($this->result['products']) && $this->result['products'] && isset($this->request->get['order_by_sale']) && $this->request->get && ['order_by_sale']) {
//
//                $this->result['products_really'] = $this->model_extension_module_filter_ajax->getProductsNotPagination($this->result['products_id'], false, false, false, 'ASC', true);
//            }


//            if  ( isset($this->request->get['min_price']) && isset($this->request->get['max_price']) ) {
//                $this->sortByPrice($this->request->get['min_price'], $this->request->get['max_price']);
//            }

//            if (isset($this->request->get['search']) && $this->request->get['search']) {
//                $this->result['search_result_id'] = $this->model_extension_module_filter_ajax->getProductBySearch($this->request->get['search']);
//                $this->result['products'] = $this->filterArraysByIndex($this->result['search_result_id'], $this->result['products'], 'product_id');
//                $this->result['count_products'] = count($this->result['products']);
//            }

//            if (isset($this->result['count_products'])) $this->result['products_length'] = $this->result['count_products'];

//            echo json_encode($this->result);
        }
    }

    protected function getNormalizeParamsWishList() {
        $result_wish_list = array();
        $received_data_wish_list = explode('wish_list[]=', $this->request->get['wish_list'][0]);

        foreach ($received_data_wish_list as $wish_item) {
            $wish_item_params = array(
                'product_id' => (int)$wish_item
            );

            $result_wish_list[] = $wish_item_params;
        }

        return $result_wish_list;
    }

    protected function generateResultProducts() {
        $generate_step   = 0;
        $result_products = array();

        if (isset($this->result['products']) && $this->result['products']) {
            foreach ($this->result['products'] as $product_key => $val) {
                if (isset($this->result['products'][$product_key]['name'])) $this->result['products'][$product_key]['name'] = mb_strimwidth(trim($this->result['products'][$product_key]['name']), 0, 30, "...");
                if (isset($this->result['products'][$product_key]['model'])) $this->result['products'][$product_key]['model'] = mb_strimwidth(trim($this->result['products'][$product_key]['model']), 0, 20, "...");
                $this->result['products'][$product_key]['href'] = $this->url->link('product/product', '&product_id=' . $this->result['products'][$product_key]['product_id']);
            }
        }

        if (isset($this->request->get['search'])) {
            $this->result['search_val']   = mb_strimwidth(trim($this->request->get['search']), 0, 20, "...");
            $this->result['search_title'] = $this->language->get('title_select_default_panel_filter_by_search');
        }

        $start_offset       = ($this->current_page - 1) ? $this->current_page - 1 : 0;
        $products_grid      = $this->current_grid;
        $start_id_products  = ((int)$start_offset) ? ((int)$start_offset * (int)$products_grid) : (int)$start_offset;

        while ($generate_step < (int)$products_grid) {
            if (isset($this->result['products'][$start_id_products])) {
                if (!empty($result_products)) {
                    $result_products[] = $this->result['products'][$start_id_products];
                } else  $result_products[] = $this->result['products'][$start_id_products];
            }

            ++$start_id_products;
            ++$generate_step;
        }

        return $result_products;
    }

    protected function setProductsResult() {
        $result = array();

        $this->result_operations['step'][] = $this->result['products'];

        if (empty($this->result_operations['first_products']) && count($this->result_operations['step']) === 1) {
            $this->result_operations['first_products'] = $this->result['products'];
        } else {
            $this->result['products'] = $this->filterArraysByIndex($this->result_operations['first_products'], $this->result['products'], 'product_id');

            $this->result_operations['first_products'] = $this->result['products'];
            $this->result_operations['last_products']  = array();
        }

        $this->result['products_length'] = count($this->result['products']);

        if ($this->debug) {
            $result['products'] = $this->result['products'];
            $result['products_length'] = count($this->result['products']);

            $this->result['debug_steps'][] = $result;
        }
    }

    public function sortByPrice($min_price = false, $max_price = false) {
        if ($min_price !== false && $max_price !== false) {
            $this->result['products_id_sort_price'] = array();
            $this->result['products_sort_price'] = array();

            foreach ($this->result['products'] as $product) {
                if ($product['sale_price'] !== null && (int)$product['sale_price'] < (int)$product['price']) {
                    $price = $product['sale_price'];
                } else $price = $product['price'];

                $this->result['debug'][] = array(
                    'min' => (int)$min_price,
                    'max' => (int)$max_price,
                    'price' => (int)$price,
                );

                if ( (int)$price >= (int)$min_price && (int)$price <= (int)$max_price ) {
                    $product['current_price'] = round($price, 2);

                    $this->result['products_sort_price'][] = $product;
                    $this->result['products_id_sort_price'][] = array(
                        'product_id' => $product['product_id']
                    );
                }
            }

            $this->result['products_id'] = $this->result['products_id_sort_price'];
            $this->result['count_products'] = COUNT($this->result['products_id']);

            if ($this->result['count_products']) {
                $sort_by_price = (isset($this->request->get['order_by_price_desc']) && $this->request->get['order_by_price_desc']) || (isset($this->request->get['order_by_price_asc']) && $this->request->get['order_by_price_asc']);

                if ($sort_by_price) {
                    if (isset($this->request->get['order_by_price_desc']) && $this->request->get['order_by_price_desc']) $order_by = 'DESC';
                    if (isset($this->request->get['order_by_price_asc']) && $this->request->get['order_by_price_asc']) $order_by = 'ASC';
                } else $order_by = 'ASC';

                $this->result['products'] = $this->model_extension_module_filter_ajax->getProductsNotPagination(
//                    $this->current_page,
//                    $this->current_grid,
                    $this->result['products_id'],
                    false,
                    isset($this->request->get['order_by_popular']) && $this->request->get['order_by_popular'],
                    $sort_by_price,
                    $order_by,
                    isset($this->request->get['order_by_sale']) && $this->request->get['order_by_sale']
                );

                $this->result['order_by_price_desc_||_order_by_price_asc'] = $sort_by_price;
                $this->result['o_order_by_price'] = $order_by;
                $this->result['order_by_sale'] = isset($this->request->get['order_by_sale']) && $this->request->get && ['order_by_sale'];

            } else $this->result['products'] = array();
        }
    }

    public function getNewProducts() {
        $result_array = array();

//        if (isset($this->result['products']) && $this->result['products']) {
            $this->load->model('catalog/product');
            $result = $this->model_catalog_product->getNewProducts(false, $this->config->get('config_language_id'));

            if ($result) {
                foreach ($result as $product_item) {
                    if ($product_item['pr_id']) {
                        $result_array[] = array(
                            'product_id' => $product_item['pr_id']
                        );
                    }
                }
            }
//        }

//        if (isset($this->request->get['order_by_new']) && $this->request->get['order_by_new']) $this->result['count_products'] = count($result_array);
        return $result_array;
    }

    public function filterArraysByIndex($first_array = array(), $last_array = array(), $some_index = false) {
        if ($first_array && $last_array && $some_index) {
            $some_products_id = array();

            $this->result['some_find']['counts'][] = array(
                'first_array' => $first_array,
                'last_array'  => $last_array,
                'some_index'  => $some_index
            );

            foreach ($first_array as $first_item) {
                if (isset($first_item[$some_index]) && $first_item[$some_index]) {
                    foreach ($last_array as $last_item) {
                        if (isset($last_item[$some_index]) && $last_item[$some_index]) {
                            if ( (int)$first_item[$some_index] === (int)$last_item[$some_index] ) {
                                $some_products_id[] = $first_item;
                            }
                        }
                    }
                }
            }

            $this->result['some_find']['result'] = $some_products_id;
            return $this->result['some_find']['result'];
        }

        return array();
    }
}