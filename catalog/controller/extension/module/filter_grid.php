<?php

//TODO: will add this module for admin
class ControllerExtensionModuleFilterGrid extends Controller {
    private $status_active = true;

    public function getItems() {
        $this -> load -> language('extension/module/filter_grid');
        $this -> load -> model('extension/module/filter_grid');

        if ($this->status_active) {
            $grid_items = $this-> model_extension_module_filter_grid -> get();

            if ($grid_items) return $grid_items;
        }

        return false;
    }

    //TODO: will add this method
//    protected function checkStatusActive() {
//        $this -> load -> model('extension/module/filter_grid');
//
//        $this->status_active = $this-> model_extension_module_filter_grid -> getStatus();
//    }
}