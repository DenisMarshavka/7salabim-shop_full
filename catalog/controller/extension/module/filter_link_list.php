<?php
class ControllerExtensionModuleFilterLinkList extends Controller {
    public function index() {
        $data['theme_url_img'] = '/image/';

        $this->load->model('extension/module/filter_link_list');
        $data['filter_links'] = $this->model_extension_module_filter_link_list->getFilterLinks();

        if (!empty($data['filter_links'])) {
            foreach ($data['filter_links'] as $current_filter => $val) {
                if (isset($data['filter_links'][$current_filter]['filter_id'])) {
                    if ( $_SERVER['SCRIPT_URI'] . '?' . $_SERVER['QUERY_STRING'] !== trim($this->url->link('product/catalog') . '&' . 'filter_id=' . $data['filter_links'][$current_filter]['filter_id']) ) {
                        $data['filter_links'][$current_filter]['filter_href'] = $this->url->link('product/catalog') .  '&filter_id=' . $data['filter_links'][$current_filter]['filter_id'];
                    } else $data['filter_links'][$current_filter]['filter_href'] = 'javascript:void(0)';
                }
            }
        }

        return $this->load->view('extension/module/filter_link_list', $data);
    }
}