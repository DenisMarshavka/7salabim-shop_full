<?php

class ControllerExtensionModulePanelPriority extends Controller {
    public function index() {
        $data['theme_url_img'] = '/image/';

        $this->load->language('extension/module/panel_priority');
        $data['panel_priority_title'] = $this->language->get('panel_priority_title');

        $this->load->model('extension/module/panel_priority');
        $data['panel_priorities'] = $this->model_extension_module_panel_priority->getPanelPriorities();

        return $this->load->view('extension/module/panel_priority', $data);
    }
}
