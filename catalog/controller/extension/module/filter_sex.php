<?php

//TODO: will added this module for admin
class ControllerExtensionModuleFilterSex extends Controller {
    private $status_active = false;

    public function getItems() {
        $this -> load -> language('extension/module/filter_sex');
        $this -> load -> model('extension/module/filter_sex');

        $this->checkStatusActive();

        if ($this->status_active) {
            $sex_items = $this-> model_extension_module_filter_sex -> get();

            if ($sex_items) return $sex_items;
        }

        return false;
    }

    protected function checkStatusActive() {
        $this -> load -> model('extension/module/filter_sex');

        $this->status_active = $this-> model_extension_module_filter_sex -> getStatus();
    }
}