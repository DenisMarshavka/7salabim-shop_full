<?php

class ControllerSettingSetting extends Controller {
    public function getChangelanguage() {
        if (isset($this->request->get['code'])) {
            $this->load->model('setting/setting');

            $data['language'] = $this->model_setting_setting->editSettingValue( 'config', 'config_language', trim($this->request->get['code']), $this->config->get('config_store_id') );

            unset($_COOKIE['language']);
            unset($_COOKIE['OCSESSID']);
            setcookie('OCSESSID', null, -1, '/');
            setcookie('language', $this->request->get['code'], -1, '/');
            $this->load->view('common/home', $data);
        }
    }
}