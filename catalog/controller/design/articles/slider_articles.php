<?php

date_default_timezone_set('Europe/Kiev');

class ControllerDesignArticlesSliderArticles extends Controller {
    public function get($get_popular = false) {
        $this->load->language('design/articles/slider_articles');
        $this->load->model('design/articles/slider_articles');

        $data['url_img'] = '/image/';
        $data['theme_url_img'] = 'catalog/view/theme/7salabim/assets/img';

        $data['btn_to_blog'] = false;

        if ($get_popular) {
            $data['title_slider_articles'] = $this->language->get('slider_articles_popular');

            $data['slider'] = $this->model_design_articles_slider_articles->get(
                array(
                    'sort'  => 'id.viewed',
                    'order' => 'DESC',
                    'start' => 0,
                    'limit' => 20
                ),
                true
            );
        } else {
            $data['title_slider_articles'] = $this->language->get('slider_articles');

            $data['btn_to_blog'] = true;
            $data['url_to_blog'] = $this->url->link('information/blog');

            $data['slider'] = $this->model_design_articles_slider_articles->get(
                array(
                    'sort'  => 'id.date_added',
                    'order' => 'DESC',
                    'start' => 0,
                    'limit' => 20
                ),
                true
            );
        }

        $data['slider'] = $this->getNormaliseItems($data['slider']);

        return $this->load->view('design/articles/slider_articles', $data);
    }

    protected function getNormaliseItems($articles = array()) {
        $this->load->language('design/articles/slider_articles');

        if (!empty($articles)) {
            foreach ($articles as $key => $val) {
                if (isset($articles[$key]['information_id'])) {
                    if ( $_SERVER['SCRIPT_URI'] . '?' . $_SERVER['QUERY_STRING'] !== trim($this->url->link('information/article') . '&' . 'article_id=' . $articles[$key]['information_id']) ) {
                        $articles[$key]['link'] = $this->url->link('information/article', 'article_id=' . $articles[$key]['information_id']);
                    } else $articles[$key]['link'] = 'javascript:void(0)';
                }

                if (isset($articles[$key]['title'])) {
                    $articles[$key]['title'] = mb_strimwidth($articles[$key]['title'], 0, 32, "...");
                }

                if (isset($articles[$key]['description'])) {
                    $articles[$key]['text'] = mb_strimwidth(strip_tags(html_entity_decode($articles[$key]['description'], ENT_QUOTES, 'UTF-8')), 0, 133, "...");
                }

                if (isset($articles[$key]['date_added'])) {
                    $articles[$key]['date'] = date('d.m.Y', strtotime($articles[$key]['date_added']));
                    $data['date_hours'] = date('H:i', strtotime($articles[$key]['date_added']));

                    $data_received_date_array = explode('.', $articles[$key]['date']);
                    $current_date_array = explode('.', date('d.m.Y'));

                    $data_received_hours_array = explode(':', $data['date_hours']);
                    $current_hours_array = explode(':', date('H:i'));

                    if ($data_received_date_array[0] === $current_date_array[0] && $data_received_date_array[1] === $current_date_array[1]) {
                        if ((int)$current_hours_array[0] >= (int)$data_received_hours_array[0]) {
                            $data_between_dates = (int)$current_hours_array[0] - (int)$data_received_hours_array[0];

                            if (($data_between_dates <= 4) && ($data_between_dates > 0)) {
                                $articles[$key]['date'] = $data_between_dates . ' ' . $this->language->get('title_hours');
                            } elseif (($data_between_dates) === 0) {
                                $articles[$key]['date'] = $this->language->get('title_newly');
                            }
                        }
                    }
                }
            }

            return $articles;
        }

        return false;
    }
}