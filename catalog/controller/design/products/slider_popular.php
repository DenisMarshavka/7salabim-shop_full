<?php

class ControllerDesignProductsSliderPopular extends Controller {
    public function index($data = false) {
        if ($data) {
            $data['theme_url_img'] = '/image/';

            $this->load->language('design/products/slider_popular');
            $data['products_popular_title'] = $this->language->get('products_popular_title');
            $data['selected_title'] = $this->language->get('selected_title');

            $this->load->model('catalog/product');

            //TODO: well modify params 1 on automatic, of the functions "getNewProducts"
            $data['products'] = $this->model_catalog_product->getPopularProducts(10);

            if (isset($data['products']) && $data['products']) {
                foreach ($data['products'] as $product => $val) {
                    if (isset($data['products'][$product]['price'])) $data['products'][$product]['price'] = round($data['products'][$product]['price'], 1);
                    if (isset($data['products'][$product]['special'])) $data['products'][$product]['special'] = round($data['products'][$product]['special'], 1);
                    if (isset($data['products'][$product]['model'])) $data['products'][$product]['model'] = mb_strimwidth(trim($data['products'][$product]['model']), 0, 15, "...");
                    if (isset($data['products'][$product]['name'])) $data['products'][$product]['name'] = mb_strimwidth(trim($data['products'][$product]['name']), 0, 25, "...");
                    if (isset($data['products'][$product]['product_id'])) $data['products'][$product]['href'] = $this->url->link('product/product', 'product_id=' . (int)$data['products'][$product]['product_id']);
                    if (isset($data['products'][$product]['quantity'])) $data['products'][$product]['quantity'] = (int)$data['products'][$product]['quantity'];
                }
            }
        }

        return $this->load->view('design/products/slider_popular', $data);
    }
}