<?php

class ControllerDesignProductsSliderNew extends Controller {
    public function index($data = false) {
        if ($data) {
            $data['theme_url_img'] = '/image/';

            $this->load->language('design/products/slider_new');
            $data['products_new_title'] = $this->language->get('products_new_title');
            $data['selected_title'] = $this->language->get('selected_title');

            $this->load->model('catalog/product');

            //TODO: well modify params 1 on automatic, of the function "getNewProducts"
            $data['products'] = $this->model_catalog_product->getNewProducts(10, $this->config->get('config_language_id'));

            if (isset($data['products']) && $data['products']) {
                for ($e = 0; $e < count($data['products']); $e++) {
                    if (isset($data['products'][$e]['old_price'])) $data['products'][$e]['old_price'] = round($data['products'][$e]['old_price'], 1);
                    if (isset($data['products'][$e]['price'])) $data['products'][$e]['price'] = round($data['products'][$e]['price'], 1);
                    if (isset($data['products'][$e]['model'])) $data['products'][$e]['model'] = mb_strimwidth(trim($data['products'][$e]['model']), 0, 15, "...");
                    if (isset($data['products'][$e]['name'])) $data['products'][$e]['name'] = mb_strimwidth(trim($data['products'][$e]['name']), 0, 25, "...");
                    if (isset($data['products'][$e]['pr_id'])) $data['products'][$e]['href'] = $this->url->link('product/product', 'product_id=' .(int)$data['products'][$e]['pr_id']);
                    if (isset($data['products'][$e]['quantity'])) $data['products'][$e]['quantity'] = (int)$data['products'][$e]['quantity'];
                }
            }

            return $this->load->view('design/products/slider_new', $data);
        }
    }
}