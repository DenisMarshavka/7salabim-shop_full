<?php

class ControllerDesignProductsSliderSale extends Controller {
    public function index($data = false) {
        if ($data) {
            $data['theme_url_img'] = '/image/';

            $this->load->model('catalog/product');

            $data['products'] = $this->model_catalog_product->getProductSpecials();

            $this->load->language('design/products/slider_sale');
            $data['products_sale_title'] = $this->language->get('products_sale_title');
            $data['selected_title'] = $this->language->get('selected_title');

            if (isset($data['products']) && $data['products']) {
                $number_order_product_lg = 1;
                $number_order_product_md = 1;

                $count_array_id_product_lg = 0;
                $count_array_id_product_md = 0;

                foreach ($data['products'] as $product => $val) {
                    if (isset($data['products'][$product]['price'])) $data['products'][$product]['price'] = round($data['products'][$product]['price'], 1);
                    if (isset($data['products'][$product]['special'])) $data['products'][$product]['special'] = round($data['products'][$product]['special'], 1);
                    if (isset($data['products'][$product]['model'])) $data['products'][$product]['model'] = mb_strimwidth(trim($data['products'][$product]['model']), 0, 15, "...");
                    if (isset($data['products'][$product]['name'])) $data['products'][$product]['name'] = mb_strimwidth(trim($data['products'][$product]['name']), 0, 25, "...");
                    if (isset($data['products'][$product]['product_id'])) $data['products'][$product]['href'] = $this->url->link('product/product', 'product_id=' . (int)$data['products'][$product]['product_id']);
                    if (isset($data['products'][$product]['quantity'])) $data['products'][$product]['quantity'] = (int)$data['products'][$product]['quantity'];

                    if ($number_order_product_lg <= 5) {
                        $data['products_response']['lg'][$count_array_id_product_lg][] = $data['products'][$product];
                        ++$number_order_product_lg;
                    } else {
                        ++$count_array_id_product_lg;
                        $data['products_response']['lg'][$count_array_id_product_lg][] = $data['products'][$product];

                        $number_order_product_lg = 1;
                    }

                    if ($number_order_product_md <= 3) {
                        $data['products_response']['md'][$count_array_id_product_md][] = $data['products'][$product];
                        ++$number_order_product_md;
                    } else {
                        ++$count_array_id_product_md;
                        $data['products_response']['md'][$count_array_id_product_md][] = $data['products'][$product];

                        $number_order_product_md = 1;
                    }

                    $data['products_response']['mob'][] = $data['products'][$product];
                }
            }
        }

        return $this->load->view('design/products/slider_sale', $data);
    }
}