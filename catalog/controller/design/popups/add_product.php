<?php
class ControllerDesignPopupsAddProduct extends Controller {
    public function index($data = false) {
        $data['theme_url_img_assets'] = 'catalog/view/theme/7salabim/assets/img';

        $data['basket_href'] = $this->url->link('product/basket');

        $this->load->language('design/popups/add_product');

        $data['add_product_title'] = $this->language->get('add_product_title');
        $data['add_product_description'] = $this->language->get('add_product_description');

        $data['end_btn_product'] = $this->language->get('end_btn_product');
        $data['continue_btn_product'] = $this->language->get('continue_btn_product');

        return $this->load->view('design/popups/add_product', $data);
    }
}