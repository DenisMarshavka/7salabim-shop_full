<?php
class ControllerDesignBlocks extends Controller {
    public function index($data = false) {
        $data['banner_path_img'] = '/image/';

        if ($data) {
            $this->load->model('design/banner');
            $data['banners'] = $this->model_design_banner->getBanners();

            $this->load->language('design/blocks');

            if ($data['blocks']) {
                for ($e = 0; $e < count($data['blocks']); $e++) {
                    switch ($data['blocks'][$e]['code']) {
                        case 'carousel.33':
                            $data['products_popular'] = true;
                            break;
                        case 'carousel.32':
                            $data['products_new'] = true;
                            break;
                        case 'carousel.37':
                            $data['products_buying'] = true;
                            break;
                        case 'carousel.35':
                            $data['carousel_articles'] = true;
                            break;
                        case 'carousel.29':
                            $data['products_sale'] = true;
                            break;
                        case 'carousel.36':
                            $data['products_similar'] = true;
                            break;
                        case 'slideshow.27':
                            $data['stocks_slider'] = true;
                            break;
                        case 'featured.28':
                            $data['filter_links_panel'] = true;
                            break;
                        case 'featured.40':
                            $data['priority_list'] = true;
                    }
                }

                return $this->load->view('design/blocks', $data);
            }
        }
    }
}