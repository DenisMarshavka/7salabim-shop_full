<?php
class ControllerErrorPayment extends Controller {
	public function index() {
		$this->load->language('error/payment');

		$this->document->setTitle($this->language->get('heading_title'));

        $data['theme_url_img_assets'] = 'catalog/view/theme/7salabim/assets/img';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => '7salabim',
			'href' => $this->url->link('common/home')
		);

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('bread_crumbs_title'),
            'href' => 'javascript:void(0);'
        );

        $this->load->model('extension/payment/wayforpay');

        $data['error_code'] = '1001';
        $data['error_title'] = $this->language->get('error_title');
        $data['error'] = $this->model_extension_payment_wayforpay->getDataError(1, $this->config->get('config_language_id'));

        if (isset($this->request->get['route']) && isset($this->request->get['error_code']) && (int)$this->request->get['error_code']) {
            $url_data = $this->request->get;

            $data['error_code'] = (int)$url_data['error_code'];
            $data['error'] = $this->model_extension_payment_wayforpay->getDataError($data['error_code'], $this->config->get('config_language_id'));
		}

		$data['continue_href'] = $this->url->link('checkout/payment_address');

        $this->document->addStyle('catalog/view/theme/7salabim/assets/css/main.css');

        $this->document->addScript('catalog/view/javascript/jquery/jquery.min.js', 'footer');
        $this->document->addScript('catalog/view/javascript/common.js', 'footer');
        $this->document->addScript('catalog/view/javascript/Ajaxes.js', 'footer');
        $this->document->addScript('catalog/view/javascript/libraries/owl.carousel.min.js', 'footer');

		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

		$this->response->setOutput($this->load->view('error/payment', $data));
	}
}