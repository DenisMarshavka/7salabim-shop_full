<?php
class ControllerErrorNotFound extends Controller {
	public function index() {
		$this->load->language('error/not_found');

		$this->document->setTitle($this->language->get('heading_title'));

        $data['theme_url_img_assets'] = 'catalog/view/theme/7salabim/assets/img';

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => '7salabim',
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('bread_crumbs_title'),
            'href' => 'javascript:void(0);'
        );

		if (isset($this->request->get['route'])) {
			$url_data = $this->request->get;

			unset($url_data['_route_']);
			unset($url_data['route']);

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('heading_title'),
				'href' => 'javascript:void(0)'
			);
		}

		$data['error_code'] = 404;
		$data['error_title'] = $this->language->get('title_text');
		$data['error_text'] = $this->language->get('description_text');

        $data['continue_btn_text'] = $this->language->get('continue_btn_text');
        $data['continue_href'] = $this->url->link('product/catalog');

        $this->document->addStyle('catalog/view/theme/7salabim/assets/css/main.css');

        $this->document->addScript('catalog/view/javascript/jquery/jquery.min.js', 'footer');
        $this->document->addScript('catalog/view/javascript/common.js', 'footer');
        $this->document->addScript('catalog/view/javascript/Ajaxes.js', 'footer');
        $this->document->addScript('catalog/view/javascript/libraries/owl.carousel.min.js', 'footer');

		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

		$this->response->setOutput($this->load->view('error/not_found', $data));
	}
}