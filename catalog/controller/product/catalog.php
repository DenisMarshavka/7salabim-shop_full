<?php
class ControllerProductCatalog extends Controller {
    public $data;
    public $query;

    public function index() {
        $this->load->model('catalog/information');

        $data['theme_url_img_assets'] = 'catalog/view/theme/7salabim/assets/img';
        $data['theme_url_img'] = 'catalog/view/theme/7salabim/assets/img';

        $route = $this->request->get['route'];

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '7salabim/template/product/catalog.twig')) {
            $this->template = $this->config->get('config_template') . '7salabim/template/product/catalog.twig';
            $this->data['template'] = $this->config->get('config_template');
        } else {
            $this->template = '7salabim/template/product/catalog.twig';
        }

        $data['breadcrumbs'][] = array(
            'text'      => '7salabim',
            'href'      => $this->url->link('common/home'), // путь маршрута на главную страницу
            'separator' => false
        );
        // Привязываем текущую страницу к хлебным крошкам
        $data['breadcrumbs'][] = array(
            'text'      => 'Каталог',
            'href'      => $this->url->link($route), // путь маршрута к текущему url
            'separator' => false //Символ разделителя для Хлебных крошек
        );

        $data['language_id'] = $this->config->get('config_language_id');

        $this->load->model('design/layout');
        $data['layout_id'] = (isset($this->request->request['route'])) ? $this->model_design_layout->getLayout($this->request->request['route']) : 1;

        //Set title and description
        $this->load->model('catalog/category');
        $data['category_id'] = $this->model_catalog_category->getCategoryIdByLayoutId($data['layout_id']);
        $data['category_info'] = $this->model_catalog_category->getCategory($data['category_id']);

        $this->document->setTitle( trim(strip_tags($data['category_info']['meta_title'])) );
        $this->document->setDescription( trim(strip_tags($data['category_info']['meta_description'])) );

        $this->load->language('product/catalog');

        //filters grid
        $data['panel_filter_grid']     = $this->getFiltersGrid();

        //Filter panels
        $data['panel_filter_price']    = $this->getFiltersByPrice();
        $data['panel_filter_holidays'] = $this->getFiltersByHolidays();
        $data['panel_filter_colors']   = $this->getFiltersByColor();
        $data['panel_filter_sex']      = $this->getFiltersBySex();

        $this->document->addStyle('catalog/view/theme/7salabim/assets/css/main.css');

        $this->document->addScript('catalog/view/javascript/jquery/jquery.min.js', 'footer');
        $this->document->addScript('catalog/view/javascript/jquery/my_plugins/jquery.sumSale.js', 'footer');
        $this->document->addScript('catalog/view/javascript/common.js', 'footer');
        $this->document->addScript('catalog/view/javascript/Ajaxes.js', 'footer');
        $this->document->addScript('catalog/view/javascript/FilterProducts.js', 'footer');
        $this->document->addScript('catalog/view/javascript/sliders.js', 'footer');
        $this->document->addScript('catalog/view/javascript/libraries/ion.rangeSlider.min.js', 'footer');
        $this->document->addScript('catalog/view/javascript/myscript.rangeSlider.js', 'footer');
        $this->document->addScript('catalog/view/javascript/libraries/owl.carousel.min.js', 'footer');


        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $this->response->setOutput($this->load->view('product/catalog', $data));
    }

    protected function getFiltersGrid() {
        $grid_values = $this->load->controller('extension/module/filter_grid/getItems');

        if (!empty($grid_values)) return $grid_values;

        return false;
    }

    protected function getFiltersByPrice() {
        $params = $this->load->controller('extension/module/filter_price/getParams');

        if (!empty($params)) return $params;

        return false;
    }

    protected function getFiltersByHolidays() {
        $filters_value = $this->load->controller('extension/module/filter_holidays/getItems');

        if (!empty($filters_value)) return $filters_value;

        return false;
    }

    protected function getFiltersByColor() {
        $filters_value = $this->load->controller('extension/module/filter_colors/getItems');

        if (!empty($filters_value)) return $filters_value;

        return false;
    }

    protected function getFiltersBySex() {
        $filters_value = $this->load->controller('extension/module/filter_sex/getItems');

        if (!empty($filters_value)) return $filters_value;

        return false;
    }
}