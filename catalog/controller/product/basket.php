<?php
class ControllerProductBasket extends Controller {
    public $data;

    public function index() {
        $this->getTranslation();

        $data['breadcrumbs'][] = array(
            'text'      => '7salabim',
            'href'      => $this->url->link('common/home'),// путь маршрута на главную страницу
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text'      => 'Корзина',
            'href'      => 'javascript:void(0);',
            'separator' => false
        );

        $this->load->model('design/layout');
        $data['layout_id'] = (isset($this->request->request['route'])) ? $this->model_design_layout->getLayout($this->request->request['route']) : 1;

        //Set title and description
        $this->load->model('catalog/category');
        $data['category_id'] = $this->model_catalog_category->getCategoryIdByLayoutId($data['layout_id']);
        $data['category_info'] = $this->model_catalog_category->getCategory($data['category_id']);

        $this->document->setTitle( trim(strip_tags($data['category_info']['meta_title'])) );
        $this->document->setDescription( trim(strip_tags($data['category_info']['meta_description'])) );

        $data['theme_url_img_assets'] = 'catalog/view/theme/7salabim/assets/img';

        $this->document->addStyle('catalog/view/theme/7salabim/assets/css/main.css');

        $this->document->addScript('catalog/view/javascript/jquery/jquery.min.js', 'footer');
        $this->document->addScript('catalog/view/javascript/common.js', 'footer');
        $this->document->addScript('catalog/view/javascript/Ajaxes.js', 'footer');
        $this->document->addScript('catalog/view/javascript/libraries/owl.carousel.min.js', 'footer');

        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        $data['catalog_href'] = $this->url->link('product/catalog');
        $data['submit_href'] = $this->url->link('checkout/payment_address');

        $this->response->setOutput($this->load->view('product/basket', $data));
    }

    public function getList() {
        $data['theme_url_img'] = '/image/';

        $this->getTranslation();

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && isset($this->request->post)) {
            $step = 1;

            for ($i = 0; $i < count($this->request->post); $i++) {
                if (isset($this->request->post[$step . 'id']) && isset($this->request->post[$step . 'count'])) {
                    $product_item = array(
                        'id' => $this->request->post[$step . 'id'],
                        'count' => $this->request->post[$step . 'count']
                    );

                    if (isset($this->request->post[$step . 'color']) && $this->request->post[$step . 'color'] !== 'default') {
                        $product_item['color'] = $this->request->post[$step . 'color'];
                    }

                    $data['added_products_post'][] = $product_item;

                    $step++;
                }
            }

            if (!empty($data['added_products_post'])) {
                $this->load->model('product/basket');

                $data['added_products'] = $this->model_product_basket->getAddedProducts($data['added_products_post']);

                if (!empty($data['added_products'])) {
                    if (is_array($data['added_products'])) {
                        foreach ($data['added_products'] as $product_item => $val) {
                            $data['added_products'][$product_item]['price'] = (isset($data['added_products'][$product_item]['sale_price']) && ((int)$data['added_products'][$product_item]['sale_price'] < (int)$data['added_products'][$product_item]['price'])) ? round($data['added_products'][$product_item]['sale_price'], 2) : round($data['added_products'][$product_item]['price'], 2);
                            $data['added_products'][$product_item]['image'] = $data['theme_url_img'] . $data['added_products'][$product_item]['image'];
                            $data['added_products'][$product_item]['href'] = $this->url->link('product/product') . '&product_id=' . (int)$data['added_products'][$product_item]['product_id'];

                            foreach ($data['added_products_post'] as $product_post_item => $val) {
                                if ((int)$data['added_products_post'][$product_post_item]['id'] === (int)$data['added_products'][$product_item]['product_id']) {
                                    $data['added_products'][$product_item]['count'] = ((int)$data['added_products_post'][$product_post_item]['count'] <= (int)$data['added_products'][$product_item]['quantity']) ? (int)$data['added_products_post'][$product_post_item]['count'] : (int)$data['added_products'][$product_item]['quantity'];

                                    if (isset($data['added_products_post'][$product_post_item]['color'])) {
                                        $data['added_products'][$product_item]['color'] = $data['added_products_post'][$product_post_item]['color'];
                                    }
                                }
                            }
                        }

                        $this->session->data['data_added_products'] = $data['added_products'];
                    }

                    echo json_encode($data['added_products']);
                }
            } else {
                $this->session->data['data_added_products'] = [];
                echo 'Error, the basket is empty';
            }
        }
    }

    public function getTranslation() {
        $this->load->language('product/basket');

        $data['title_list_photo'] = $this->language->get('title_list_photo');
        $data['title_list_name'] = $this->language->get('title_list_name');
        $data['title_list_count'] = $this->language->get('title_list_count');
        $data['title_list_price'] = $this->language->get('title_list_price');

        $data['placeholder_input'] = $this->language->get('placeholder_input');

        $data['confirm_btn'] = $this->language->get('confirm_btn');
        $data['reset_btn'] = $this->language->get('reset_btn');

        $data['title_all_price'] = $this->language->get('title_all_price');

        $data['order_btn'] = $this->language->get('order_btn');
        $data['continue_purchase'] = $this->language->get('continue_purchase');

        $data['title_basket_empty'] = $this->language->get('title_basket_empty');
        $data['description_basket_empty'] = $this->language->get('description_basket_empty');
        $data['btn_basket_empty'] = $this->language->get('btn_basket_empty');
    }
}
