<?php

class ControllerProductFormReview extends Controller {
    public function index() {
        $data['theme_url_img'] = 'catalog/view/theme/7salabim/assets/img';

        $this->load->language('product/form_review');

        return $this->load->view('product/form_review', $data);
    }
}