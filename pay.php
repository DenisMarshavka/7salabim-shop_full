Оплата и доставка
В интернет-магазине 7salabim предусмотрены следующие варианты доставки:

Самовывоз (Черновцы)
Курьерская доставка (по Черновцам)
Доставка перевозчиком (вся Украина)
Самовывоз
Черновцы
Пр-т Маяковского, 4 (в помещении Microtron)

Вы можете самостоятельно забрать заказ из точки выдачи в Запорожье. Время доставки заказов в точку выдачи оговаривается менеджером. При заказе до 16:00 в будний день, в тот же день вы сможете его забрать.

График работы розничного магазина Kibet:

пн-пт – с 10:00 до 19:00, субб - с 10:00 до 15:00 без перерыва, вс - выходной


Курьерская доставка по г. Запорожье
Курьеры доставляют товар в будние дни после 14:00 (при заказе до 13:30 отправка в тот же день). Стоимость доставки товара зависит от района города:

- Центр города - 35 грн

- Хортицкий - 75 грн

- Заводской - 50 грн

- Бородинский, Осипенковский - 55 грн

- Космический - 60 грн

- Шевченковский - 50 грн

- Южный мкрн - 55 грн

Обращаем ваше внимание на то, что наши курьеры доставляют товар только до подъезда. В целях безопасности им запрещается заходить в здание, квартиру, офис.

Доставка компанией перевозчиком по Украине
Интернет-мультимаркет Kibet осуществляет доставку перевозчиками по всей Украине. На следующее утро после отправки заказа наш логист сообщит вам номер багажной декларации. По Украине доставка в населенные пункты занимает в большинстве случаев 1-2 дня. Минимальный заказ не ограничен. Стоимость доставки оплачивает покупатель в соответствии с тарифами транспортной компании.

При получении товара необходимо иметь при себе паспорт и номер декларации.

Мы пользуемся услугами приведенных ниже компаний перевозчиков. Каким перевозчиком отправлять заказ – выбираете вы.

Новая Почта (https://novaposhta.ua) – отправка как по предоплате, так и наложенным платежом*.

*Обратите внимание, услугу наложенного платежа оплачивает клиент: 20 грн за оформление услуги + 2% от суммы заказа за обратную пересылку денег.

Доставленный товар хранится в отделении «Новой почты» в городе прибытия 5 дней. Если вы не получили свой заказ в течении этого срока, он возвращается обратно. Если вы не успеваете забрать заказ в этот срок, сообщите нам – мы продлим хранение.

Интайм (www.intime.com.ua) – отправка только по предоплате.

Деливери (http://www.delivery-auto.com/) – отправка только по предоплате.

Способы оплаты
В интернет-мультимаркете Kibet предусмотрены следующие способы оплаты:

Наличными при получении заказа
Оплата производится в гривнах.

Вы можете оплатить заказ при получении курьеру или менеджеру в точке выдачи.

После оплаты наличными вы получаете на руки товарный чек, подтверждающий факт покупки.

Наложенным платежом в отделении «Новой почты»
Вы можете оплатить заказ при получении в отделении «Новой почты». При этом сумма платежа будет состоять из:

- суммы заказа;

- оплаты услуги наложенного платежа (20 грн оформление + 2% от суммы заказа за обратную пересылку денег);

- оплаты доставки.

Внимание! Заказы на сумму до 100 грн оплачиваются на карту ПриватБанк.

На карту ПриватБанка
Вы можете оплатить заказ на карту ПриватБанка с помощью Privat24, через терминал ПриватБанка, через кассу как ПриватБанка, так и любого другого банка. Реквизиты для оплаты высылаются в смс на номер телефона, который вы укажите при оформлении заказа.

После оплаты ОБЯЗАТЕЛЬНО! сообщите менеджеру, что заказ оплачен.

Обращаем Ваше внимание на то, что при оплате на карту ПриватБанка с плательщика взымается комиссия, размер которой зависит от типа карты плательщика и используемой платежной системы/банка.


Безналичный расчет
Оплата заказа производится согласно выставленного менеджером магазина счета-фактуры безналичным банковским платежом через любой банк. Счет выставляется на основании скан-копий документов плательщика (Свидетельство о гос. регистрации, Свидетельство плательщика НДС – если являетесь).

Выставленный Вам счет действителен один банковский день. При оплате просроченного счета необходимо уточнить наличие товара и его актуальную цену.

Оплата в кредит
Подробную информацию об этом способе оплаты читайте в разделе «Кредиты и рассрочки»

Если у Вас возникли какие-либо вопросы, наши менеджеры с радостью ответят на них!