<?php
class ModelCustomerQuestions extends Model {
    public function deleteQuestion($question_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "questions WHERE id = '" . (int)$question_id . "'");
	}

	public function changeView($question_id = false) {
        if ($question_id !== false) {
            $this->db->query("UPDATE " . DB_PREFIX . "questions SET status_view = '1' WHERE id = '". (int)$question_id ."'");
        }
    }

	public function getQuestion($question_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "questions WHERE id = '" . (int)$question_id . "'");

		return $query->row;
	}

	public function getQuestions($data = array()) {
		$sql = "SELECT * FROM " . DB_PREFIX . "questions";

		$sort_data = array(
			'name',
			'date',
			'status_view'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalQuestions() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "questions");

		return $query->row['total'];
	}

	public function getNotViewQuestionsCount() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "questions WHERE status_view = '0'");

        return $query->row['total'];
    }
}
