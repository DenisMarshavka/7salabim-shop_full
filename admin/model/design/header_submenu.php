<?php
class ModelDesignHeaderSubMenu extends Model {
    public function addLink($data) {
        $this->db->query("
            CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "submenu_links` (
              `id` int(50) NOT NULL AUTO_INCREMENT,
              `filter_id` int(50) NOT NULL,
              `link_status` int(1) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");

        $this->db->query("INSERT INTO " . DB_PREFIX . "submenu_links SET filter_id = '" . (int)$this->db->escape($data['name']) . "', link_status = '" . (int)$data['status'] . "'");

        $link_id = $this->db->getLastId();

        return $link_id;
    }

    public function editLink($link_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "submenu_links SET filter_id = '" . (int)$this->db->escape($data['name']) . "', link_status = '" . (int)$data['status'] . "' WHERE id = '" . (int)$link_id . "'");
    }

    public function deleteLink($link_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "submenu_links WHERE id = '" . (int)$link_id . "'");
    }

    public function getLink($link_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "submenu_links WHERE id = '" . (int)$link_id . "'");

        return $query->row;
    }

    public function getLinks($data = array()) {
        $sql  = "SELECT DISTINCT * FROM " . DB_PREFIX . "submenu_links AS sub_links ";
        $sql .= "LEFT JOIN " . DB_PREFIX . "filter_description AS fd ON (sub_links.filter_id = fd.filter_id) WHERE fd.language_id = '" . (int)$this->config->get('config_language_id') . "'";



        $sort_data = array(
            'filter_id',
            'link_status'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY id";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getLinksInfo($link_id) {
        $link_info_data = array();

        $query  = "SELECT DISTINCT fd.name, fd.filter_id, sub_links.link_status, ";
        $query .= "CASE WHEN fd.filter_id = sub_links.filter_id THEN '1' ELSE '0' END AS current_active_group ";
        $query .= "FROM " . DB_PREFIX . "submenu_links AS sub_links ";
        $query .= "LEFT JOIN " . DB_PREFIX . "filter_description AS fd ON (fd.language_id = '" . (int)$this->config->get('config_language_id') . "') ";

        $query .= "WHERE fd.language_id = '" . (int)$this->config->get('config_language_id') . "' ";

        if ($link_id) $query .= "AND sub_links.id = '" . (int)$link_id . "'";

        $link_info_query = $this->db->query($query);

        foreach ($link_info_query->rows as $link_info) {
            if ((int)$link_info['current_active_group']) {
                $link_info_data['current'] = array(
                    'link_name'             => $link_info['name'],
                    'link_status'           => $link_info['link_status'],
                );
            }

            $link_info_data['selects'][] = array(
                'link_name'             => $link_info['name'],
                'filter_id'             => $link_info['filter_id']
            );
        }

        return $link_info_data;
    }

    public function getTotalLinks() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "submenu_links");

        return $query->row['total'];
    }
}
