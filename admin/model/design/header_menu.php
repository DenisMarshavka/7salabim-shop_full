<?php
class ModelDesignHeaderMenu extends Model {
    public function addLink($data) {
        $this->db->query("
            CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "menu_links` (
              `link_id` int(11) NOT NULL AUTO_INCREMENT,
              `link_name` char(100) NOT NULL,
              `link_status` int(11) NOT NULL,
              PRIMARY KEY (`link_id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");

        $this->db->query("INSERT INTO " . DB_PREFIX . "menu_links SET link_name = '" . $this->db->escape($data['name']) . "', link_status = '" . (int)$data['status'] . "'");

        $link_id = $this->db->getLastId();

        if (isset($data['link'])) {
            $this->db->query("
                CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "menu_links_info` (
                  `link_id` int(11) NOT NULL AUTO_INCREMENT,
                  `parent_link_id` int(11) NOT NULL,
                  `link_name` char(100) NOT NULL,
                  `link_href` char(100) NOT NULL,
                  `language_id` int(11) NOT NULL,
                  `header_status` int(1) NOT NULL,
                  `footer_status` int(1) NOT NULL,
                  `render_submenu_status` int(1) NOT NULL,
                  PRIMARY KEY (`link_id`)
                ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");

            foreach ($data['link'] as $language_id => $value) {
                foreach ($value as $link) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "menu_links_info SET parent_link_id = '" . (int)$link_id . "', language_id = '" . (int)$language_id . "', link_name = '" .  $this->db->escape($link['title']) . "', link_href = '" .  $link['link'] . "', header_status = '" .  (int)$link['header_status'] . "', footer_status = '" .  (int)$link['footer_status'] . "', render_submenu_status = '" . (int)$link['render_submenu_status'] . "'");
                }
            }
        }

        return $link_id;
    }

    public function editLink($link_id, $data) {
        $this->db->query("UPDATE " . DB_PREFIX . "menu_links SET link_name = '" . $this->db->escape($data['name']) . "', link_status = '" . (int)$data['status'] . "' WHERE link_id = '" . (int)$link_id . "'");

        $this->db->query("DELETE FROM " . DB_PREFIX . "menu_links_info WHERE parent_link_id = '" . (int)$link_id . "'");

        if (isset($data['link'])) {
            foreach ($data['link'] as $language_id => $value) {
                foreach ($value as $link) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "menu_links_info SET parent_link_id = '" . (int)$link_id . "', language_id = '" . (int)$language_id . "', link_name = '" .  $this->db->escape($link['title']) . "', link_href = '" .  $link['link'] . "', header_status = '" .  (int)$link['header_status'] . "', footer_status = '" .  (int)$link['footer_status'] . "', render_submenu_status = '" . (int)$link['render_submenu_status'] . "'");
                }
            }
        }
    }

    public function deleteLink($link_id) {
        $this->db->query("DELETE FROM " . DB_PREFIX . "menu_links WHERE link_id = '" . (int)$link_id . "'");
        $this->db->query("DELETE FROM " . DB_PREFIX . "menu_links_info WHERE parent_link_id = '" . (int)$link_id . "'");
    }

    public function getLink($link_id) {
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "menu_links WHERE link_id = '" . (int)$link_id . "'");

        return $query->row;
    }

    public function getLinks($data = array()) {
        $sql = "SELECT * FROM " . DB_PREFIX . "menu_links";

        $sort_data = array(
            'link_name',
            'language_id',
            'link_status'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY link_name";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getLinksInfo($link_id) {
        $link_info_data = array();

        $link_info_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "menu_links_info WHERE parent_link_id = '" . (int)$link_id . "'");

        foreach ($link_info_query->rows as $link_info) {
            $link_info_data[$link_info['language_id']][] = array(
                'link_name'             => $link_info['link_name'],
                'link_href'             => $link_info['link_href'],
                'header_status'         => $link_info['header_status'],
                'render_submenu_status' => $link_info['render_submenu_status'],
                'footer_status'         => $link_info['footer_status']
            );
        }

        return $link_info_data;
    }

    public function getTotalLinks() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "menu_links");

        return $query->row['total'];
    }
}
