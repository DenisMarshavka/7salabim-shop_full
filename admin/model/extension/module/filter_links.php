<?php
class ModelExtensionModuleFilterLinks extends Model {
    private $attributes_group = array();

    public function install() {
        $this->load->model('setting/event');

        $this->model_setting_event->addEvent('filter_links', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/module/filter_links/eventInstall');

        $this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "filter_links` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `filter_id_selected` int(50) NOT NULL,
			  `filter_image` varchar(255) NOT NULL,
			  `filter_order` int(1) NOT NULL,
			  PRIMARY KEY (`id`)
		) DEFAULT COLLATE=utf8_general_ci;");
    }

    public function uninstall() {
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "filter_links`");

        $this->load->model('setting/event');
        $this->model_setting_event->deleteEventByCode('filter_links');
    }

    public function addFilterLink($data = false) {
        if ($data) {
            $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "filter_links");

            if (isset($data['filter_links'])) {
                foreach ($data['filter_links'] as $filter) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "filter_links SET filter_id_selected = '" .  $filter['filter_id'] . "', filter_image = '" .  $this->db->escape($filter['image']) . "', filter_order = '" . $this->db->escape($filter['sort_order']) . "'");
                }
            }

            return true;
        } else return false;
    }

    public function getFilters() {
        $query = "SELECT filter_id, name FROM " . DB_PREFIX . "filter_description WHERE language_id = '" . (int)$this->config->get('config_language_id') . "'";

        $result_query = $this->db->query($query);

        if ($result_query->num_rows) {
            $this->attributes_group = $result_query->rows;

            return $this->attributes_group;
        }

        return false;
    }

    public function checkFilterLinks() {
        $filter_links = array();

        $query  = "SELECT DISTINCT fd.name, fd.filter_id, filter_links.filter_image, filter_links.filter_order, ";
        $query .= "CASE WHEN fd.filter_id = filter_links.filter_id_selected THEN '1' ELSE '0' END AS current_active_group ";
        $query .= "FROM " . DB_PREFIX . "filter_links AS filter_links ";
        $query .= "LEFT JOIN " . DB_PREFIX . "filter_description AS fd ON (fd.language_id = '" . (int)$this->config->get('config_language_id') . "')";

        $this->getFilters();

        $result_query = $this->db->query($query);

        foreach ($result_query->rows as $filter_link) {
            if ((int)$filter_link['current_active_group']) {
                $filter_links['active'][] = array(
                    'title' => $filter_link['name'],
                    'filter_id' => $filter_link['filter_id'],
                    'image' => $filter_link['filter_image'],
                    'sort_order' => $filter_link['filter_order']
                );
            }
        }

        if (!empty($this->attributes_group)) {
            foreach ($this->attributes_group as $select_item) {
                $filter_links['selects'][] = array(
                    'filter_id' => $select_item['filter_id'],
                    'title' => $select_item['name']
                );
            }
        }

        return $filter_links;
    }
}