<?php
class ModelExtensionModulePanelPriority extends Model {
    public function install() {
        $this->load->model('setting/event');

        $this->model_setting_event->addEvent('panel_priority', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/module/panel_priority/eventInstall');

        $this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "panel_priority` (
			  `priority_id` int(11) NOT NULL AUTO_INCREMENT,
			  `priority_title` varchar(155) NOT NULL ,
			  `priority_icon` varchar(255) NOT NULL,
			  `priority_order` int(1) NOT NULL ,
			  `language_id` int(5) NOT NULL ,
			  PRIMARY KEY (`priority_id`)
		) DEFAULT COLLATE=utf8_general_ci;");
    }

    public function uninstall() {
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "panel_priority`");

        $this->load->model('setting/event');
        $this->model_setting_event->deleteEventByCode('panel_priority');
    }

    public function addPriorityItems($data = false) {
        if ($data) {
            $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "panel_priority");

            if (isset($data['priority_item'])) {
                foreach ($data['priority_item'] as $language_id => $value) {
                    foreach ($value as $item) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "panel_priority SET language_id = '" . (int)$language_id . "', priority_title = '" .  $item['title'] . "', priority_icon = '" .  $this->db->escape($item['icon']) . "', priority_order = '" . $this->db->escape($item['sort_order']) . "'");
                    }
                }
            }

            return true;
        } else return false;
    }

    public function getPriorityItems() {
        $priority_items = array();

        $priority_items_query = $this->db->query("SELECT * FROM  " . DB_PREFIX . "panel_priority");

        foreach ($priority_items_query->rows as $item) {
            $priority_items[$item['language_id']][] = array(
                'title'      => $item['priority_title'],
                'icon'       => $item['priority_icon'],
                'sort_order' => $item['priority_order']
            );
        }

        return $priority_items;
    }
}