<?php

class ModelExtensionModuleSystemLikeReview extends Model {
    public function install() {
        $this->load->model('setting/event');

        $this->model_setting_event->addEvent('system_like_review', 'catalog/model/checkout/order/addOrderHistory/after', 'extension/module/system_like_review/eventInstall');

        $this->db->query("
			CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "likes_reviews` (
			  `id` int(11) NOT NULL AUTO_INCREMENT,
			  `user_ip` varchar(50) NOT NULL,
			  `review_id` int(11) NOT NULL,
			  `product_id` int(11) NOT NULL,
			  `is_like` int(11) NOT NULL DEFAULT '0' COMMENT '1 - it`s like, 0 - it`s unlike',
			  PRIMARY KEY (`id`)
		) DEFAULT COLLATE=utf8_general_ci;");
    }

    public function uninstall() {
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "likes_reviews`");

        $this->load->model('setting/event');
        $this->model_setting_event->deleteEventByCode('system_like_review');
    }
}