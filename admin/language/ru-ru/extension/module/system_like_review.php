<?php

// Heading
$_['heading_title']    = 'Система лайков для отзывов';

// Text
$_['text_extension']   = 'Расширения';
$_['text_edit']        = 'Настройки модуля';
$_['text_enabled']     = 'Включено';
$_['text_disabled']    = 'Отключено';

// Entry
$_['entry_status']     = 'Статус';

// Error
$_['error_permission']  = 'У Вас нет прав для управления данным модулем!';