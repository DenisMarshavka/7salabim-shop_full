<?php
// Heading
$_['heading_title']     = 'Вопросы';

// Text
$_['text_success']      = 'Настройки успешно изменены!';
$_['text_list']         = 'Список вопросов';
$_['button_view']       = 'Посмотреть';
$_['text_no_results']   = 'Вопросов пока что нету, спите спокойно :)';
$_['text_form']         = 'Чтение';

// Column
$_['column_name']       = 'Имя автора';
$_['column_status']     = 'Статус';
$_['column_date']       = 'Дата';
$_['column_action']     = 'Действие';

// Entry
$_['entry_name']        = 'Имя автора';
$_['entry_phone']       = 'Номер телефона';
$_['entry_email']       = 'E-mail';
$_['entry_text']        = 'Вопрос';
$_['entry_date']        = 'Дата';
$_['entry_time']        = 'Время';

$_['text_viewed']       = 'Прочитано';
$_['text_not_viewed']   = 'Не прочитано';

// Error
$_['error_permission']       = 'У Вас нет прав для изменения настроек!';

//Warning
$_['warning_date']           = 'От момента поступления вопроса, прошло уже больше 10-ти дней!';

