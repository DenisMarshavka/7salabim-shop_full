<?php
//Heading
$_['heading_title']      = 'Поденю';

// Text
$_['text_success']       = 'Настройки успешно изменены!';
$_['text_list']          = 'Список пунктов';
$_['text_add']           = 'Добавить';
$_['text_edit']          = 'Редактирование';
$_['text_default']       = 'По умолчанию';

// Column
$_['column_name']        = 'Название пункта';
$_['column_status']      = 'Статус';
$_['column_action']      = 'Действие';

// Entry
$_['entry_name']         = 'Название пункта';
$_['entry_status']       = 'Статус';

// Error
$_['error_permission']   = 'У Вас нет прав для изменения настроек!';