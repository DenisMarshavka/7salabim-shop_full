<?php
//Heading
$_['heading_title']      = 'Меню';

// Text
$_['text_success']       = 'Настройки успешно изменены!';
$_['text_list']          = 'Список пунктов';
$_['text_add']           = 'Добавить';
$_['text_edit']          = 'Редактирование';
$_['text_default']       = 'По умолчанию';

// Column
$_['column_name']        = 'Название пункта';
$_['column_status']      = 'Статус';
$_['column_action']      = 'Действие';

// Entry
$_['entry_name']         = 'Название пункта';
$_['entry_title']        = 'Заголовок';
$_['entry_link']         = 'Ссылка';
$_['entry_image']        = 'Изображение';
$_['entry_description']  = 'Описание';
$_['entry_price']        = 'Цена';
$_['entry_old_price']    = 'Старая цена';
$_['entry_link_inst']    = 'Ссылка Instagram';
$_['entry_link_fac']     = 'Ссылка Facebook';
$_['entry_link_ytb']     = 'Ссылка Youtube';
$_['entry_status']       = 'Статус';
$_['entry_header']       = 'У верху';
$_['entry_footer']       = 'У подвал';
$_['entry_inner_submenu']= 'Подменю';
$_['entry_sort_order']   = 'Порядок сортировки';

// Error
$_['error_permission']   = 'У Вас нет прав для изменения настроек!';
$_['error_name']         = 'Название пункта должно быть от 3 до 64 символов!';
$_['error_href']         = 'Ссылка не может быть пустой, должена местить от 2 до 64 символов!';
