<?php
//Heading
$_['heading_title']     = 'Верхнє меню';

// Text
$_['text_success']      = 'Настройки успішно змінені!!';
$_['text_list']         = 'Список пунктів';
$_['text_add']          = 'Добавити';
$_['text_edit']         = 'Редагування';
$_['text_default']      = 'По замовчуванню';

// Column
$_['column_name']       = 'Назва пунскта';
$_['column_status']     = 'Статус';
$_['column_action']     = 'Подія';

// Entry
$_['entry_name']        = 'Назва пункта';
$_['entry_title']       = 'Заголовок';
$_['entry_link']        = 'Ссилка';
$_['entry_image']       = 'Зоображення';
$_['entry_description'] = 'Опис';
$_['entry_price']       = 'Ціна';
$_['entry_old_price']   = 'Старая ціна';
$_['entry_link_inst']   = 'Ссилка Instagram';
$_['entry_link_fac']    = 'Ссилка Facebook';
$_['entry_link_ytb']    = 'Ссилка Youtube';
$_['entry_status']      = 'Статус';
$_['entry_header']      = 'У вверху';
$_['entry_submenu']     = 'У підменю';
$_['entry_footer']      = 'У підвал';
$_['entry_inner_submenu']= 'Підменю';
$_['entry_sort_order']  = 'Порядок сортування';

// Error
$_['error_permission'] = 'У Вас немає прав для зміни налаштувань!';
$_['error_name']       = 'Назва пункта має містити в собі від 3 до 64 символів!';
$_['error_href']       = 'Ссилка не може бути пустою, і має містити від 2 до 64 символів!';
