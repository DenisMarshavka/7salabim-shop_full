<?php
// Heading
$_['heading_title']     = 'Questions';

// Text
$_['text_success']      = 'Success: You have modified customer approvals!';
$_['text_list']         = 'List questions';
$_['button_view']       = 'View';
$_['text_no_results']   = 'List with questions is empty :)';
$_['text_form']         = 'Reading';

// Column
$_['column_name']       = 'First name';
$_['column_status']     = 'Status';
$_['column_date']       = 'Date';
$_['column_action']     = 'Events';

// Entry
$_['entry_name']        = 'First name';
$_['entry_phone']       = 'Phone number';
$_['entry_email']       = 'E-mail';
$_['entry_text']        = 'Question';
$_['entry_date']        = 'Date';
$_['entry_time']        = 'Time';

$_['text_viewed']       = 'Readed';
$_['text_not_viewed']   = 'Not readed';

// Error
$_['error_permission']       = 'Warning: You do not have permission to modify customer groups!';

//Warning
$_['warning_date']           = 'More than 10 days have passed since the moment the question arrived!';

