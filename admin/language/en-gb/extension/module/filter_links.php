<?php
// Heading
$_['heading_title']                = 'Panel filters';
$_['text_form']                    = 'Editor panel filters';

// Text
$_['text_extension']               = 'Extensions';
$_['text_success']                 = 'This settings is success changed!';

// Entry
$_['entry_title']                  = 'Name';
$_['entry_link']                   = 'Link';
$_['entry_image']                  = 'Icon';
$_['entry_sort_order']             = 'Sort order';

// Error
$_['error_permission']             = 'Warning: You do not have permission to modify panel filters!';
$_['error_title']                  = 'This filter Title must be between 3 and 35 characters!';
$_['error_link']                   = 'This filter Link must be between 3 and 35 characters!';
$_['error_image']                  = 'Icon for this filter is not selected!';
$_['error_sort']                   = 'Sort order for this filter is not installed!';