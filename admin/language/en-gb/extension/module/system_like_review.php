<?php

// Heading
$_['heading_title']    = 'System likes for reviews';

// Text
$_['text_extension']   = 'Extensions';
$_['text_edit']        = 'Settings module';
$_['text_enabled']     = 'Enabled';
$_['text_disabled']    = 'Disabled';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify system!';