<?php
// Heading
$_['heading_title']    = 'Submenu';

// Text
$_['text_success']      = 'Success: You have modified Links!';
$_['text_list']         = 'Links List';
$_['text_add']          = 'Add Link';
$_['text_edit']         = 'Edit Link';
$_['text_default']      = 'Default';

// Column
$_['column_name']       = 'Link Name';
$_['column_status']     = 'Status';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Link Name';
$_['entry_status']      = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify banners!';