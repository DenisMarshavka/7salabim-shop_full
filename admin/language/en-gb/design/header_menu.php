<?php
// Heading
$_['heading_title']    = 'Header menu';

// Text
$_['text_success']      = 'Success: You have modified banners!';
$_['text_list']         = 'Banner List';
$_['text_add']          = 'Add Banner';
$_['text_edit']         = 'Edit Banner';
$_['text_default']      = 'Default';

// Column
$_['column_name']       = 'Banner Name';
$_['column_status']     = 'Status';
$_['column_action']     = 'Action';

// Entry
$_['entry_name']        = 'Banner Name';
$_['entry_title']       = 'Title';
$_['entry_link']        = 'Link';
$_['entry_image']       = 'Image';
$_['entry_description'] = 'Description';
$_['entry_price']       = 'Price';
$_['entry_old_price']   = 'Old price';
$_['entry_link_inst']   = 'Link Instagram';
$_['entry_link_fac']    = 'Link Facebook';
$_['entry_link_ytb']    = 'Link Youtube';
$_['entry_status']      = 'Status';
$_['entry_header']       = 'In header';
$_['entry_footer']       = 'In footer';
$_['entry_inner_submenu'] = 'Submenu';
$_['entry_sort_order']   = 'Sort Order';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify banners!';
$_['error_name']       = 'Link Name must be between 3 and 64 characters!';
$_['error_title']      = 'Link href must be between 2 and 64 characters!';