<?php
class ControllerDesignHeaderMenu extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('design/header_menu');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('design/header_menu');

        $this->getList();
    }

    public function add() {
        $this->load->language('design/header_menu');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('design/header_menu');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_design_header_menu->addLink($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('design/header_menu', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getForm();
    }

    public function edit() {
        $this->load->language('design/header_menu');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('design/header_menu');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->model_design_header_menu->editLink($this->request->get['link_id'], $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('design/header_menu', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getForm();
    }

    public function delete() {
        $this->load->language('design/header_menu');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('design/header_menu');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $link_id) {
                $this->model_design_header_menu->deleteLink($link_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('design/header_menu', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getList();
    }

    protected function getList() {
        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('design/header_menu', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        $data['add'] = $this->url->link('design/header_menu/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['delete'] = $this->url->link('design/header_menu/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['links'] = array();

        $filter_data = array(
            'sort'  => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $link_total = $this->model_design_header_menu->getTotalLinks();

        $results = $this->model_design_header_menu->getLinks($filter_data);

        foreach ($results as $result) {
            $data['links'][] = array(
                'link_id'   => $result['link_id'],
                'name'      => $result['link_name'],
                'status'    => ($result['link_status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
                'edit'      => $this->url->link('design/header_menu/edit', 'user_token=' . $this->session->data['user_token'] . '&link_id=' . $result['link_id'] . $url, true)
            );
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_name'] = $this->url->link('design/header_menu', 'user_token=' . $this->session->data['user_token'] . '&sort=name' . $url, true);
        $data['sort_status'] = $this->url->link('design/header_menu', 'user_token=' . $this->session->data['user_token'] . '&sort=status' . $url, true);

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        $pagination = new Pagination();
        $pagination->total = $link_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('design/header_menu', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($link_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($link_total - $this->config->get('config_limit_admin'))) ? $link_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $link_total, ceil($link_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('design/header_menu_list', $data));
    }

    protected function getForm() {
        $data['text_form'] = !isset($this->request->get['link_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = '';
        }

        if (isset($this->error['links'])) {
            $data['links'] = $this->error['links'];
        } else {
            $data['links'] = array();
        }

        $url = '';

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('design/header_menu', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

        if (!isset($this->request->get['link_id'])) {
            $data['action'] = $this->url->link('design/header_menu/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        } else {
            $data['action'] = $this->url->link('design/header_menu/edit', 'user_token=' . $this->session->data['user_token'] . '&link_id=' . $this->request->get['link_id'] . $url, true);
        }

        $data['cancel'] = $this->url->link('design/header_menu', 'user_token=' . $this->session->data['user_token'] . $url, true);

        if (isset($this->request->get['link_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $link_info = $this->model_design_header_menu->getLink($this->request->get['link_id']);
        }

        $data['user_token'] = $this->session->data['user_token'];

        if (isset($this->request->post['link_name'])) {
            $data['link_name'] = $this->request->post['link_name'];
        } elseif (!empty($link_info)) {
            $data['link_name'] = $link_info['link_name'];
        } else {
            $data['link_name'] = '';
        }

        if (isset($this->request->post['link_status'])) {
            $data['link_status'] = $this->request->post['link_status'];
        } elseif (!empty($link_info)) {
            $data['link_status'] = $link_info['link_status'];
        } else {
            $data['link_status'] = true;
        }

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

//        $this->load->model('tool/image');
//
        if (isset($this->request->post['link_id'])) {
            $links = $this->request->post['link_id'];
        } elseif (isset($this->request->get['link_id'])) {
            $links = $this->model_design_header_menu->getLinksInfo($this->request->get['link_id']);
        } else {
            $links = array();
        }

        $data['links'] = array();

        foreach ($links as $key => $value) {
            foreach ($value as $link) {
                $data['links'][$key][] = array(
                    'title'                  => $link['link_name'],
                    'link'                   => $link['link_href'],
                    'header_status'          => $link['header_status'],
                    'footer_status'          => $link['footer_status'],
                    'render_submenu_status'  => $link['render_submenu_status']
                );
            }
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('design/header_menu_form', $data));
    }

    protected function validateForm() {
        if (!$this->user->hasPermission('modify', 'design/banner')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 64)) {
            $this->error['name'] = $this->language->get('error_name');
        }

        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'design/banner')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}