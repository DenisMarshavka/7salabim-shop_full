<?php
class ControllerCustomerQuestions extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('customer/questions');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('customer/questions');

        $this->getList();
    }

    protected function getList() {
        $data['button_view']     = $this->language->get('button_view');
        $data['text_no_results'] = $this->language->get('text_no_results');

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'status_view';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $url = '';

//        if (isset($this->request->get['sort'])) {
//            $url .= '&sort=' . $this->request->get['sort'];
//        }
//
//        if (isset($this->request->get['order'])) {
//            $url .= '&order=' . $this->request->get['order'];
//        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('customer/questions', 'user_token=' . $this->session->data['user_token'] . $url, true)
        );

//        $data['add'] = $this->url->link('design/banner/add', 'user_token=' . $this->session->data['user_token'] . $url, true);
        $data['delete'] = $this->url->link('customer/questions/delete', 'user_token=' . $this->session->data['user_token'] . $url, true);

        $data['questions'] = array();

        $filter_data = array(
            'sort'  => $sort,
            'order' => $order,
            'start' => ($page - 1) * $this->config->get('config_limit_admin'),
            'limit' => $this->config->get('config_limit_admin')
        );

        $questions_total = $this->model_customer_questions->getTotalQuestions();

        $results = $this->model_customer_questions->getQuestions($filter_data);

        if ($results) {
            foreach ($results as $result) {
                $url = '';
                $url = $this->url->link('customer/questions/view', 'user_token=' . $this->session->data['user_token'] . '&question_id=' . $result['id'] . $url, true);

                $data['questions'][] = array(
                    'question_id' => $result['id'],
                    'name'        => $result['name'],
                    'date'        => $this->getNormalDate($result['date']),
                    'status'      => ((int)$result['status_view'] ? $this->language->get('text_viewed') : $this->language->get('text_not_viewed')),
                    'status_view' => (int)$result['status_view'] ? true : false,
                    'view'        => $url .= ( (!(int)$result['status_view']) && ( $this->checkWarningDate($this->getNormalDate($result['date'], false)) ) ) ? '&warning_date=true' : ''
                );
            }
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['sort_view'] = $this->url->link('customer/questions', 'user_token=' . $this->session->data['user_token'] . '&sort=status_view' . $url, true);
        $data['sort_date'] = $this->url->link('customer/questions', 'user_token=' . $this->session->data['user_token'] . '&sort=date' . $url, true);
//        $data['sort_status'] = $this->url->link('design/banner', 'user_token=' . $this->session->data['user_token'] . '&sort=status' . $url, true);
//
//        $url = '';
//
//        if (isset($this->request->get['sort'])) {
//            $url .= '&sort=' . $this->request->get['sort'];
//        }
//
//        if (isset($this->request->get['order'])) {
//            $url .= '&order=' . $this->request->get['order'];
//        }

        $pagination = new Pagination();
        $pagination->total = $questions_total;
        $pagination->page = $page;
        $pagination->limit = $this->config->get('config_limit_admin');
        $pagination->url = $this->url->link('customer/questions', 'user_token=' . $this->session->data['user_token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($questions_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($questions_total - $this->config->get('config_limit_admin'))) ? $questions_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $questions_total, ceil($questions_total / $this->config->get('config_limit_admin')));

        $data['sort'] = $sort;
        $data['order'] = $order;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('customer/questions_list', $data));
    }

    public function view() {
        $this->load->language('customer/questions');

        $this->document->setTitle($this->language->get('heading_title'));

        if (isset($this->request->get['warning_date']) && $this->request->get['warning_date']) {
            $this->error['warning'] = $this->language->get('warning_date');;
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('customer/questions', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['cancel'] = $this->url->link('customer/questions', 'user_token=' . $this->session->data['user_token'], true);
        $data['user_token'] = $this->session->data['user_token'];

        if (isset($this->request->get['question_id'])) {
            $this->load->model('customer/questions');

            $data['question'] = $this->model_customer_questions->getQuestion($this->request->get['question_id']);
            $this->model_customer_questions->changeView($this->request->get['question_id']);
        }

        if (isset($data['question']['date'])) $data['question']['time'] = explode(' ', $data['question']['date'])[1];
        if (isset($data['question']['date'])) $data['question']['date'] = $this->getNormalDate($data['question']['date'], false);

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('customer/questions_form', $data));
    }

    public function delete() {
        $this->load->language('customer/questions');

        $this->document->setTitle($this->language->get('customer/questions'));

        $this->load->model('customer/questions');

        if (isset($this->request->post['selected'])) {
            foreach ($this->request->post['selected'] as $question_id) {
                $this->model_customer_questions->deleteQuestion($question_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('customer/questions', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getList();
    }

    public function getNormalDate($date = '', $time = true) {
        if ($date !== false) {
            $date_arr = explode(' ' ,$date);
            $date_months_arr = explode('-' ,$date_arr[0]);
            $date_year_arr = $date_months_arr[0];
            $date_time_arr = explode(':' ,$date_arr[1]);

            $normal_date = $date_months_arr[2] . '.' . $date_months_arr[1] . '.' . $date_year_arr[2] . $date_year_arr[3];

            if ($time) $normal_date .= ' ' . $date_time_arr[0] . ':' . $date_time_arr[1];

            return $normal_date;
        }
    }

    public function checkWarningDate($date = false) {
        if ($date !== false) {
            $arr_date          = explode('.', $date);
            $arr_current_date  = explode('.', date("d.m.y"));

            if ( ($arr_date[1] <= $arr_current_date[1]) && ($arr_date[2] <= $arr_current_date[2]) ) {
                if ( ($arr_date[0] < $arr_current_date[0]) && ( ($arr_current_date[0] - $arr_date[0]) > 10 ) ) {
                    return true;
                }
            }

            return false;
        }
    }
}
