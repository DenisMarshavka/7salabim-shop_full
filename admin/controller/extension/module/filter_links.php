<?php
class ControllerExtensionModuleFilterLinks extends Controller {
    private $error = array();
    public $filter_links = array();

    public function index() {
        $this->load->language('extension/module/filter_links');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/module');

        $this->load->model('extension/module/filter_links');
        $this->filter_links = $this->model_extension_module_filter_links->checkFilterLinks();

        //Languages
        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        $this->getForm();
    }

    public function install() {
        $this->load->language('extension/module/filter_links');
        $this->load->model('setting/module');

        if ($this->validate()) {
            $this->model_setting_module->addModule('filter_links', $this->request->get['extension']);

            $this->load->model('user/user_group');

            $this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'extension/module/filter_links' . $this->request->get['extension']);
            $this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'extension/module/filter_links' . $this->request->get['extension']);

            $this->load->model('extension/module/filter_links');
            $this->model_extension_module_filter_links->install();

            // Call install method if it exsits
            $this->load->controller('extension/module/filter_links' . $this->request->get['extension'] . '/install');

            $this->session->data['success'] = $this->language->get('text_success');
        }
    }

    public function uninstall() {
        $this->load->language('extension/module/filter_links');
        $this->load->model('setting/module');

        if ($this->validate()) {
            $this->model_setting_module->deleteModule($this->request->get['module_id']);

            // Call uninstall method if it exsits
            $this->load->controller('extension/module/filter_links' . $this->request->get['extension'] . '/uninstall');

            $this->session->data['success'] = $this->language->get('text_success');
        }
    }

    public function add() {
        $this->load->language('extension/module/filter_links');
        $this->load->model('extension/module/filter_links');

        if ($this->request->server['REQUEST_METHOD'] = 'POST' && $this->validate()) {
            $this->model_extension_module_filter_links->addFilterLink($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('extension/module/filter_links', 'user_token=' . $this->session->data['user_token'] . $url, true));
        }

        $this->getForm();
    }

    public function getForm() {
        $this->load->language('extension/module/filter_links');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_module->editSetting('module_filter_links', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['filter_image'])) {
            $data['error_filter_image'] = $this->error['filter_image'];
        } else {
            $data['error_filter_image'] = array();
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/filter_links', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['action'] = $this->url->link('extension/module/filter_links/add', 'user_token=' . $this->session->data['user_token'], true);

        $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        $this->load->model('tool/image');

        if ($this->filter_links) {
            $filter_links = $this->filter_links;
        } else {
            $filter_links = $this->model_extension_module_filter_links->checkFilterLinks();
        }

        $data['title_web_agency'] = 'By Starfall WEB Agency';
        $data['logo_web_agency'] = (is_file(DIR_IMAGE . '/catalog/starall-WEB-icon.png')) ? 'http://' . $_SERVER['HTTP_HOST'] . '/image/catalog/starall-WEB-icon.png' : false;
        $data['logo_web_agency_alt'] = 'Starfall WEB Agency';

        $data['filter_links'] = array();

        foreach ($filter_links['active'] as $active_item) {
            if (is_file(DIR_IMAGE . $active_item['image'])) {
                $image =  $active_item['image'];
                $thumb =  $active_item['image'];
            } else {
                $image = '';
                $thumb = 'no_image.png';
            }

            $data['filter_links']['active'][] = array(
                'title'      => $active_item['title'],
                'filter_id'  => $active_item['filter_id'],
                'image'      => $image,
                'thumb'      => $this->model_tool_image->resize($thumb, 100, 100),
                'sort_order' => $active_item['sort_order']
            );


            if (isset($filter_links['selects'])) {
                $data['filter_links']['selects'] = $filter_links['selects'];
                $data['data_select'] = json_encode($filter_links['selects']);
            }
        }

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/filter_links', $data));
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/module/filter_links')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (isset($this->request->post['filter_links'])) {
            foreach ($this->request->post['filter_links'] as $filter_key => $value) {
                    if ( empty(trim($this->request->post['filter_links'][$filter_key]['image'])) ) {
                        $this->error['filter_image'][$filter_key] = $this->language->get('error_image');
                    }
                }
        }

        return !$this->error;
    }
}