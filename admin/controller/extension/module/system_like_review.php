<?php
class ControllerExtensionModuleSystemLikeReview extends Controller {
    protected $error = array();
    
    public function index() {
        $this->load->language('extension/module/system_like_review');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('module_system_like_review', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/system_like_review', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['action'] = $this->url->link('extension/module/system_like_review', 'user_token=' . $this->session->data['user_token'], true);

        $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

        if (isset($this->request->post['module_system_like_review_status'])) {
            $data['module_system_like_review_status'] = $this->request->post['module_system_like_review_status'];
        } else {
            $data['module_system_like_review_status'] = $this->config->get('module_system_like_review_status');
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/system_like_review', $data));
    }

    public function install() {
        $this->load->language('extension/module/system_like_review');

        $this->load->model('setting/module');

        if ($this->validate()) {
            $this->model_setting_module->addModule('system_like_review', $this->request->get['extension']);

            $this->load->model('user/user_group');

            $this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'extension/module/system_like_review' . $this->request->get['extension']);
            $this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'extension/module/system_like_review' . $this->request->get['extension']);

            $this->load->model('extension/module/system_like_review');
            $this->model_extension_module_system_like_review->install();

            // Call install method if it exsits
            $this->load->controller('extension/module/system_like_review' . $this->request->get['extension'] . '/install');

            $this->session->data['success'] = $this->language->get('text_success');
        }
    }

    public function uninstall() {
        $this->load->language('extension/module/system_like_review');

        $this->load->model('setting/module');

        if ($this->validate()) {
            $this->load->model('extension/module/system_like_review');

            $this->model_setting_module->deleteModule($this->request->get['module_id']);

            // Call uninstall method if it exsits
            $this->load->controller('extension/module/system_like_review' . $this->request->get['extension'] . '/uninstall');
            $this->model_extension_module_system_like_review->uninstall();

            $this->session->data['success'] = $this->language->get('text_success');
        }
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/module/system_like_review')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}