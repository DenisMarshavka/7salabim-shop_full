<?php
class ControllerExtensionModulePanelPriority extends Controller {
    private $error = array();
    public $priority_items = array();

    public function index() {
        $this->load->language('extension/module/panel_priority');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/module');

        $this->load->model('extension/module/panel_priority');
        $this->priority_items = $this->model_extension_module_panel_priority->getPriorityItems();

        //Languages
        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        $this->getForm();
    }

    public function install() {
        $this->load->language('extension/module/panel_priority');

        $this->load->model('setting/module');

        if ($this->validate()) {
            $this->model_setting_module->addModule('panel_priority', $this->request->get['extension']);

            $this->load->model('user/user_group');

            $this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'extension/module/panel_priority' . $this->request->get['extension']);
            $this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'extension/module/panel_priority' . $this->request->get['extension']);

            $this->load->model('extension/module/panel_priority');
            $this->model_extension_module_panel_priority->install();

            // Call install method if it exsits
            $this->load->controller('extension/module/panel_priority' . $this->request->get['extension'] . '/install');

            $this->session->data['success'] = $this->language->get('text_success');
        }
    }

    public function uninstall() {
        $this->load->model('setting/module');

        if ($this->validate()) {
            $this->model_setting_module->deleteModule($this->request->get['module_id']);

            // Call uninstall method if it exsits
            $this->load->controller('extension/module/panel_priority' . $this->request->get['extension'] . '/uninstall');

            $this->load->model('extension/module/panel_priority');
            $this->model_extension_module_panel_priority->uninstall();

            $this->session->data['success'] = $this->language->get('text_success');
        }
    }

    public function add() {
        $this->load->language('extension/module/panel_priority');

        $this->load->model('extension/module/panel_priority');

        if ($this->request->server['REQUEST_METHOD'] = 'POST' && $this->validate()) {
            $this->model_extension_module_panel_priority->addPriorityItems($this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/module/panel_priority', 'user_token=' . $this->session->data['user_token'], true));
        }

        $this->getForm();
    }

    public function getForm() {
        $this->load->language('extension/module/panel_priority');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_module->editSetting('module_panel_priority', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['priority_title'])) {
            $data['error_priority_title'] = $this->error['priority_title'];
        } else {
            $data['error_priority_title'] = array();
        }

        if (isset($this->error['priority_icon'])) {
            $data['error_priority_icon'] = $this->error['priority_icon'];
        } else {
            $data['error_priority_icon'] = array();
        }

        if (isset($this->error['priority_sort_order'])) {
            $data['error_priority_sort_order'] = $this->error['priority_sort_order'];
        } else {
            $data['error_priority_sort_order'] = array();
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/filter_links', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['action'] = $this->url->link('extension/module/panel_priority/add', 'user_token=' . $this->session->data['user_token'], true);

        $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        $this->load->model('tool/image');

        if (isset($this->request->post['priority_item'])) {
            $priority_items = $this->request->post['priority_item'];
        } elseif ($this->priority_items) {
            $priority_items = $this->priority_items;
        } else {
            $priority_items = array();
        }

        foreach ($priority_items as $key => $value) {
            foreach ($value as $item) {
                if (is_file(DIR_IMAGE . $item['icon'])) {
                    $image = $item['icon'];
                    $thumb = $item['icon'];
                } else {
                    $image = '';
                    $thumb = 'no_image.png';
                }

                $data['priority_items'][$key][] = array(
                    'title'      => $item['title'],
                    'icon'      => $image,
                    'thumb'      => $this->model_tool_image->resize($thumb, 100, 100),
                    'sort_order' => $item['sort_order']
                );
            }
        }

        $data['placeholder'] = $this->model_tool_image->resize('no_image.png', 100, 100);

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/module/panel_priority', $data));
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/module/panel_priority')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (isset($this->request->post['priority_item'])) {
            foreach ($this->request->post['priority_item'] as $language_id => $value) {
                foreach ($value as $item_id => $item) {
                    if ( (utf8_strlen($item['title']) < 3) || (utf8_strlen($item['title']) > 35) ) {
                        $this->error['priority_title'][$language_id][$item_id] = $this->language->get('error_title');
                    }

                    if ( empty(trim($item['icon'])) ) {
                        $this->error['priority_icon'][$language_id][$item_id] = $this->language->get('error_icon');
                    }

                    if ( (utf8_strlen($item['sort_order']) < 1) || !is_numeric(trim($item['sort_order'])) ) {
                        $this->error['priority_sort_order'][$language_id][$item_id] = $this->language->get('error_sort');
                    }
                }
            }
        }

        return !$this->error;
    }
}